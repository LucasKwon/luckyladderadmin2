/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 2.0.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v2.0/admin/html/
*/

var getMonthName = function(number) {
    var month = [];
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";

    return month[number];
};

var getDate = function(date) {
    var currentDate = new Date(date);
    var dd = currentDate.getDate();
    var mm = currentDate.getMonth() + 1;
    var yyyy = currentDate.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    currentDate = yyyy+'-'+mm+'-'+dd;

    return currentDate;
};


var handleVisitorsLineChart = function() {
    var green = '#0D888B';
    var greenLight = '#00ACAC';
    var blue = '#3273B1';
    var blueLight = '#348FE2';
    var blackTransparent = 'rgba(0,0,0,0.6)';
    var whiteTransparent = 'rgba(255,255,255,0.4)';



    var tax_data = $('#chartField').data("field-id");

    Morris.Line({
        element: 'visitors-line-chart',
        data: tax_data,
        xkey: 'dtv_date',
        ykeys: ['BETS', 'WINS'],
        xLabelFormat: function(x) {
            x = getMonthName(x.getMonth());
            return x.toString();
        },
        labels: ['BET', 'WIN'],
        lineColors: [green, blue],
        pointFillColors: [greenLight, blueLight],
        lineWidth: '2px',
        pointStrokeColors: [blackTransparent, blackTransparent],
        resize: true,
        gridTextFamily: 'Open Sans',
        gridTextColor: whiteTransparent,
        gridTextWeight: 'normal',
        gridTextSize: '11px',
        gridLineColor: 'rgba(0,0,0,0.5)',
        hideHover: 'auto',
    });
};

var handleDepositsLineChart = function() {
    var green = '#0D888B';
    var greenLight = '#00ACAC';
    var blue = '#3273B1';
    var blueLight = '#348FE2';

    var yellow = '#f3c816';
    var yellowLight = '#efcd0e';
    var red = '#d4460a';
    var redLight = '#D92B0A';

    var totdepositcolor = '#DF0BE3';
    var totdepositcolorLight = '#b60dd0';
    var totpayoutcolor = '#eae0e8';
    var totpayoutcolorLight = '#E2F1F1';

    var blackTransparent = 'rgba(0,0,0,0.6)';
    var whiteTransparent = 'rgba(255,255,255,0.4)';



    var tax_data = $('#chartDepositField').data("field-id");

    Morris.Line({
        element: 'deposits-line-chart',
        data: tax_data,
        xkey: 'dtv_date',
        ykeys: ['DEPOSITS', 'MANUAL_DEPOSITS', 'PAYOUT', 'MANUAL_PAYOUT', 'TOT_DEPOSITS', 'TOT_PAYOUTS'],
        xLabelFormat: function(x) {
            x = getMonthName(x.getMonth());
            return x.toString();
        },
        labels: ['DEPOSITS', 'MANUAL DEPOSITS', 'PAYOUT', 'MANUAL PAYOUT', 'TOTAL DEPOSITS', 'TOTAL PAYOUTS'],
        lineColors: [green, blue, yellow, red, totdepositcolor, totpayoutcolor],
        pointFillColors: [greenLight, blueLight, yellowLight, redLight, totdepositcolorLight, totpayoutcolorLight],
        lineWidth: '2px',
        pointStrokeColors: [blackTransparent, blackTransparent],
        resize: true,
        gridTextFamily: 'Open Sans',
        gridTextColor: whiteTransparent,
        gridTextWeight: 'normal',
        gridTextSize: '11px',
        gridLineColor: 'rgba(0,0,0,0.5)',
        hideHover: 'auto',
    });
};


var handleVisitorsDonutChart = function() {
    var green = '#00acac';
    var blue = '#348fe2';
    Morris.Donut({
        element: 'visitors-donut-chart',
        data: [
            {label: "New Visitors", value: 900},
            {label: "Return Visitors", value: 1200}
        ],
        colors: [green, blue],
        labelFamily: 'Open Sans',
        labelColor: 'rgba(255,255,255,0.4)',
        labelTextSize: '12px',
        backgroundColor: '#242a30'
    });
};

var handleVisitorsDonutChart2 = function() {
    var blue		= '#348fe2',
        blueLight	= '#5da5e8',
        blueDark	= '#1993E4',
        aqua		= '#49b6d6',
        aquaLight	= '#6dc5de',
        aquaDark	= '#3a92ab',
        green		= '#00acac',
        greenLight	= '#33bdbd',
        greenDark	= '#008a8a',
        orange		= '#f59c1a',
        orangeLight	= '#f7b048',
        orangeDark	= '#c47d15',
        dark		= '#2d353c',
        grey		= '#b6c2c9',
        purple		= '#727cb6',
        purpleLight	= '#8e96c5',
        purpleDark	= '#5b6392',
        red         = '#ff5b57';

    var tax_data = $('#chartThirdpartyField').data("field-id");

    Morris.Donut({
        element: 'visitors-donut-chart2',
        data: tax_data,
        colors: [green, blue, orange, grey, purple, red],
        labelFamily: 'Open Sans',
        labelColor: 'rgba(255,255,255,0.4)',
        labelTextSize: '12px',
        backgroundColor: '#242a30'
    });
};


var handleVisitorsVectorMap = function() {
    if ($('#visitors-map').length !== 0) {
        map = new jvm.WorldMap({
            map: 'world_merc_en',
            scaleColors: ['#e74c3c', '#0071a4'],
            container: $('#visitors-map'),
            normalizeFunction: 'linear',
            hoverOpacity: 0.5,
            hoverColor: false,
            markerStyle: {
                initial: {
                    fill: '#4cabc7',
                    stroke: 'transparent',
                    r: 3
                }
            },
            regions: [{
                attribute: 'fill'
            }],
            regionStyle: {
                initial: {
                    fill: 'rgb(97,109,125)',
                    "fill-opacity": 1,
                    stroke: 'none',
                    "stroke-width": 0.4,
                    "stroke-opacity": 1
                },
                hover: {
                    "fill-opacity": 0.8
                },
                selected: {
                    fill: 'yellow'
                },
                selectedHover: {
                }
            },
            series: {
                regions: [{
                values: {
                    IN:'#00acac',
                    US:'#00acac',
                    KR:'#00acac'
                }
                }]
            },
            focusOn: {
                x: 0.5,
                y: 0.5,
                scale: 2
            },
            backgroundColor: '#2d353c'
        });
    }
};

var handleScheduleCalendar = function() {
    var monthNames = ["January", "February", "March", "April", "May", "June",  "July", "August", "September", "October", "November", "December"];
    var dayNames = ["S", "M", "T", "W", "T", "F", "S"];

    var now = new Date(),
        month = now.getMonth() + 1,
        year = now.getFullYear();

    var events = [
        [
            '2/' + month + '/' + year,
            'Popover Title',
            '#',
            '#00acac',
            'Some contents here'
        ],
        [
            '5/' + month + '/' + year,
            'Tooltip with link',
            'http://www.seantheme.com/color-admin-v1.3',
            '#2d353c'
        ],
        [
            '18/' + month + '/' + year,
            'Popover with HTML Content',
            '#',
            '#2d353c',
            'Some contents here <div class="text-right"><a href="http://www.google.com">view more >>></a></div>'
        ],
        [
            '28/' + month + '/' + year,
            'Color Admin V1.3 Launched',
            'http://www.seantheme.com/color-admin-v1.3',
            '#2d353c',
        ]
    ];
    var calendarTarget = $('#schedule-calendar');
    $(calendarTarget).calendar({
        months: monthNames,
        days: dayNames,
        events: events,
        popover_options:{
            placement: 'top',
            html: true
        }
    });
    $(calendarTarget).find('td.event').each(function() {
        var backgroundColor = $(this).css('background-color');
        $(this).removeAttr('style');
        $(this).find('a').css('background-color', backgroundColor);
    });
    $(calendarTarget).find('.icon-arrow-left, .icon-arrow-right').parent().on('click', function() {
        $(calendarTarget).find('td.event').each(function() {
            var backgroundColor = $(this).css('background-color');
            $(this).removeAttr('style');
            $(this).find('a').css('background-color', backgroundColor);
        });
    });
};

var handleDashboardGritterNotification = function() {
    $(window).load(function() {
        setTimeout(function() {
            $.gritter.add({
                title: 'Welcome back, Admin!',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus lacus ut lectus rutrum placerat.',
                image: 'assets/img/user-14.jpg',
                sticky: true,
                time: '',
                class_name: 'my-sticky-class'
            });
        }, 1000);
    });
};

var DashboardV2 = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleVisitorsLineChart();
            //handleVisitorsDonutChart();
            handleVisitorsDonutChart2();
            //handleVisitorsVectorMap();
            handleScheduleCalendar();
            handleDepositsLineChart();
            //handleDashboardGritterNotification();
        }
    };
}();