<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Games extends Model
{
    protected $primaryKey = 'limit_idx';

    /**
     * 베팅리밋 리스트
     */
    public function selectBettingLimitsList($params)
    {
        return DB::table('betting_limits')
            ->join('operators', 'betting_limits.op_idx', '=', 'operators.op_idx')
            ->where('betting_limits.op_parent_idx', '=', $params['op_idx'])
            ->groupBy('betting_limits.max_bet'
                , 'betting_limits.min_bet'
                , 'betting_limits.max_tie'
                , 'betting_limits.min_tie'
                , 'betting_limits.op_parent_idx')
            ->orderBy('betting_limits.max_bet', 'asc')
            ->get();
    }

    public function selectBetLimitByIdx($whereParams)
    {
        return DB::table('betting_limits')
            ->where([['limit_idx', '=', $whereParams['limit_idx']], ['op_parent_idx', '=', $whereParams['op_idx']]])
            ->first();
    }


    public function selectBettingLimitsOperatorList($whereParams)
    {
        return DB::table('betting_limits')
            ->where([
                ['max_bet', '=', $whereParams['max_bet']]
                , ['min_bet', '=', $whereParams['min_bet']]
                , ['max_tie', '=', $whereParams['max_tie']]
                , ['min_tie', '=', $whereParams['min_tie']]
                , ['op_parent_idx', '=', $whereParams['op_parent_idx']]
            ])->get();
    }

    /**
     * @param $params
     *
     * 베팅리밋 입력
     */
    public function insertBettingLimits($params)
    {
        return DB::table('betting_limits')->insertGetId($params);
    }

    /**
     * @param $whereParams
     * @param $updateParams
     * @return mixed
     *
     * 베팅리밋 활성/비활성
     */
    public function updateBettingLimits($whereParams, $updateParams)
    {
        //dd($whereParams, $updateParams);
        return DB::table('betting_limits')
            ->where([
                ['op_parent_idx', '=', $whereParams['op_parent_idx']],
                ['op_idx', '=', $whereParams['op_idx']],
                ['max_bet', '=', $whereParams['max_bet']],
                ['min_bet', '=', $whereParams['min_bet']],
                ['max_tie', '=', $whereParams['max_tie']],
                ['min_tie', '=', $whereParams['min_tie']],
            ])
            ->update($updateParams);
    }

    /**
     * @param $whereParams
     * @return mixed
     *
     * 베팅리밋 삭제
     */
    public function deleteBettingLimits($whereParams)
    {
        return DB::table('betting_limits')
            ->where([
                ['op_parent_idx', '=', $whereParams['op_parent_idx']],
                ['max_bet', '=', $whereParams['max_bet']],
                ['min_bet', '=', $whereParams['min_bet']],
                ['max_tie', '=', $whereParams['max_tie']],
                ['min_tie', '=', $whereParams['min_tie']],
            ])
            ->when($whereParams['op_idx'], function ($query) use ($whereParams) {
                return $query->where('op_idx', $whereParams['op_idx']);
            })
            ->delete();

    }


    /**
     * @return mixed
     *
     * 게임(테이블 리스트
     */
    public function selectGameList()
    {
        return DB::table('games')
            //->select('games.g_idx', 'games.g_title', 'games.g_status', 'games.g_org_status', 'game_table_info.tb_status', 'dealers.d_name')
            ->join('game_table_info', 'games.g_idx', '=', 'game_table_info.g_idx')
            ->join('dealers', 'game_table_info.d_idx', '=', 'dealers.d_idx')
            ->groupBy('games.g_idx', 'games.g_title', 'games.g_status', 'games.g_org_status', 'game_table_info.tb_status', 'dealers.d_name')
            ->get();
    }


    /**
     * @return mixed
     *
     * 딜러 리스트
     */
    public function selectDealerList()
    {
        return DB::table('dealers')
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 게임 상세정보
     */
    public function selectGameDetailInfo($params)
    {
        return DB::table('games')
            ->where('g_idx', '=', $params['g_idx'])
            ->first();
    }


    /**
     * @param $params
     * @return mixed
     */
    public function selectGameId($params)
    {
        return DB::table('games')
            ->where('g_id', '=', $params['g_id'])
            ->count();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function selectGameTitle($params)
    {
        return DB::table('games')
            ->where('g_title', '=', $params['g_title'])
            ->count();
    }


    /**
     * @param $whereParams
     * @param $updateParams
     * @return mixed
     *
     * 게임 상세정보 업데이트
     */
    public function updateGameInfo($whereParams, $updateParams)
    {
        return DB::table('games')
            ->where('g_idx', '=', $whereParams['g_idx'])
            ->update($updateParams);
    }


    /**
     * @return mixed
     *
     * 게임 테이블 리스트
     */
    public function selectGameTableList($params)
    {
        return DB::table('game_tables')
            ->groupBy(
                'game_tables.tb_name',
                'game_tables.tb_currency',
                'game_tables.tb_status',
                'game_tables.op_parent_idx'
            )
            ->join('games', 'games.g_idx', '=', 'game_tables.g_idx')
            ->where('op_parent_idx', '=', $params['op_parent_idx'])
            ->get();

    }


    /**
     * @param $params
     * @return mixed
     *
     * 테이블명 중복 확인
     */
    public function selectTableName($params)
    {
        return DB::table('game_tables')
            ->where('tb_name', '=', $params['tb_name'])
            ->count();
    }


    /**
     * @param $params
     * @return mixed
     *
     * 테이블 정보 입력
     */
    public function insertGameTableInfo($params)
    {
        return DB::table('game_tables')->insertGetId($params);
    }


    /**
     * @param $params
     * @return mixed
     *
     * 테이블 상세정보
     */
    public function selectTableDetailInfo($params)
    {
        return DB::table('game_tables')
            ->join('games', 'games.g_idx', '=', 'game_tables.g_idx')
            ->join('dealers', 'dealers.d_idx', '=', 'game_tables.d_idx')
            ->where('game_tables.tb_idx', '=', $params['tb_idx'])
            ->first();
    }

    /**
     * @param $whereParams
     * @param $updateParams
     * @return mixed
     *
     * 테이블 정보 업데이트
     */
    public function updateGameTableInfo($whereParams, $updateParams)
    {
        return DB::table('game_tables')
            ->where([
                ['tb_name', '=', $whereParams['tb_name']],
                ['tb_currency', '=', $whereParams['tb_currency']],
                ['g_idx', '=', $whereParams['g_idx']],
                ['d_idx', '=', $whereParams['d_idx']],
                ['tb_status', '=', $whereParams['tb_status']],
                ['op_parent_idx', '=', $whereParams['op_parent_idx']],
                ['op_idx', '=', $whereParams['op_idx']],
            ])
            ->update($updateParams);
    }

    /**
     * @param $whereParams
     * @return mixed
     *
     * 테이블 정보 삭제
     */
    public function deleteGameTable($whereParams)
    {
        return DB::table('game_tables')
            ->where([
                ['tb_name', '=', $whereParams['tb_name']],
                ['tb_currency', '=', $whereParams['tb_currency']],
                ['g_idx', '=', $whereParams['g_idx']],
                ['d_idx', '=', $whereParams['d_idx']],
                ['tb_status', '=', $whereParams['tb_status']],
                ['op_parent_idx', '=', $whereParams['op_parent_idx']],
            ])
            ->when($whereParams['op_idx'], function ($query) use ($whereParams) {
                return $query->where('op_idx', $whereParams['op_idx']);
            })
            ->delete();
    }

    /**
     * @param $whereParams
     * @return mixed
     *
     * 테이블 관리자별 에이전트리스트
     */
    public function selectTableOperatorList($whereParams)
    {
        return DB::table('game_tables')
            ->where([
                ['tb_name', '=', $whereParams['tb_name']],
                ['tb_currency', '=', $whereParams['tb_currency']],
                ['g_idx', '=', $whereParams['g_idx']],
                ['d_idx', '=', $whereParams['d_idx']],
                ['op_parent_idx', '=', $whereParams['op_parent_idx']]
            ])->get();
    }
}
