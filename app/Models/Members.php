<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Members extends Model
{
    protected $primaryKey = 'mb_idx';

    /**
     * @param $params
     * @return mixed
     *
     * 해당 오퍼레이터 하위의 플레이어 리스트
     */
    public function selectPlayerListByAdminOperatorIdx($params)
    {
        return DB::table('members')
            ->where('op_idx', '=', $params['op_idx'])
            ->orderBy('mb_created_date', 'desc')
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 해당 플레이어 상세정보
     */
    public function selectPlayerDetailByAdminOperatorIdx($params)
    {
        return DB::table('members')
            ->where([['op_idx', $params['op_idx']], ['mb_idx', $params['mb_idx']]])
            ->first();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 게임 사용자(유저) 등록시 아이디 중복확인
     */
    public function selectMemberIdByInputId($params)
    {
        return DB::table('members')
            ->where([
                ['mb_id', '=', $params['mb_id']],
                ['op_idx', '=', $params['op_idx']]
            ])
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 신규 게임사용자 등록
     */
    public function insertNewGamePlayerInfo($params)
    {
        return DB::table('members')
            ->insertGetId($params);
    }


    /**
     * @param $updateFields
     * @param $where_Fields
     *
     * 게임사용자 정보 갱신
     */
    public function updateGamePlayerInfo($updateFields, $where_Fields)
    {
        DB::table('members')
            ->where([['mb_id', '=', $where_Fields['mb_id']], ['op_idx', '=', $where_Fields['op_idx']]])
            ->update($updateFields);
    }


    /**
     * @param $params
     * @return mixed
     *
     * 게임사용자 이름 중복확인
     */
    public function selectGamePlayerByName($params)
    {
        return DB::table('members')
            ->where('mb_name','=',$params['mb_name'])
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     *
     * 게임사용자 이메일 중복확인
     */
    public function selectPlayerEmailByUserEmail($params)
    {
        return DB::table('members')
            ->where('mb_email','=',$params['mb_email'])
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     *
     * 게임사용자 아이디 중복확인
     */
    public function selectPlayerIdByUserEmail($params)
    {
        return DB::table('members')
            ->where('mb_id','=',$params['mb_id'])
            ->get();
    }
}
