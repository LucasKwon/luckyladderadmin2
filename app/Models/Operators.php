<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Operators extends Model implements Authenticatable
{
    //
    public function getAuthIdentifierName()
    {
        // TODO: Implement getAuthIdentifierName() method.
        return 'op_id';
    }

    public function getAuthIdentifier()
    {
        // TODO: Implement getAuthIdentifier() method.
        return $this->op_idx;
    }

    public function getAuthPassword()
    {
        // TODO: Implement getAuthPassword() method.
        return $this->op_password;
    }

    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
        return 'remember_token';
    }

    /**
     * @param $params
     * @return mixed
     *
     * ADMIN 레벨의 Operator 리스트 : 단일 ADMIN 이라면 단지 시스템에 등록된 Operator 리스트일 뿐이다.
     */
    public function selectOperatorListByAdminOperatorIdx($params)
    {
        //dd($params);
        return DB::table('operators')
            ->join('roles', 'operators.op_level', '=', 'roles.admin_level')
            ->where('op_parent_idx', '=', $params['op_idx'])
            ->orderBy('signup_date', 'desc')
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * Operator 의 상세정보
     */
    public function selectOperatorDetailByOperatorIdx($params)
    {
        //dd($params);
        return DB::table('operators')
            ->join('roles', 'operators.op_level', '=', 'roles.admin_level')
            ->where('op_idx', '=', $params['op_idx'])
            ->first();
    }

    /**
     * @param $updateFields
     * @param $where_Fields
     * @return mixed
     *
     * Operator 의 상세정보 갱신
     */
    public function updateOperatorDetailInfo($updateFields, $where_Fields)
    {
        return DB::table('operators')
            ->where([['op_id', '=', $where_Fields['op_id']], ['op_idx', '=', $where_Fields['op_idx']]])
            ->update($updateFields);
    }


    /**
     * @param $params
     * @return mixed
     *
     * 신규 사용자생성( 현재는 Operator )
     */
    public function insertNewMemberInfo($params)
    {
        return DB::table('operators')
            ->insertGetId($params);
    }


    /**
     * @param $updateFields
     * @param $where_Fields
     */
    public function updateOperatorPoint($updateFields, $where_Fields)
    {
        DB::table('operators')
            ->where([['op_id', '=', $where_Fields['op_id']], ['op_idx', '=', $where_Fields['op_idx']]])
            ->update($updateFields);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function selectOperatorNameByUserName($params)
    {
        return DB::table('operators')
            ->where('op_name', '=', $params['op_name'])
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function selectOperatorEmailByUserEmail($params)
    {
        //dd($params);
        return DB::table('operators')
            ->where('op_email', '=', $params['op_email'])
            ->count();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function selectOperatorIdByInputId($params)
    {
        //dd($params);
        return DB::table('operators')
            ->where('op_id', '=', $params['op_id'])
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     */
    public function selectOperatorPrefixByUserPrefix($params)
    {
        return DB::table('operators')
            ->where('op_prefix', '=', $params['op_prefix'])
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     *
     * operator agent 리스트
     */
    public function selectAgentListByAdminOperatorIdx($params)
    {
        return DB::table('operators')
            ->join('roles', 'operators.op_level', '=', 'roles.admin_level')
            ->where([['op_parent_idx', '=', $params['op_idx']], ['op_level','=',$params['op_level']]])
            ->orderBy('signup_date', 'desc')
            ->get();
    }



    /**
     * @param $params
     * @return mixed
     *
     * ADMIN 레벨의 Operator 리스트 : 단일 ADMIN 이라면 단지 시스템에 등록된 Operator 리스트일 뿐이다. super master agent level 제외한 list
     */
    public function selectNewOperatorListByAdminOperatorIdx($params)
    {
        //dd($params);
        return DB::table('operators')
            ->join('roles', 'operators.op_level', '=', 'roles.admin_level')
            ->where([['op_parent_idx', '=', $params['op_idx']],['op_level', '=', 3]])
            ->orderBy('signup_date', 'desc')
            ->get();
    }


    public function selectNewOperatorListByAdminOperatorId($params)
    {
    //dd($params);
        return DB::table('operators')
            ->join('roles', 'operators.op_level', '=', 'roles.admin_level')
           // ->where([['op_level', '=', 3]])
            ->when($params['op_id'], function ($query) use ($params) {
                return $query->whereIn('operators.op_id', $params['op_id']);
            })

            ->orderBy('signup_date', 'desc')
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     *
     * 입력받은 파라미터 operator 아이디를 포함한 다운라인 operator or super master agent 아이디 리스트를 반환
     */

    public function downLineListInOpIdx($params)
    {
        $query = '
                   select op_idx, op_id, op_parent_idx  from (
                        SELECT  TRUE AS ancestry, _id AS op_idx, op_id, op_parent_idx, level
                                        FROM    (
                        SELECT  @r AS _id,
                                                        (
                                                        SELECT  @r := op_parent_idx
                                                        FROM    operators
                                                        WHERE   op_idx = _id
                                                        ) AS parent,
                                                        @l := @l + 1 AS level
                                                FROM    (
                                                        SELECT  @r :=  '.$params['op_idx'].',
                                                                @l := 0,
                                                                @cl := 0
                                                        ) vars,
                                                        operators h
                                                WHERE   @r <> 0
                                                ORDER BY
                                                        level DESC
                        ) A join operators B on A._id = B.op_idx
                        union all
                        SELECT  FALSE, hi.op_idx, op_id, op_parent_idx, level
                                        FROM    (
                                                SELECT  hierarchy_connect_by_parent_eq_prior_id(op_idx) AS id, @level AS level
                                                FROM    (
                                                        SELECT  @start_with := '.$params['op_idx'].',
                                                                @id := @start_with,
                                                                @level := 0
                                                        ) vars, operators
                                                WHERE   @id IS NOT NULL
                                                ) ho
                                        JOIN    operators hi
                                        ON      hi.op_idx = ho.id
                   ) TA where op_idx >=  '.$params['op_idx'].'
                   ';

        return DB::select($query);
    }

    /**
     * @param $params
     * @return mixed
     *
     * 입력받은 파라미터 operator 아이디를 포함하지 않은 다운라인 operator or super master agent 아이디 리스트를 반환
     */
    public function downLineListNotInOpIdx($params)
    {
        $query = '
                   select op_idx, op_id, op_parent_idx  from (
                        SELECT  TRUE AS ancestry, _id AS op_idx, op_id, op_parent_idx, level
                                        FROM    (
                        SELECT  @r AS _id,
                                                        (
                                                        SELECT  @r := op_parent_idx
                                                        FROM    operators
                                                        WHERE   op_idx = _id
                                                        ) AS parent,
                                                        @l := @l + 1 AS level
                                                FROM    (
                                                        SELECT  @r :=  '.$params['op_idx'].',
                                                                @l := 0,
                                                                @cl := 0
                                                        ) vars,
                                                        operators h
                                                WHERE   @r <> 0
                                                ORDER BY
                                                        level DESC
                        ) A join operators B on A._id = B.op_idx
                        union all
                        SELECT  FALSE, hi.op_idx, op_id, op_parent_idx, level
                                        FROM    (
                                                SELECT  hierarchy_connect_by_parent_eq_prior_id(op_idx) AS id, @level AS level
                                                FROM    (
                                                        SELECT  @start_with := '.$params['op_idx'].',
                                                                @id := @start_with,
                                                                @level := 0
                                                        ) vars, operators
                                                WHERE   @id IS NOT NULL
                                                ) ho
                                        JOIN    operators hi
                                        ON      hi.op_idx = ho.id
                   ) TA where op_idx >  '.$params['op_idx'].'
                   ';

        return DB::select($query);
    }

}
