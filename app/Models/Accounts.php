<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Accounts extends Model
{
    /**
     * @param $params
     * @return mixed
     *
     *정산리스트
     */
    public function selectAccountRequest($params)
    {
//        dd($params);
        return DB::table('game_history')
            ->select(
                'game_history.op_id',
                'operators.op_idx',
                'operators.op_name',
                'operators.op_profit',
                'operators.op_parent_idx',
                'operators.op_cash_type',
                DB::raw('sum(case when game_history.trs_type = \'BET\' then game_history.trs_amount end ) `bets`'),
                DB::raw('sum(case when game_history.trs_type = \'WIN\' then game_history.trs_amount end ) `wins`'),
                DB::raw(
                    'sum(case when game_history.trs_type = \'BET\' then game_history.trs_amount end )-
                    sum(case when game_history.trs_type = \'WIN\' then game_history.trs_amount end ) `betwin`'),
                DB::raw(
                    'ROUND(sum(case when game_history.trs_type = \'WIN\' then game_history.trs_amount end )/
                    sum(case when game_history.trs_type = \'BET\' then game_history.trs_amount end )*100, 2) `payout`'),
                DB::raw('ROUND((sum(case when game_history.trs_type = \'BET\' then game_history.trs_amount end )-
                    sum(case when game_history.trs_type = \'WIN\' then game_history.trs_amount end ))*operators.op_profit/100, -1) `amount`')
            )
            ->join('operators', 'game_history.op_id', '=', 'operators.op_id')
            ->when($params['startdate'] && $params['enddate'], function ($query) use ($params){
                return $query->whereBetween('game_history.trs_date', [$params['startdate'], $params['enddate']]);
            })
            //->whereBetween('game_history.trs_date', [$params['startdate'], $params['enddate']])
//            ->when($params['op_idx'], function ($query) use ($params) {
//                return $query->where('operators.op_idx', '=', $params['op_idx']);
//            })
            ->when($params['op_idx'], function ($query) use ($params) {
                return $query->whereIn('operators.op_idx', $params['op_idx']);
            })

            ->groupBy('game_history.op_id')->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 정산기간 리스트
     */
    public function selectAccountDateList($params)
    {
        return DB::table('accounts')
            ->select(DB::raw('acc_start_date sdate, acc_end_date edate'))
            ->where([['op_idx', '=', $params['reqOpIdx']], ['acc_type', '=', $params['reqAccType']]])
            ->whereIn('acc_status', ['REQUESTED', 'CONFIRMED'])
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 정산완료 리스트
     */
    public function selectConfirmedAccountList($params)
    {
        return DB::table()
            ->where('acc_status', '=', 'CONFIRMED')
            ->whereIn('op_idx', $params)
            ->orderBy('acc_change_date', 'desc')
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 정산요청 등록
     */
    public function insertAccountRequest($params)
    {
        return DB::table('accounts')->insertGetId($params);
    }


    /**
     * @param $params
     * @param $whenParam
     * @return mixed
     *
     * 정산요청 리스트
     */
    public function selectAccountRequestedList($whenParam, $params)
    {
        return DB::table('accounts')
            //->where('acc_type', '=', 'BETWIN')
            ->whereIn('op_idx', $params)
            ->when($whenParam['acc_status'], function ($query) use ($whenParam) {
                return $query->where('acc_status', $whenParam['acc_status']);
            })
            ->when($whenParam['acc_type'], function ($query) use ($whenParam) {
                return $query->where('acc_type', $whenParam['acc_type']);
            })
            ->orderBy('acc_req_date', 'desc')
            ->get();
    }

    /**
     * @param $updateParams
     * @param $whereParam
     * @return mixed
     *
     * 정산요청 처리 (Confirm, Restore)
     */
    public function updateAccountRequestStatus($updateParams, $whereParam)
    {
        return DB::table('accounts')
            ->where('acc_id', '=', $whereParam['acc_id'])->update($updateParams);
    }


    /**
     * @param $params
     * @return mixed
     *
     * 롤링정산 리스트
     */
    public function selectRollingAccountRequest($params)
    {
        return DB::table('game_history_detail')
            ->select(
                'operators.op_idx',
                'operators.op_name',
                'game_history.op_id',
                DB::raw('SUM(game_history_detail.trs_amount) bets'),
                'operators.op_rolling'
            )
            ->join('game_result', 'game_history_detail.period_id', '=', 'game_result.g_trs_id')
            ->join('game_history', 'game_history_detail.gh_idx', '=', 'game_history.gh_idx')
            ->join('operators', 'game_history.op_id', '=', 'operators.op_id')
            ->where([
                ['game_history_detail.trs_type', '=', 'BET'],
                ['operators.op_rolling', '>', 0]
            ])
            ->whereBetween('game_history_detail.trs_date', [$params['startdate'], $params['enddate']])
            ->whereIn('operators.op_idx', $params['arrayIdx'])
            ->groupBy('game_history.op_id')
            ->get();
    }

    /**
     * @param $params
     * @return mixed
     *
     * 롤링정산 타이결과의 타이를 제외한 배팅금액(무승부 금액)
     */
    public function selectUserPayBackAmountByTie($params)
    {
        //dd($params);
        return DB::table('game_history_detail')
            ->select(DB::raw('SUM(game_history_detail.trs_amount) bets'))
            ->join('game_result', 'game_history_detail.period_id', '=', 'game_result.g_trs_id')
            ->join('game_history', 'game_history_detail.gh_idx', '=', 'game_history.gh_idx')
            ->whereBetween('game_history_detail.trs_date', [$params['startdate'], $params['enddate']])
            ->whereIn('game_result.g_result', ['BA8E', 'BA8O'])
            ->where([
                ['game_history_detail.trs_type', '=', 'WIN'],
                ['game_history_detail.bet_location', '<>', 'TIE8'],
                ['game_history.op_id', '=', $params['op_id']]
            ])
            ->groupBy('game_history.op_id')
            ->first();
    }
}
