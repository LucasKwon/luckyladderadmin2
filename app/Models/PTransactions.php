<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PTransactions extends Model
{
    protected $table = 'ptransactions';
    protected $primaryKey = 'ptrs_id';
    public $timestamps = false;

    public function insertPointTransactions($params)
    {
        return DB::table('ptransactions')->insertGetid($params);
    }
}
