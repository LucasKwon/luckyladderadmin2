<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class History extends Model
{
    protected $primaryKey = 'gh_idx';

    /**
     * @param $params
     * @return mixed
     *
     * 게임 BET/WIN 히스토리 리스트
     */
    public function selectBetWinHistoryList1($params)
    {
//        dd($params);

        return DB::table('game_history')
            ->join('operators', 'operators.op_id', '=', 'game_history.op_id')
            ->join('roles', 'roles.admin_level', '=', 'operators.op_level')
            ->where('operators.op_parent_idx', '=', $params['op_idx'])
            ->when($params['checkYnArr'], function ($query) use ($params) {
                return $query->whereIn('game_history.check_result', $params['checkYnArr']);
            })
            ->when($params['startdate'] && $params['enddate'], function ($query) use ($params) {
                return $query->whereBetween('game_history.trs_date', [$params['startdate'], $params['enddate']]);
            })
            ->when($params['mbLoginId'], function ($query) use ($params) {
                return $query->where('game_history.mb_id', '=', $params['mbLoginId']);
            })
            ->when($params['whereInArr'], function ($query) use ($params) {
                return $query->whereIn('game_history.op_id', $params['whereInArr']);
            })
            ->orderBy('game_history.trs_date', $params['lineUp'])
            ->take($params['limit'])->skip($params['offset'])
            ->get();
    }

    public function selectBetWinHistoryList3($params)
    {
//        dd($params);

        return DB::table('game_history')
            ->join('operators', 'operators.op_id', '=', 'game_history.op_id')
            ->join('roles', 'roles.admin_level', '=', 'operators.op_level')
            ->when($params['opIdxArr'], function ($query) use ($params) {
                return $query->whereIn('operators.op_idx', $params['opIdxArr']);
            })
//            ->whereIn('operators.op_parent_idx',  $params['opIdxArr'])
            ->when($params['checkYnArr'], function ($query) use ($params) {
                return $query->whereIn('game_history.check_result', $params['checkYnArr']);
            })
            ->when($params['startdate'] && $params['enddate'], function ($query) use ($params) {
                return $query->whereBetween('game_history.trs_date', [$params['startdate'], $params['enddate']]);
            })
            ->when($params['mbLoginId'], function ($query) use ($params) {
                return $query->where('game_history.mb_id', '=', $params['mbLoginId']);
            })
            ->when($params['whereInArr'], function ($query) use ($params) {
                return $query->whereIn('game_history.op_id', $params['whereInArr']);
            })
            ->orderBy('game_history.trs_date', $params['lineUp'])
            ->take($params['limit'])->skip($params['offset'])
            ->get();
    }

    public function selectBetWinHistoryList2($params)
    {
//        dd('>',$params);

        return DB::table('game_history')
            ->join('operators', 'operators.op_id', '=', 'game_history.op_id')
            ->join('roles', 'roles.admin_level', '=', 'operators.op_level')
            ->where('operators.op_idx', '=', $params['op_idx'])
            ->when($params['checkYnArr'], function ($query) use ($params) {
                return $query->whereIn('game_history.check_result', $params['checkYnArr']);
            })
            ->when($params['startdate'] && $params['enddate'], function ($query) use ($params) {
                return $query->whereBetween('game_history.trs_date', [$params['startdate'], $params['enddate']]);
            })
            ->when($params['mbLoginId'], function ($query) use ($params) {
                return $query->where('game_history.mb_id', '=', $params['mbLoginId']);
            })
            ->when($params['whereInArr'], function ($query) use ($params) {
                return $query->whereIn('game_history.mb_id', $params['whereInArr']);
            })
            ->orderBy('game_history.trs_date', 'asc')
            ->take($params['limit'])->skip($params['offset'])
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     *
     * 게임 BET/WIN 베팅 상세정보
     */
    public function selectBetWinHistoryDetail($params)
    {
        //dd($params);
        return DB::table('game_history_detail')
            ->when($params['trsId'], function ($query) use ($params) {
                return $query->where('game_history_detail.trs_id', '=', $params['trsId']);
            })
            ->when($params['trsType'], function ($query) use ($params) {
                return $query->where('game_history_detail.trs_type', '=', $params['trsType']);
            })
            ->when($params['periodId'], function ($query) use ($params) {
                return $query->where('game_history_detail.period_id', '=', $params['periodId']);
            })
            ->when($params['checkResult'], function ($query) use ($params) {
                return $query->where('game_history_detail.check_result', '=', $params['checkResult']);
            })
            ->get();
    }


    /**
     * @param $params
     * @return mixed
     *
     * 게임결과 정보
     */
    public function selectGameResultDetail($params)
    {
        return DB::table('game_result')
            ->where('g_trs_id', '=', $params['periodId'])
            ->first();
    }
}
