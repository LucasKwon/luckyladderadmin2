<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Roles extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function selectRoleShortName($params)
    {
        return DB::table('roles')
            ->select('role_short_name')
            ->where('admin_level','=', $params['admin_level'])
            ->first();
    }
}
