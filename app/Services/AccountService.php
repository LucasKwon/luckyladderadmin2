<?php

namespace App\Services;


use App\Models\Accounts;
use App\Models\Operators;
use App\Services\CommonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AccountService
{

    protected $accountModel;
    protected $operatorModel;
    protected $commonService;

    /**
     * @param $accounts
     * @param $operators
     * AccountService constructor.
     */
    public function __construct(Accounts $accounts, Operators $operators, CommonService $commonService)
    {
        $this->accountModel = $accounts;
        $this->operatorModel = $operators;
        $this->commonService =  $commonService;
    }


    /**
     * @param $request
     * @return array|null
     *
     * 정산리스트
     */
    public function getViewAccountRequest($request)
    {
        $result = array(
            'accountingDetail' => [],
            'accountingList' => [],
            'accRollingDetail' => [],
            'accRollingList' => []
        );

        $idxList = array();
        $opList = null;

        //accountingList
        $whereParam = array(
            'acc_status'    => '',
            'acc_type'      => ''
        );


        if(Auth::user()->admin_level === 2){

            $downParams = array(
                'op_idx' => $request['op_idx'],
            );
            $downLineList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($downParams);
            if($downLineList != null) {
                $opList = $this->commonService->makeStringWhereInArr($downLineList, 'op_idx', 'Y');
            }
            else{
                $opList = null;
            }

            if($opList != null)
            {
                foreach ($opList as $item) {
                    array_push($idxList, $item);
                }
            }
            else
            {
                array_push($idxList, $request['op_idx']);
            }

        }
        else{


            $downParams = array(
                'op_idx' => $request['op_idx'],
            );
            $downLineList = $this->operatorModel->downLineListNotInOpIdx($downParams);
            if($downLineList != null) {
                $opList = $this->commonService->makeStringWhereInArr($downLineList, 'op_idx', 'Y');
            }
            else{
                $opList = null;
            }

            if($opList != null)
            {
                foreach ($opList as $item) {
                    array_push($idxList, $item);
                }
            }
            else
            {
                array_push($idxList, $request['op_idx']);
            }

        }


//dd($idxList);


        $whereParam['acc_status']   = 'CONFIRMED';
        $whereParam['acc_type']     = 'BETWIN';

        /* Account Bet / Win List  */
        $result['accountingList'] = $this->accountModel->selectAccountRequestedList($whereParam, $idxList);

//        dd($result['accountingList']);

        $whereParam['acc_status'] = 'CONFIRMED';
        $whereParam['acc_type'] = 'ROLLING';
        //dd($whereParam);

        /* Account Rolling List */
        $result['accRollingList'] = $this->accountModel->selectAccountRequestedList($whereParam, $idxList);

//        dd($result['accountingList'], $result['accRollingList']);


        /* 날짜 검색 조건이 있을때 만 조회 시작 */
        if (!empty($request['startdate']) && !empty($request['enddate'])) {

            $params = array(
                'op_idx' => null,
                'startdate' => null,
                'enddate' => null
            );

            if (Auth::user()->admin_level === 2) {

                $downParams = array(
                    'op_idx' => $request['op_idx'],
                );
                $downLineList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($request);
                $opList = $this->commonService->makeStringWhereInArr($downLineList, 'op_idx', 'Y');


                $accountDetailOpIdx = array();
                $accountResultDetail = array();
                $rollingResultDetail = array();
                foreach($opList as $items){

                    $downParams = null;
                    $downParams = array(
                        'op_idx' => $items,
                    );
//                    $downDetailLineList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($downParams);
                    $downDetailLineList = $this->operatorModel->downLineListInOpIdx($downParams);


                    if($downDetailLineList != null)
                    {

                      $reaultDetailOpIdxArr= $this->commonService->makeStringWhereInArr($downDetailLineList, 'op_idx', 'Y');
                        $accountDetailOpIdx = [];
                      foreach ($reaultDetailOpIdxArr as $detailItem){
                          array_push($accountDetailOpIdx,  $detailItem);
                      }

                    }
                    else{
                        array_push($accountDetailOpIdx,  $items);
                    }

                    $params['op_idx'] = $accountDetailOpIdx;
                    $params['startdate'] = !empty($request['startdate']) ? $request['startdate'] . ' 00:00:00' : null;
                    $params['enddate'] = !empty($request['enddate']) ? $request['enddate'] . ' 23:59:59' : null;


                    $resultaccountingDetail = $this->accountModel->selectAccountRequest($params);

                    if($resultaccountingDetail != null){


                        $resutBetsSum = 0;
                        $resutWinsSum = 0;
                        $resutBetWinSum = 0;
                        $resutAmountSum = 0;
                        $resultLastDetailArr = array();
                        $resultLastDetailArr = null;
                        $getResult=null;
                        foreach ( $resultaccountingDetail as $resultItem) {

//                            dd($resultItem);
                            if($getResult == null){
                                $getResult= $resultItem;
                            }

                            $resutBetsSum += $resultItem->bets;
                            $resutWinsSum += $resultItem->wins;
                            $resutBetWinSum += $resultItem->betwin;
                            $resutAmountSum += $resultItem->amount;


                            $resultItem->bets = $resutBetsSum;
                            $resultItem->wins = $resutWinsSum;
                            $resultItem->betwin = $resutBetWinSum;
                            $resultItem->amount = $resutAmountSum;

                            if($resultLastDetailArr == null) {
                                $getResult->op_id = $resultItem->op_id;
                                $getResult->op_idx = $resultItem->op_idx;
                                $getResult->op_name = $resultItem->op_name;
                                $getResult->op_profit = $resultItem->op_profit;
                                $getResult->payout = $resultItem->payout;
                            }
                            else{

                                $getResult->bets = $resutBetsSum;
                                $getResult->wins = $resutWinsSum;
                                $getResult->betwin = $resutBetWinSum;
                                $getResult->amount = $resutAmountSum;
                            }

//                            dd($getResult);
                            $resultLastDetailArr= $getResult;

                        }
//                        dd($resultLastDetailArr);
                        array_push($accountResultDetail, $resultLastDetailArr );

                    }

                    /* Rolling Detail */

                    $rollParams = array(
                        'startdate' => $request['startdate'] . ' 00:00:00',
                        'enddate' => $request['enddate'] . ' 23:59:59',
                        'arrayIdx' => $accountDetailOpIdx
                    );

                    $rollingResult = $this->accountModel->selectRollingAccountRequest($rollParams);


                    if($rollingResult != null){
//                        dd($rollingResult, $accountDetailOpIdx);

                        $rollingBetsSum = 0;
                        $rollingBetAmountSum = 0;
                        $rollingRolAmountSum = 0;
                        $rollingTieAmountSum = 0;
                        $rollingLastDetailArr = [];
                        $rollingLastDetailArr = null;
                        $getRollingResult=null;

                        foreach ($rollingResult as $item) {

//                            dd($item);

                            if($getRollingResult == null){
                                $getRollingResult = $item;
                            }

                            $payBackParams = array(
                                'startdate' => $request['startdate'],
                                'enddate' => $request['enddate'],
                                'op_id' => $item->op_id
                            );

                            $noTieBets = $this->accountModel->selectUserPayBackAmountByTie($payBackParams);
                            $noTieBets = is_null($noTieBets) ? '0' : $noTieBets->bets;
                            $item->bet_amount = $item->bets;
                            $item->rol_amount = $item->bets - $noTieBets;
                            $item->tie_amount = $noTieBets;

                            $rollingBetsSum += $item->bets;
                            $rollingBetAmountSum +=  $item->bet_amount;
                            $rollingRolAmountSum += $item->rol_amount;
                            $rollingTieAmountSum += $item->tie_amount;

                            if($rollingLastDetailArr == null) {
                                $getRollingResult->op_id = $item->op_id;
                                $getRollingResult->op_idx = $item->op_idx;
                                $getRollingResult->op_name = $item->op_name;
                                $getRollingResult->op_rolling = $item->op_rolling;
                            }
                            else{

                                $getRollingResult->bets = $rollingBetsSum;
                                $getRollingResult->bet_amount = $rollingBetAmountSum;
                                $getRollingResult->rol_amount = $rollingRolAmountSum;
                                $getRollingResult->tie_amount = $rollingTieAmountSum;
                            }

//                            dd($getResult);
                            $rollingLastDetailArr= $getRollingResult;
//
//                            if($rollingLastDetailArr == null) {
//                                $rollingLastDetailArr['op_id'] = $item->op_id;
//                                $rollingLastDetailArr['op_idx'] = $item->op_idx;
//                                $rollingLastDetailArr['op_name'] = $item->op_name;
//                                $rollingLastDetailArr['op_rolling'] = $item->op_rolling;
//
//                            }
//                            $rollingLastDetailArr['bets'] = $rollingBetsSum;
//                            $rollingLastDetailArr['bet_amount'] = $rollingBetAmountSum;
//                            $rollingLastDetailArr['rol_amount'] = $rollingRolAmountSum;
//                            $rollingLastDetailArr['tie_amount'] = $rollingTieAmountSum;

                        }

                        array_push($rollingResultDetail, $rollingLastDetailArr);
                    }




                }
                $result['accountingDetail'] = $accountResultDetail;
                $result['accRollingDetail'] = $rollingResultDetail;
//                dd($result);



            } else {//if (Auth::user()->admin_level === 3 ) {

                $resultOpIdx = array();
                $downParams = array(
                    'op_idx' => $request['op_idx'],
                );
                $downLineList = $this->operatorModel->downLineListNotInOpIdx($downParams);
                if ($downLineList != null) {
                    $resultOpIdx = $this->commonService->makeStringWhereInArr($downLineList, 'op_idx', 'Y');
                } else {
//                    $resultOpIdx= $request['op_idx'];
                    array_push($resultOpIdx, $request['op_idx']);
                }

//                dd($downLineList, $resultOpIdx);
//                $params['op_idx']       = $request['op_idx'];
                $params['op_idx'] = $resultOpIdx;
                $params['startdate'] = $request['startdate'] . ' 00:00:00';
                $params['enddate'] = $request['enddate'] . ' 23:59:59';


                if (Auth::user()->admin_level != 4) {
                    /* Bet/win 조회 */
                    $result['accountingDetail'] = $this->accountModel->selectAccountRequest($params);

//            dd( $result['accountingDetail'], $params);

                    /* rolling accounting 조회 시작 */
                    $rollParams = array(
                        'startdate' => $request['startdate'] . ' 00:00:00',
                        'enddate' => $request['enddate'] . ' 23:59:59',
                        'arrayIdx' => $idxList
                    );

                    $rollingResult = $this->accountModel->selectRollingAccountRequest($rollParams);

                    foreach ($rollingResult as $item) {
                        $payBackParams = array(
                            'startdate' => $request['startdate'],
                            'enddate' => $request['enddate'],
                            'op_id' => $item->op_id
                        );

                        $noTieBets = $this->accountModel->selectUserPayBackAmountByTie($payBackParams);
                        $noTieBets = is_null($noTieBets) ? '0' : $noTieBets->bets;
                        $item->bet_amount = $item->bets;
                        $item->rol_amount = $item->bets - $noTieBets;
                        $item->tie_amount = $noTieBets;
                    }

//            dd($rollingResult);
                    $result['accRollingDetail'] = $rollingResult;
                    /* rolling accounting 조회 종료 */

                }
            }



        }
        /* 날짜 검색 조건이 있을때 만 조회 종료 */
//        dd($result);
        return $result;
    }

    /**
     * @param $request
     * @return array
     *
     * 정산요청 리스트 확인
     */
    public function getViewAccountRequested($request)
    {
        $result = array(
            'accountingList' => []
        );
        $idxList = array();

        if (Auth::user()->op_level === 2 || Auth::user()->admin_level === 3 ) {
            $request['op_idx'] = $request['opIdx'];
            $opList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($request);
            foreach ($opList as $item) {
                array_push($idxList, $item->op_idx);
            }
        } elseif ( Auth::user()->admin_level === 4) {
            array_push($idxList, $request['opIdx']);
        }

//        dd($idxList);
        $whenParam = array(
            'acc_status' => null,
            'acc_type' => null
        );

        $result['accountingList'] = $this->accountModel->selectAccountRequestedList($whenParam, $idxList);
        return $result;
    }


    /**
     * @param $request
     * @return array
     *
     * 정산요청 등록
     */
    public function addAccountRequest($request)
    {
        $result = array(
            'result' => 0,
            'message' => ''
        );

        $checkDate = $this->checkRegisteredAccountDate($request);

        if ($checkDate > 0) {
            $result['message'] = '이미 정산요청을 했거나, 정산기간이 중복됩니다.';
        } else {
            $params = array(
                'op_idx' => $request['reqOpIdx'],
                'op_id' => $request['reqOpId'],
                'acc_type' => 'BETWIN',
                'bet_amount' => $request['reqBets'],
                'win_amount' => $request['reqWins'],
                'acc_deal' => $request['reqOpProfit'],
                'acc_amount' => $request['reqAmount'],
                'pay_amount' => $request['reqPayout'],
                'acc_req_date' => Carbon::now(),
                'acc_start_date' => $request['reqStartDate'],
                'acc_end_date' => $request['reqEndDate'],
                'acc_status' => 'REQUESTED'

            );
            //dd($params);
            $this->accountModel->insertAccountRequest($params);
            $result['message'] = '정산요청을 정상적으로 등록했습니다.';
            $result['result'] = 1;
        }
        return $result;
    }

    /**
     * @param $params
     * @return int|null
     *
     * 정산기간 체크
     */
    public function checkRegisteredAccountDate($params)
    {
        $accList = $this->accountModel->selectAccountDateList($params);

        $result = 0;

        foreach ($accList as $list) {
            if ($list->sdate <= $params['reqStartDate'] && $list->edate >= $params['reqStartDate']) {
                $result += 1;
            }

            if ($list->sdate <= $params['reqEndDate'] && $list->edate >= $params['reqEndDate']) {
                $result += 1;
            }
        }

        return $result;
    }

    /**
     * @return array
     *
     * 정산요청 상태 업데이트 - Confirm, Restore
     */
    public function updateAccountRequestStatus($request)
    {
        $result = array(
            'result' => 0,
            'message' => ''
        );

        $updateParams = array(
            'acc_status' => ($request['reqStatus'] == 'C') ? 'CONFIRMED' : 'RESTORED',
            'acc_change_date' => Carbon::now()
        );

        $whereParam = array(
            'acc_id' => $request['reqAccId']
        );

        $this->accountModel->updateAccountRequestStatus($updateParams, $whereParam);
        $result['message'] = '정산요청을 성공적으로 처리했습니다.';
        $result['result'] = 1;
        return $result;
    }


    /**
     * @param $request
     * @return array
     */
    public function addRollAccountRequest($request)
    {
        $result = array(
            'result' => 0,
            'message' => ''
        );

        $checkDate = $this->checkRegisteredAccountDate($request);

        if ($checkDate > 0) {
            $result['message'] = '이미 정산요청을 했거나, 정산기간이 중복됩니다.';
        } else {
            $params = array(
                'op_idx' => $request['reqOpIdx'],
                'op_id' => $request['reqOpId'],
                'acc_type' => 'ROLLING',
                'bet_amount' => $request['reqBetAmount'],
                'tie_amount' => $request['reqTieAmount'],
                'acc_deal' => $request['reqOpRolling'],
                'acc_amount' => $request['reqAccAmount'],
                'acc_req_date' => Carbon::now(),
                'acc_start_date' => $request['reqStartDate'],
                'acc_end_date' => $request['reqEndDate'],
                'acc_status' => 'REQUESTED'

            );

            $this->accountModel->insertAccountRequest($params);
            $result['message'] = '정산요청을 정상적으로 등록했습니다.';
            $result['result'] = 1;
        }

        return $result;
    }
}