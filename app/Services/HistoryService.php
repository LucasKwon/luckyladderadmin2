<?php

namespace App\Services;


use App\Models\History;
use App\Models\Members;
use App\Models\Operators;
use App\Services\CommonService;

class HistoryService
{
    protected $historyModel;
    protected $operatorModel;
    protected $memberModel;
    protected $commonService;

    /**
     * HistoryService constructor.
     * @param History $history
     * @param Operators $operators
     * @param Members $members
     */
    public function __construct(History $history, Operators $operators, Members $members, CommonService $commonService)
    {
        $this->historyModel = $history;
        $this->operatorModel = $operators;
        $this->memberModel = $members;
        $this->commonService =$commonService;
    }

    /**
     * @param $request
     * @return array
     */
    public function getBetWinHistory($request)
    {
//        dd($request);

        $getOp_idx = $request['op_idx'];

        $downparams = array(
            'op_idx' => $request['op_idx'],
        );

        if($request['op_level'] == '2'){
            /* 선택된 operator id 로 operator_idx 값 조회*/

            if(!empty($request['vendoridarr'])) {
                $opidArr = array(
                    'op_id' => explode(',', $request['vendoridarr']),
                );

                $resultOpInfo = $this->operatorModel->selectNewOperatorListByAdminOperatorId($opidArr);
                $whereInParentIdxArr = $this->commonService->makeStringWhereInArr($resultOpInfo, 'op_idx', 'Y');

                $resultParentIdxArr = array();
                $resultParentIdArr = null;
                /* 선택된 오퍼레이터 op_idx 값으로 하부 오퍼레이터 가져오기 시작 */
                foreach ($whereInParentIdxArr as $items) {

                    $loopDownParams = null;
                    $loopDownParams = array(
                        'op_idx' => $items,
                    );
                    $loopDownLineList = $this->operatorModel->downLineListInOpIdx($loopDownParams);
                    /* 하부 오퍼레이터 array , 오퍼레이터 아이디 추가 시작*/
                    foreach ($loopDownLineList as $arrItem) {

                        if (!empty($arrItem->op_idx)) {
                            array_push($resultParentIdxArr, $arrItem->op_idx);
                        }

                        if (!empty($arrItem->op_id)) {
                            if (empty($resultParentIdArr)) {
                                $resultParentIdArr = $arrItem->op_id;
                            } else {
                                $resultParentIdArr = $resultParentIdArr . "," . $arrItem->op_id;
                            }
                        }
                    }
                    /* 하부 오퍼레이터 array , 오퍼레이터 아이디 추가 종료*/

                }

                /* 선택된 오퍼레이터 op_idx 값으로 하부 오퍼레이터 가져오기 종료 */

                /* 선택된 operator id 하부 오퍼레이터 op_idx */
                $getOp_idx = $resultParentIdxArr;

                /* 선택된 operator id 하부 오퍼레이터 op_id */
                $request['vendoridarr'] = $resultParentIdArr;

            }
            else{
                $getOp_idx = null;
            }

        }
        elseif($request['op_level'] == '3')
        {
            $downLineList = $this->operatorModel->downLineListInOpIdx($downparams);
            $whereInParentIdxArr= $this->commonService->makeStringWhereInArr($downLineList, 'op_idx','Y');/* op_idx 만 가져온다 */
            $getOp_idx = $whereInParentIdxArr;
        }



        $params = array(
             'whereInArr' => empty($request['vendoridarr']) ? null : explode(',', $request['vendoridarr']),
            'startdate' => empty($request['startdate']) ? null : $request['startdate'] . ' 00:00:00',
            'enddate' => empty($request['enddate']) ? null : $request['enddate'] . ' 23:59:59',
            'mbLoginId' => empty($request['mbLoginId']) ? null : $request['mbLoginId'],
            'limit' => empty($request['limit']) ? 1000 : $request['limit'],
            'offset' => empty($request['offset']) ? 0 : $request['offset'],
            'op_idx' => $request['op_idx'],
            'checkYnArr' => empty($request['checkynarr']) ? null : explode(',', $request['checkynarr']),
            'lineUp' => empty($request['lineUp']) ? 'ASC' : $request['lineUp'],
            'opIdxArr' => $getOp_idx,
        );


//        dd($params, $request['op_level']);
//dd($downLineList);
//        dd($downLineList, $downLineListNot);
//dd($request['op_level']);
//        $returnParams = array(
//            'betwinList' => ($request['op_level'] == '2') ?
//                $this->historyModel->selectBetWinHistoryList1($params) :
//                $this->historyModel->selectBetWinHistoryList2($params),
//            'operatorList' => ($request['op_level'] == '2') ?
//                $this->operatorModel->selectOperatorListByAdminOperatorIdx($params) :
//                $this->memberModel->selectPlayerListByAdminOperatorIdx($params),
//        );

        $returnBetWinList = null;
        $returnOperatorList = null;

        if($request['op_level'] == '2')
        {
            $returnBetWinList = $this->historyModel->selectBetWinHistoryList3($params);
            $returnOperatorList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($params); // 직속하부의 super master agent id 포함한 오퍼레이터 id
//            $returnOperatorList = $this->operatorModel->selectNewOperatorListByAdminOperatorIdx($params); //직속하부의 super master agent id 제외한 오퍼레이터 id

        }
        elseif( $request['op_level'] == '3'){
            $returnBetWinList = $this->historyModel->selectBetWinHistoryList3($params);
            $returnOperatorList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($downparams);
        }
        else
        {
            $returnBetWinList = $this->historyModel->selectBetWinHistoryList2($params);
            $returnOperatorList = $this->memberModel->selectPlayerListByAdminOperatorIdx($params);
        }

//        dd($this->operatorModel->selectNewOperatorListByAdminOperatorIdx($params));
//dd($returnOperatorList);
        $returnParams = array(
            'betwinList' => $returnBetWinList,
            'operatorList' => $returnOperatorList,
        );

        return $returnParams;

    }

    /**
     * @param $params
     * @return array
     *
     * BET 에 해당하는 상세(베팅위치 등) 베팅정보
     */
    public function getBetWinDetailInfo($params)
    {

        //dd($params);

        $betWinDetail = $this->historyModel->selectBetWinHistoryDetail($params);

        if (count($betWinDetail) == 0) {
            $result = array(
                'gameResult' => 'no result',
                'betLocation' => 'no result',
                'betWinAmount' => 'no result',
                'gameTrsId' => 'no result',
                'gameDate' => 'no result',
                'resultDate' => 'no result',
                'gamePrdId' => 'no result',
            );
        } else {
            //dd($betWinDetail);
            $betLocation = "";
            $betWinAmount = 0;
            for ($i = 0; $i < count($betWinDetail); $i++) {
                if ($i == 0) {
                    $betLocation .= '<b>' . $this->changeBetLocationName($betWinDetail[$i]->bet_location)
                        . '</b> (' . number_format($betWinDetail[$i]->trs_amount, 0) . ')';
                } else {
                    $betLocation .= ', <b>' . $this->changeBetLocationName($betWinDetail[$i]->bet_location)
                        . '</b> (' . number_format($betWinDetail[$i]->trs_amount, 0) . ')';
                }

                $betWinAmount += (int)$betWinDetail[$i]->trs_amount;
            }

            //dd($betLocation);

            $params = array(
                'periodId' => $betWinDetail[0]->period_id
            );

            //dd($params);

            $resultDetail = $this->historyModel->selectGameResultDetail($params);

            //dd($resultDetail);

            $result = array(
                'gameResult' => ($resultDetail != null) ? $resultDetail->g_result : 'no result',
                'betLocation' => $betLocation,
                'betWinAmount' => number_format($betWinAmount, 0),
                'gameTrsId' => ($betWinDetail[0]->trs_id != null) ? $betWinDetail[0]->trs_id : 'no result',
                'gameDate' => ($betWinDetail[0]->trs_date != null) ? $betWinDetail[0]->trs_date : 'no result',
                'resultDate' => ($betWinDetail[0]->trs_check_date != null) ? $betWinDetail[0]->trs_check_date : 'no result',
                'gamePrdId' => ($betWinDetail[0]->period_id != null) ? $betWinDetail[0]->period_id : 'no result',
            );

            //dd($result);
        }


        return $result;
    }


    public function changeBetLocationName($params)
    {
        switch ($params) {
            case 'REDX':
                $params = 'RED';
                break;
            case 'NUM3':
                $params = '3';
                break;
            case 'NUM4':
                $params = '4';
                break;
            case 'TIE8':
                $params = 'TIE';
                break;
            case 'ODDX':
                $params = 'ODD';
                break;
        }
        return $params;
    }




}