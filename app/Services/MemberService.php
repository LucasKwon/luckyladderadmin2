<?php

namespace App\Services;

use App\Models\Members;
use App\Models\Operators;
use App\Models\PTransactions;
use App\Models\Roles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MemberService
{
    protected $operatorModel;
    protected $membersModel;
    protected $pointTrsModel;
    protected $rolesModel;

    /**
     * MemberService constructor.
     * @param Operators $operators
     * @param Members $members
     * @param PTransactions $pointTransactions
     * @param Roles $roles
     */
    public function __construct(Operators $operators, Members $members, PTransactions $pointTransactions, Roles $roles)
    {
        $this->operatorModel = $operators;
        $this->membersModel = $members;
        $this->pointTrsModel = $pointTransactions;
        $this->rolesModel = $roles;
    }

    /**
     * @param $operatorIdx
     * @return mixed|null
     *
     * ADMIN 레벨하위 Operator 리스트
     */
    public function getOperatorListByOperatorIdx($operatorIdx)
    {
        $params = array(
            'op_idx' => $operatorIdx
        );

        return $this->operatorModel->selectOperatorListByAdminOperatorIdx($params);
    }

    /**
     * @param $operatorIdx
     * @return mixed|null
     *
     * ADMIN 레벨하위 Operator 리스트
     */
    public function getNewOperatorListByOperatorIdx($operatorIdx)
    {
        $params = array(
            'op_idx' => $operatorIdx
        );

        return $this->operatorModel->selectNewOperatorListByAdminOperatorIdx($params);
    }

    /**
     * @param $operatorIdx
     * @return mixed|null
     *
     * Operator 의 Player 리스트
     */
    public function getOperatorPlayerListByOperatorIdx($operatorIdx)
    {
        $params = array(
            'op_idx' => $operatorIdx
        );

        return $this->membersModel->selectPlayerListByAdminOperatorIdx($params);
    }

    /**
     * @param $operatorIdx
     * @return mixed|null
     *
     * Operator 의 Player 의 상세정보
     */
    public function getOperatorPlayerDetailByOperatorIdx($operatorIdx, $playerIdx)
    {
        $params = array(
            'op_idx' => $operatorIdx,
            'mb_idx' => $playerIdx
        );

        return $this->membersModel->selectPlayerDetailByAdminOperatorIdx($params);
    }

    /**
     * @param $operatorIdx
     * @return mixed|null
     *
     * Operator 의 상세정보
     */
    public function getOperatorDetailByOperatorIdx($operatorIdx)
    {
        $params = array(
            'op_idx' => $operatorIdx
        );

        return $this->operatorModel->selectOperatorDetailByOperatorIdx($params);
    }

    /**
     * @param $request
     * @return mixed|null
     *
     * Operator 의 상세정보 갱신
     */
    public function setOperatorDetails($request)
    {
        $updateFields = array();
        $where_Fields = array(
            'op_id' => $request['op_id'],
            'op_idx' => $request['op_idx'],
        );

        $updateFields = !empty($request['op_name']) ?
            array_add($updateFields, 'op_name', $request['op_name']) : $updateFields;
        $updateFields = !empty($request['op_status']) ?
            array_add($updateFields, 'op_status', $request['op_status']) : $updateFields;
        $updateFields = !empty($request['op_profit']) ?
            array_add($updateFields, 'op_profit', $request['op_profit']) : $updateFields;
        $updateFields = !empty($request['op_rolling']) ?
            array_add($updateFields, 'op_rolling', $request['op_rolling']) : $updateFields;
        $updateFields = !empty($request['op_cash_type']) ?
            array_add($updateFields, 'op_cash_type', $request['op_cash_type']) : $updateFields;
        $updateFields = !empty($request['op_tel']) ?
            array_add($updateFields, 'op_tel', $request['op_tel']) : $updateFields;
        $updateFields = !empty($request['op_mobile']) ?
            array_add($updateFields, 'op_mobile', $request['op_mobile']) : $updateFields;
        $updateFields = !empty($request['op_email']) ?
            array_add($updateFields, 'op_email', $request['op_email']) : $updateFields;
        $updateFields = !empty($request['op_prefix']) ?
            array_add($updateFields, 'op_prefix', $request['op_prefix']) : $updateFields;

        $updateFields = array_add($updateFields, 'updated_at', Carbon::now());

        return $this->operatorModel->updateOperatorDetailInfo($updateFields, $where_Fields);
    }

    /**
     * @param $request
     * @return null|void
     *
     * 게임사용자 정보 갱신 - 패스워드를 입력할 경우 함께 갱신
     */
    public function setGamePlayerDetails($request)
    {
        $updateFields = array(
            'mb_name' => $request['mb_name'],
            'mb_status' => $request['op_status'],
            'mb_updated_date' => Carbon::now()
        );

        $updateFields = !empty($request['password']) ?
            array_add($updateFields, 'mb_password', sha1($request['password'])) : $updateFields;

        $where_Fields = array(
            'mb_id' => $request['mb_id'],
            'op_idx' => $request['mb_idx']
        );

        $this->membersModel->updateGamePlayerInfo($updateFields, $where_Fields);
    }

    /**
     * @param $request
     * @return mixed|null
     *
     * Vendor 생성
     */
    public function createNewDetailMemberProcess($request)
    {
        if ($request['op_level'] < 3) {

            $params = array(
                'op_level' => $request['op_level'] + 1,
                'op_parent_idx' => $request['op_parent_idx'],
                'op_id' => $request['op_id'],
                'op_name' => $request['op_name'],
                'op_email' => $request['op_email'],

                'op_password' => sha1($request['password']),
                'op_api_key' => strtoupper(md5($request['op_name'] . $request['op_id'] . '&dragons', false)),
                'signup_date' => Carbon::now(),
                'op_status' => 'ACTIVATE',
                'op_rolling' => array_key_exists('op_rolling', $request)
                    ? $request['op_rolling'] : 0,
                'op_mobile' => array_key_exists('op_mobile', $request)
                    ? $request['op_mobile'] : '',
                'op_tel' => array_key_exists('op_tel', $request)
                    ? $request['op_tel'] : '',
                'op_currency' => array_key_exists('op_currency', $request)
                    ? $request['op_currency'] : Auth::user()->op_currency,
                'op_cash_type' => array_key_exists('op_cash_type', $request)
                    ? $request['op_cash_type'] : 'PRE',
                'op_profit' => array_key_exists('op_profit', $request)
                    ? $request['op_profit'] : 0,
                'op_prefix' => array_key_exists('op_prefix', $request)
                    ? $request['op_prefix'] : $request['op_id'],
                'op_point' => array_key_exists('op_point', $request)
                    ? str_replace(",", "", $request['op_point']) : 0,
            );


            $opIdx = $this->operatorModel->insertNewMemberInfo($params);

            $updateFields = array(
                'id' => $opIdx
            );
            $where_Fields = array(
                'op_id' => $request['op_id'],
                'op_idx' => $opIdx
            );

            $this->operatorModel->updateOperatorDetailInfo($updateFields, $where_Fields);

            //오퍼레이터 생성시 포인트 지급후 관리자에서 차감
            if (array_key_exists('op_point', $request)) {

                $request['op_point'] = str_replace(",", "", $request['op_point']);

                $params = array(
                    'op_point' => (double)Auth::user()->op_point - (double)$request['op_point'],
                    'op_id' => Auth::user()->op_id,
                    'op_idx' => Auth::user()->op_idx,
                );

                //dd($params);

                $this->calculationOperatorPoint($params);

                //포인트 트렌잭션 등록
                $roleParams = array(
                    'admin_level' => $request['op_level'] + 1,
                );

                $roleShortName = $this->rolesModel->selectRoleShortName($roleParams);

                $trsParams = array(
                    'ptrs_op_id' => Auth::user()->id,
                    'ptrs_from_id' => Auth::user()->op_id,
                    'ptrs_to_id' => $request['op_id'],
                    'ptrs_from_idx' => Auth::user()->op_idx,
                    'ptrs_to_idx' => $opIdx,
                    'ptrs_amount' => (double)$request['op_point'],
                    'ptrs_frt_amount' => (double)Auth::user()->op_point - (double)$request['op_point'],
                    'ptrs_trt_amount' => (double)$request['op_point'],
                    'ptrs_currency' => $request['op_currency'],
                    'ptrs_type' => 'PM', //payment:PM , withdraw:WD
                    'ptrs_path' => Auth::user()->role_short_name . '2' . $roleShortName->role_short_name,
                    'ptrs_status' => 'DONE',
                    'ptrs_req_date' => Carbon::now(),
                    'ptrs_done_date' => Carbon::now(),
                    'ptrs_issued_id' => Auth::user()->op_id,
                    'ptrs_memo' => '생성시 포인트 지급됨'
                );


                $result = $this->pointTrsModel->insertPointTransactions($trsParams);
                //DB::commit();
                return $result;
            } else {

                //DB::commit();
                return 0;
            }

        } else {
            $params = array(

                'op_idx' => $request['op_parent_idx'],
                'mb_id' => $request['op_id'],
                'mb_currency' => $request['op_currency'],
                'mb_password' => sha1($request['password']),
                'mb_point' => (array_key_exists('op_point', $request)) ? $request['op_point'] : 0,
                'mb_name' => (array_key_exists('op_name', $request)) ? $request['op_name'] : '관리자생성',
                'mb_email' => $request['op_email'],
                'mb_status' => 'ACTIVATE',
                'mb_created_date' => Carbon::now(),
                'mb_desc' => '관리자가 등록한 사용자',
            );
            return $this->membersModel->insertNewGamePlayerInfo($params);
        }
    }


    /**
     * @param $request
     * @return mixed|null
     *
     * SYSTEM 계정으로 각 ADMIN 의 OPERATOR 생성
     */
    public function createTemporaryOperatorProcess($request)
    {
        $params = array(
            'op_level' => $request['op_level'],
            'op_parent_idx' => $request['op_idx'],
            'op_id' => $request['user_id'],
            'op_password' => sha1($request['password']),
            'op_name' => $request['user_id'],
            'op_tel' => '000-0000-0000', //임의
            'op_currency' => 'KRW',
            'op_point' => 0,
            'op_email' => substr($request['user_id'], -4) . $request['op_idx'] . '@email.com',
            'op_cash_type' => 'PRE',
            'op_profit' => 0,
            'op_rolling' => 0,
            'op_api_key' => strtoupper(md5($request['password'] . $request['user_id'] . '&dragons', false)),
            'op_status' => 'ACTIVATE',
            'op_prefix' => $request['user_id'],
            'signup_date' => Carbon::now(),
        );

        $opIdx = $this->operatorModel->insertNewMemberInfo($params);

        $updateFields = array(
            'id' => $opIdx
        );
        $where_Fields = array(
            'op_id' => $request['user_id'],
            'op_idx' => $opIdx
        );

        return $this->operatorModel->updateOperatorDetailInfo($updateFields, $where_Fields);

    }


    /**
     * @param $params
     * Vendor 포인트(발란스) 증감
     */
    public function calculationOperatorPoint($params)
    {
        $updateFields = array(
            'op_point' => $params['op_point']
        );
        $where_Fields = array(
            'op_id' => $params['op_id'],
            'op_idx' => $params['op_idx']
        );
        $this->operatorModel->updateOperatorPoint($updateFields, $where_Fields);
    }

    /**
     * @param $params
     * Player 포인트(발란스) 증감
     */
    public function calculationPlayerPoint($params)
    {
        $updateFields = array(
            'mb_point' => $params['mb_point']
        );
        $where_Fields = array(
            'mb_id' => $params['mb_id'],
            'op_idx' => $params['op_idx']
        );
        $this->membersModel->updateGamePlayerInfo($updateFields, $where_Fields);
    }

    /**
     * @param $request
     * @return bool|mixed
     *
     * 게임사용자(Player) 추가
     */
    public function createTemporaryGamePlayerProcess($request)
    {
        $params = array(

            'op_idx' => $request['op_idx'],
            'mb_id' => $request['user_id'],
            'mb_currency' => $request['mb_currency'],
            'mb_password' => sha1($request['password']),
            'mb_point' => 0,
            'mb_name' => '관리자생성',
            'mb_status' => 'ACTIVATE',
            'mb_created_date' => Carbon::now(),
            'mb_desc' => '관리자가 임의생성한 사용자',

        );
        return $this->membersModel->insertNewGamePlayerInfo($params);
    }

    /**
     * @param $request
     * @param $loginMember
     * @param $targetMember
     * @return null
     *
     * Vendor 포인트 지급
     */
    public function addPointFromVendor2Vendor($request, $loginMember, $targetMember)
    {
        $adminAmount = $loginMember->op_point - $request['op_point'];
        $adminParams = array(
            'op_id' => $loginMember->op_id,
            'op_idx' => $loginMember->op_idx,
            'op_point' => $adminAmount
        );

        $this->calculationOperatorPoint($adminParams);

        $targetAmount = $targetMember->op_point + $request['op_point'];
        $targetParams = array(
            'op_id' => $targetMember->op_id,
            'op_idx' => $targetMember->op_idx,
            'op_point' => $targetAmount
        );

        $this->calculationOperatorPoint($targetParams);

        $trsParams = array(
            'ptrs_op_id' => $loginMember->id,
            'ptrs_from_id' => $loginMember->op_id,
            'ptrs_to_id' => $targetMember->op_id,
            'ptrs_from_idx' => $loginMember->op_idx,
            'ptrs_to_idx' => $targetMember->op_idx,
            'ptrs_amount' => $request['op_point'],
            'ptrs_frt_amount' => $adminAmount,
            'ptrs_trt_amount' => $targetAmount,
            'ptrs_currency' => $targetMember->op_currency,
            'ptrs_type' => 'PM', //payment:PM , withdraw:WD
            'ptrs_path' => $loginMember->role_short_name . '2' . $targetMember->role_short_name,
            'ptrs_status' => 'DONE',
            'ptrs_req_date' => Carbon::now(),
            'ptrs_done_date' => Carbon::now(),
            'ptrs_issued_id' => $loginMember->op_id,
            'ptrs_memo' => $request['op_memo']
        );

        return $this->pointTrsModel->insertPointTransactions($trsParams);
    }

    /**
     * @param $request
     * @param $loginMember
     * @param $targetMember
     * @return null
     *
     * Vendor 포인트 차감
     */
    public function subPointFromVendor2Vendor($request, $loginMember, $targetMember)
    {
        $adminAmount = $loginMember->op_point + $request['op_point'];
        $adminParams = array(
            'op_id' => $loginMember->op_id,
            'op_idx' => $loginMember->op_idx,
            'op_point' => $adminAmount
        );

        $this->calculationOperatorPoint($adminParams);

        $targetAmount = $targetMember->op_point - $request['op_point'];
        $targetParams = array(
            'op_id' => $targetMember->op_id,
            'op_idx' => $targetMember->op_idx,
            'op_point' => $targetAmount
        );

        $this->calculationOperatorPoint($targetParams);

        $trsParams = array(
            'ptrs_op_id' => $loginMember->id,
            'ptrs_from_id' => $targetMember->op_id,
            'ptrs_to_id' => $loginMember->op_id,
            'ptrs_from_idx' => $targetMember->op_idx,
            'ptrs_to_idx' => $loginMember->op_idx,
            'ptrs_amount' => $request['op_point'],
            'ptrs_frt_amount' => $adminAmount,
            'ptrs_trt_amount' => $targetAmount,
            'ptrs_currency' => $targetMember->op_currency,
            'ptrs_type' => 'WD', //payment:PM , withdraw:WD
            'ptrs_path' => $targetMember->role_short_name . '2' . $loginMember->role_short_name,
            'ptrs_status' => 'DONE',
            'ptrs_req_date' => Carbon::now(),
            'ptrs_done_date' => Carbon::now(),
            'ptrs_issued_id' => $targetMember->op_id,
            'ptrs_memo' => $request['op_memo']
        );

        return $this->pointTrsModel->insertPointTransactions($trsParams);
    }

    /**
     * @param $request
     * @param $loginMember
     * @param $targetPlayer
     * @return null
     *
     * Player 포인트 지급
     */
    public function addPointFromVendor2Player($request, $loginMember, $targetPlayer)
    {
        $adminAmount = $loginMember->op_point - $request['op_point'];
        $adminParams = array(
            'op_id' => $loginMember->op_id,
            'op_idx' => $loginMember->op_idx,
            'op_point' => $adminAmount
        );
        $this->calculationOperatorPoint($adminParams);

        $targetAmount = $targetPlayer->mb_point + $request['op_point'];
        $targetParams = array(
            'op_idx' => $loginMember->op_idx,
            'mb_id' => $targetPlayer->mb_id,
            'mb_point' => $targetAmount
        );
        $this->calculationPlayerPoint($targetParams);

        $trsParams = array(
            'ptrs_op_id' => $loginMember->id,
            'ptrs_from_id' => $loginMember->op_id,
            'ptrs_to_id' => $targetPlayer->mb_id,
            'ptrs_from_idx' => $loginMember->op_idx,
            'ptrs_to_idx' => $targetPlayer->mb_idx,
            'ptrs_amount' => $request['op_point'],
            'ptrs_frt_amount' => $adminAmount,
            'ptrs_trt_amount' => $targetAmount,
            'ptrs_currency' => $targetPlayer->mb_currency,
            'ptrs_type' => 'PM', //payment:PM , withdraw:WD
            'ptrs_path' => $loginMember->role_short_name . '2PLY',
            'ptrs_status' => 'DONE',
            'ptrs_req_date' => Carbon::now(),
            'ptrs_done_date' => Carbon::now(),
            'ptrs_issued_id' => $loginMember->op_id,
            'ptrs_memo' => $request['op_memo']
        );

        return $this->pointTrsModel->insertPointTransactions($trsParams);
    }


    /**
     * @param $request
     * @param $loginMember
     * @param $targetPlayer
     * @return null
     *
     * Player 포인트 차감
     */
    public function subPointFromPlayer2Vendor($request, $loginMember, $targetPlayer)
    {
        $adminAmount = $loginMember->op_point + $request['op_point'];
        $adminParams = array(
            'op_id' => $loginMember->op_id,
            'op_idx' => $loginMember->op_idx,
            'op_point' => $adminAmount
        );
        $this->calculationOperatorPoint($adminParams);

        $targetAmount = $targetPlayer->mb_point - $request['op_point'];
        $targetParams = array(
            'op_idx' => $loginMember->op_idx,
            'mb_id' => $targetPlayer->mb_id,
            'mb_point' => $targetAmount
        );
        $this->calculationPlayerPoint($targetParams);

        $trsParams = array(
            'ptrs_op_id' => $loginMember->id,
            'ptrs_from_id' => $targetPlayer->mb_id,
            'ptrs_to_id' => $loginMember->op_id,
            'ptrs_from_idx' => $targetPlayer->mb_idx,
            'ptrs_to_idx' => $loginMember->op_idx,
            'ptrs_amount' => $request['op_point'],
            'ptrs_frt_amount' => $adminAmount,
            'ptrs_trt_amount' => $targetAmount,
            'ptrs_currency' => $targetPlayer->mb_currency,
            'ptrs_type' => 'WD', //payment:PM , withdraw:WD
            'ptrs_path' => 'PLY2' . $loginMember->role_short_name,
            'ptrs_status' => 'DONE',
            'ptrs_req_date' => Carbon::now(),
            'ptrs_done_date' => Carbon::now(),
            'ptrs_issued_id' => $targetPlayer->mb_id,
            'ptrs_memo' => $request['op_memo']
        );

        return $this->pointTrsModel->insertPointTransactions($trsParams);
    }


    /**
     * @param $request
     * @return mixed|null
     *
     * Vendor agent 생성
     */
    public function createAgentMemberProcess($request)
    {


            $params = array(
                'op_level' =>  $this->getVendorLevel($request['op_levelName']),
                'op_parent_idx' => $request['op_parent_idx'],
                'op_id' => $request['op_id'],
                'op_name' => $request['op_name'],
                'op_email' => $request['op_email'],

                'op_password' => sha1($request['password']),
                'op_api_key' => strtoupper(md5($request['op_name'] . $request['op_id'] . '&dragons', false)),
                'signup_date' => Carbon::now(),
                'op_status' => 'ACTIVATE',
                'op_rolling' => array_key_exists('op_rolling', $request)
                    ? $request['op_rolling'] : 0,
                'op_mobile' => array_key_exists('op_mobile', $request)
                    ? $request['op_mobile'] : '',
                'op_tel' => array_key_exists('op_tel', $request)
                    ? $request['op_tel'] : '',
                'op_currency' => array_key_exists('op_currency', $request)
                    ? $request['op_currency'] : Auth::user()->op_currency,
                'op_cash_type' => array_key_exists('op_cash_type', $request)
                    ? $request['op_cash_type'] : 'PRE',
                'op_profit' => array_key_exists('op_profit', $request)
                    ? $request['op_profit'] : 0,
                'op_prefix' => array_key_exists('op_prefix', $request)
                    ? $request['op_prefix'] : $request['op_id'],
                'op_point' => array_key_exists('op_point', $request)
                    ? str_replace(",", "", $request['op_point']) : 0,
            );


//            dd($params);

            $opIdx = $this->operatorModel->insertNewMemberInfo($params);

            $updateFields = array(
                'id' => $opIdx
            );
            $where_Fields = array(
                'op_id' => $request['op_id'],
                'op_idx' => $opIdx
            );

            $this->operatorModel->updateOperatorDetailInfo($updateFields, $where_Fields);

            return $opIdx;

//            //오퍼레이터 생성시 포인트 지급후 관리자에서 차감
//            if (array_key_exists('op_point', $request)) {
//
//                $request['op_point'] = str_replace(",", "", $request['op_point']);
//
//                $params = array(
//                    'op_point' => (double)Auth::user()->op_point - (double)$request['op_point'],
//                    'op_id' => Auth::user()->op_id,
//                    'op_idx' => Auth::user()->op_idx,
//                );
//
//                //dd($params);
//
//                $this->calculationOperatorPoint($params);
//
//                //포인트 트렌잭션 등록
//                $roleParams = array(
//                    'admin_level' => $request['op_level'] + 1,
//                );
//
//                $roleShortName = $this->rolesModel->selectRoleShortName($roleParams);
//
//                $trsParams = array(
//                    'ptrs_op_id' => Auth::user()->id,
//                    'ptrs_from_id' => Auth::user()->op_id,
//                    'ptrs_to_id' => $request['op_id'],
//                    'ptrs_from_idx' => Auth::user()->op_idx,
//                    'ptrs_to_idx' => $opIdx,
//                    'ptrs_amount' => (double)$request['op_point'],
//                    'ptrs_frt_amount' => (double)Auth::user()->op_point - (double)$request['op_point'],
//                    'ptrs_trt_amount' => (double)$request['op_point'],
//                    'ptrs_currency' => $request['op_currency'],
//                    'ptrs_type' => 'PM', //payment:PM , withdraw:WD
//                    'ptrs_path' => Auth::user()->role_short_name . '2' . $roleShortName->role_short_name,
//                    'ptrs_status' => 'DONE',
//                    'ptrs_req_date' => Carbon::now(),
//                    'ptrs_done_date' => Carbon::now(),
//                    'ptrs_issued_id' => Auth::user()->op_id,
//                    'ptrs_memo' => '생성시 포인트 지급됨'
//                );
//
//
//                $result = $this->pointTrsModel->insertPointTransactions($trsParams);
//                //DB::commit();
//                return $result;
//            } else {
//
//                //DB::commit();
//                return 0;
//            }


    }

    /**
     * @param $vendorLevelName
     * @return int
     *
     * 벤더의 레벨명으로 레벨 리턴
     */
    public function getVendorLevel($vendorLevelName)
    {
        switch($vendorLevelName) {
            case 'system' :
                return 1;
                break;

            case 'master' :
                return 2;
                break;

            case 'operator' :
                return 3;
                break;

            case 'site' :
                return 4;
                break;

            case 'masteragent' :
                return 5;
                break;

            case 'agent' :
                return 6;
                break;

        }
    }


    /**
     * @param $operatorIdx
     * @return mixed|null
     *
     * ADMIN 레벨하위 Operator 리스트
     */
    public function getAgentListByOperatorIdx($params)
    {

        return $this->operatorModel->selectAgentListByAdminOperatorIdx($params);
    }


}