<?php
namespace App\Services;

use App\Models\Accounts;
use App\Models\Operators;
use App\Services\CommonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ReportService
{
    protected $accountModel;
    protected $operatorModel;
    protected $commonService;

    /**
     * @param $accounts
     * @param $operators
     * AccountService constructor.
     */
    public function __construct(Accounts $accounts, Operators $operators, CommonService $commonService)
    {
        $this->accountModel = $accounts;
        $this->operatorModel = $operators;
        $this->commonService =  $commonService;
    }


    /**
     * @param $request
     * @return array|null
     *
     * 뱃윈 리포트
     */
    public function getViewBetWinReport($request)
    {
        $result = array(
            'accountingDetail' => [],
        );

        /* 날짜 검색 조건이 있을때 만 조회 시작 */
        if (!empty($request['startdate']) && !empty($request['enddate'])) {

            $params = array(
                'op_idx' => null,
                'startdate' => null,
                'enddate' => null
            );

//            if (Auth::user()->admin_level === 2) {

                $downParams = array(
                    'op_idx' => $request['op_idx'],
                );
                $downLineList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($request);
                $opList = $this->commonService->makeStringWhereInArr($downLineList, 'op_idx', 'Y');


                $accountDetailOpIdx = array();
                $accountResultDetail = array();

                foreach($opList as $items){

                    $downParams = null;
                    $downParams = array(
                        'op_idx' => $items,
                    );
//                    $downDetailLineList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($downParams);
                    $downDetailLineList = $this->operatorModel->downLineListInOpIdx($downParams);


                    if($downDetailLineList != null)
                    {

                        $reaultDetailOpIdxArr= $this->commonService->makeStringWhereInArr($downDetailLineList, 'op_idx', 'Y');
                        $accountDetailOpIdx = [];
                        foreach ($reaultDetailOpIdxArr as $detailItem){
                            array_push($accountDetailOpIdx,  $detailItem);
                        }

                    }
                    else{
                        array_push($accountDetailOpIdx,  $items);
                    }

                    $params['op_idx'] = $accountDetailOpIdx;
                    $params['startdate'] = !empty($request['startdate']) ? $request['startdate'] . ' 00:00:00' : null;
                    $params['enddate'] = !empty($request['enddate']) ? $request['enddate'] . ' 23:59:59' : null;


                    $resultaccountingDetail = $this->accountModel->selectAccountRequest($params);

                    if($resultaccountingDetail != null){
//                        dd($resultaccountingDetail);

                        $resutBetsSum = 0;
                        $resutWinsSum = 0;
                        $resutBetWinSum = 0;
                        $resutAmountSum = 0;
                        $resultLastDetailArr = array();
                        $resultLastDetailArr = null;
                        $getResult=null;
                        foreach ( $resultaccountingDetail as $resultItem) {

//                            dd($resultItem);
                            if($getResult == null){
                                $getResult= $resultItem;
                            }

                            $resutBetsSum += $resultItem->bets;
                            $resutWinsSum += $resultItem->wins;
                            $resutBetWinSum += $resultItem->betwin;
                            $resutAmountSum += $resultItem->amount;


                            $resultItem->bets = $resutBetsSum;
                            $resultItem->wins = $resutWinsSum;
                            $resultItem->betwin = $resutBetWinSum;
                            $resultItem->amount = $resutAmountSum;

                            $params = null;
                            $params = array(
                                'op_idx' => $resultItem->op_parent_idx
                            );

                         $resultOpParent = $this->operatorModel->selectOperatorDetailByOperatorIdx($params);
//                            dd($resultOpParent->op_name);

                         if($resultOpParent != null){
                             $getResult->op_parent_name = $resultOpParent->op_name;
                         }
                         else{
                             $getResult->op_parent_name = null;
                         }


                            if($resultLastDetailArr == null) {
                                $getResult->op_id = $resultItem->op_id;
                                $getResult->op_idx = $resultItem->op_idx;
                                $getResult->op_name = $resultItem->op_name;
                                $getResult->op_profit = $resultItem->op_profit;
                                $getResult->payout = $resultItem->payout;
                            }
                            else{

                                $getResult->bets = $resutBetsSum;
                                $getResult->wins = $resutWinsSum;
                                $getResult->betwin = $resutBetWinSum;
                                $getResult->amount = $resutAmountSum;
                            }

//                            dd($getResult);
                            $resultLastDetailArr= $getResult;

                        }
//                        dd($resultLastDetailArr);
                        array_push($accountResultDetail, $resultLastDetailArr );

                    }

                }
                $result['accountingDetail'] = $accountResultDetail;
//dd($result);

//            } else {//if (Auth::user()->admin_level === 3 ) {
//
//                $resultOpIdx = array();
//                $downParams = array(
//                    'op_idx' => $request['op_idx'],
//                );
//                $downLineList = $this->operatorModel->downLineListNotInOpIdx($downParams);
//                if ($downLineList != null) {
//                    $resultOpIdx = $this->commonService->makeStringWhereInArr($downLineList, 'op_idx', 'Y');
//                } else {
////                    $resultOpIdx= $request['op_idx'];
//                    array_push($resultOpIdx, $request['op_idx']);
//                }
//
////                $params['op_idx']       = $request['op_idx'];
//                $params['op_idx'] = $resultOpIdx;
//                $params['startdate'] = $request['startdate'] . ' 00:00:00';
//                $params['enddate'] = $request['enddate'] . ' 23:59:59';
//
//
//                if (Auth::user()->admin_level != 4) {
//                    /* Bet/win 조회 */
//                    $result['accountingDetail'] = $this->accountModel->selectAccountRequest($params);

//            dd( $result['accountingDetail'], $params);

//                }
//            }



        }
        /* 날짜 검색 조건이 있을때 만 조회 종료 */
//        dd($result);
        return $result;
    }

}