<?php
namespace App\Services;

use App\Models\Games;
use App\Models\Members;
use App\Models\Operators;

class CommonService
{
    protected $operatorsModel;
    protected $membersModel;
    protected $gamesModel;


    /**
     * CommonService constructor.
     * @param Operators $operators
     * @param Members $members
     * @param Games $games
     *
     */
    public function __construct(Operators $operators, Members $members, Games $games)
    {
        $this->operatorsModel = $operators;
        $this->membersModel = $members;
        $this->gamesModel = $games;
    }

    /**
     * @param $userName
     * @return null
     *
     * 회원명 중복여부
     */
    public function checkOperatorNameDuplicate($userName)
    {
        $params = array(
            'op_name' => $userName
        );

        return $this->operatorsModel->selectOperatorNameByUserName($params);
    }

    /**
     * @param $userName
     * @return mixed|null
     *
     * 게임사용자 이름중복 여부
     */
    public function checkPlayerNameDuplicate($userName)
    {
        $params = array(
            'mb_name' => $userName
        );
        return $this->membersModel->selectGamePlayerByName($params);
    }

    /**
     * @param $userEmail
     * @return mixed|null
     *
     * 이메일 중복여부
     */
    public function checkOperatorEmailDuplicate($userEmail)
    {
        $params = array(
            'op_email' => $userEmail
        );

        return $this->operatorsModel->selectOperatorEmailByUserEmail($params);
    }

    /**
     * @param $userEmail
     * @return mixed|null
     *
     * 게임사용자 이메일 중복여부
     */
    public function checkPlayerEmailDuplicate($userEmail)
    {
        $params = array(
            'mb_email' => $userEmail
        );

        return $this->membersModel->selectPlayerEmailByUserEmail($params);

    }

    /**
     * @param $userId
     * @return mixed|null
     *
     * 게임사용자 아이디 중복여부
     */
    public function checkPlayerIdDuplicate($userId)
    {
        $params = array(
            'mb_id' => $userId
        );

        return $this->membersModel->selectPlayerIdByUserEmail($params);

    }

    /**
     * @param $userId
     * @return mixed|null
     *
     * 아이디 중복여부
     */
    public function checkOperatorIdDuplicate($userId)
    {
        $params = array(
            'op_id' => $userId
        );

        return $this->operatorsModel->selectOperatorIdByInputId($params);

    }

    /**
     * @param $userPrefix
     * @return mixed|null
     */
    public function checkOperatorPrefixDuplicate($userPrefix)
    {
        $params = array(
            'op_prefix' => $userPrefix
        );

        return $this->operatorsModel->selectOperatorPrefixByUserPrefix($params);

    }

    /**
     * @param $mbId
     * @param $opIdx
     * @return mixed|null
     */
    public function checkMemberIdDuplicate($mbId, $opIdx)
    {
        $params = array(
            'mb_id' => $mbId,
            'op_idx' => $opIdx,
        );
        return $this->membersModel->selectMemberIdByInputId($params);

    }

    /**
     * @param $gameId
     * @return mixed
     */
    public function checkGameIdDuplicate($gameId)
    {
        $params = array(
            'g_id' => $gameId
        );

        return $this->gamesModel->selectGameId($params);
    }

    /**
     * @param $gTitle
     * @return mixed
     */
    public function checkGameTitleDuplicate($gTitle)
    {
        $params = array(
            'g_title' => $gTitle
        );

        return $this->gamesModel->selectGameTitle($params);
    }

    /**
     * @param $tName
     * @return mixed
     *
     * 테이블명 중복 확인
     */
    public function checkTableNameDuplicate($tName)
    {
        $params = array(
            'tb_name' => $tName
        );

        return $this->gamesModel->selectTableName($params);

    }



    /**
     * @param $str
     * @return string
     *
     * 모델에서 where in 에 사용할 string 3,5,8 과 같은 형태의 string return
     */
    public function makeStringWhereInArr($str, $getid, $arrYN)
    {
        if (!empty($str)) {
            $newArray = array();

            foreach ($str as $strItem){
                if(!empty($strItem->$getid)){
                    array_push($newArray,  $strItem->$getid );
                }
            }

            /* 배열 중복 제거 */
            $newArray = array_unique($newArray);

            if($arrYN =='Y')
            {
                return $newArray;
            }
            else{
                return join(",", $newArray);
            }

        } else {
            return $str;
        }
    }

}