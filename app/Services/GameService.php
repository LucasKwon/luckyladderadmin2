<?php
namespace App\Services;


use App\Models\Games;
use App\Models\Operators;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GameService
{

    protected $gamesModel;
    protected $operatorModel;


    /**
     * GameService constructor.
     * @param Games $games
     * @param Operators $operators
     */
    public function __construct(Games $games, Operators $operators)
    {
        $this->gamesModel = $games;
        $this->operatorModel = $operators;
    }

    /**
     * @return mixed
     *
     * 베팅제한 리스트
     */
    public function getBettingLimits($opIdx)
    {
        $params = array(
            'op_idx' => $opIdx
        );
        return $this->gamesModel->selectBettingLimitsList($params);
    }

    /**
     * @param $limitIdx
     * @return mixed
     */
    public function getBettingLimitByIdx($limitIdx)
    {
        $whereParams = array(
            'op_idx'    => Auth::user()->op_idx,
            'limit_idx' => $limitIdx
        );
        return $this->gamesModel->selectBetLimitByIdx($whereParams);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function getBettingLimitOperatorListByOppIdx($request)
    {
        $whereParams = array(
            'max_bet' => $request['maxbet'],
            'min_bet' => $request['minbet'],
            'max_tie' => $request['maxtie'],
            'min_tie' => $request['mintie'],
            'op_parent_idx' => $request['oppIdx'],
        );

        return $this->gamesModel->selectBettingLimitsOperatorList($whereParams);

    }

    /**
     * @param $request
     * @return array|int
     *
     * 베팅리밋 등록
     */
    public function registerBettingLimits($request)
    {
        $results = array(
            'message' => null,
            'result' => null
        );

        $max_bet = str_replace(',', '', $request['max_bet']);
        $min_bet = str_replace(',', '', $request['min_bet']);
        $max_tie = str_replace(',', '', $request['max_tie']);
        $min_tie = str_replace(',', '', $request['min_tie']);

        if ((integer)$max_bet < 1000 || (integer)$min_bet < 1000 || (integer)$max_tie < 1000 || (integer)$min_tie < 1000) {

            $results['message'] = '베팅 최소금액은 1000 이상이어야 합니다.';

        } else {

            if ((integer)$max_bet <= (integer)$min_bet) {

                $results['message'] = '최대 베팅금액은 최소 베팅금액보다 많아야 합니다.';

            } elseif ((integer)$max_tie <= (integer)$min_tie) {

                $results['message'] = '최대 타이베팅금액은 최소 타이베팅금액보다 많아야 합니다.';

            } elseif ((integer)$max_bet <= (integer)$max_tie) {

                $results['message'] = '최대 베팅금액이 최대 타이베팅금액보다 많아야 합니다.';

            } else {

                if( strcmp($request['opIds'], "") == 0 ){
                    $params = array('op_idx'=>Auth::user()->op_idx);
                    $opList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($params);
                }else{
                    $opList = explode(',', $request['opIds']);
                }

                for ($i = 0; $i < count($opList); $i++){

                    $params = array(
                        'max_bet'       => $max_bet,
                        'min_bet'       => $min_bet,
                        'max_tie'       => $max_tie,
                        'min_tie'       => $min_tie,
                        //'limit_status'  => 'ACTIVATE',
                        'register_date' => Carbon::now(),
                        'update_date'   => Carbon::now(),
                        'op_parent_idx' => Auth::user()->op_idx,
                        'op_idx'        => (strcmp($request['opIds'], "") == 0) ? $opList[$i]->op_idx : $opList[$i],
                    );

                    $results['result'] += $this->gamesModel->insertBettingLimits($params);
                }
            }
        }
        return $results;
    }

    /**
     * @param $request
     * @return array
     *
     * 베팅리밋 상태 업데이트
     */
    public function updateBettingLimits($request)
    {
        //dd($request);
        $previousIds = explode(',', $request['opIdsArr']);
        $currentIds = explode(',', $request['opIds']);

        //dd($previousIds, $currentIds);

        $results = array(
            'message' => null,
            'result' => null
        );

        $max_bet = str_replace(',', '', $request['max_bet']);
        $min_bet = str_replace(',', '', $request['min_bet']);
        $max_tie = str_replace(',', '', $request['max_tie']);
        $min_tie = str_replace(',', '', $request['min_tie']);

        if ((integer)$max_bet < 1000 || (integer)$min_bet < 1000 || (integer)$max_tie < 1000 || (integer)$min_tie < 1000) {

            $results['message'] = '베팅 최소금액은 1000 이상이어야 합니다.';

        }else{
            if ((integer)$max_bet <= (integer)$min_bet) {

                $results['message'] = '최대 베팅금액은 최소 베팅금액보다 많아야 합니다.';

            } elseif ((integer)$max_tie <= (integer)$min_tie) {

                $results['message'] = '최대 타이베팅금액은 최소 타이베팅금액보다 많아야 합니다.';

            } elseif ((integer)$max_bet <= (integer)$max_tie) {

                $results['message'] = '최대 베팅금액이 최대 타이베팅금액보다 많아야 합니다.';

            } else {

                if( count($previousIds) > count($currentIds) ){
                    // 에이전트 삭제 됨
                    $diffOpIds = array_values(array_diff($previousIds, $currentIds));
                    //dd('del', $diffOpIds);
                    for ( $i = 0; $i < count($diffOpIds); $i++){
                        $deleteParams = array(
                            'max_bet'       => $request['pmaxBet'],
                            'min_bet'       => $request['pminBet'],
                            'max_tie'       => $request['pmaxTie'],
                            'min_tie'       => $request['pminTie'],
                            'op_parent_idx' => Auth::user()->op_idx,
                            'op_idx'        => (integer)$diffOpIds[$i]
                        );
                        //dd($deleteParams);
                        $results['result'] += $this->gamesModel->deleteBettingLimits($deleteParams);
                    }
                }elseif ( count($previousIds) < count($currentIds) ){
                    // 에이전트 추가 됨
                    $diffOpIds = array_values(array_diff( $currentIds ,$previousIds));
                    //dd('add', $diffOpIds);
                    for ( $i = 0; $i < count($diffOpIds); $i++){
                        $params = array(
                            'max_bet'       => $max_bet,
                            'min_bet'       => $min_bet,
                            'max_tie'       => $max_tie,
                            'min_tie'       => $min_tie,
                            'register_date' => Carbon::now(),
                            'update_date'   => Carbon::now(),
                            'op_parent_idx' => Auth::user()->op_idx,
                            'op_idx'        => (integer)$diffOpIds[$i]
                        );

                        //dd($params);

                        $results['result'] += $this->gamesModel->insertBettingLimits($params);
                    }
                }else{
                    // 에이전트 변경없음
                }

                //dd($currentIds);
                for ($i = 0; $i < count($currentIds); $i++){
                    $whereParams = array(
                        //'limit_idx' => $request['limitIdx'],
                        'max_bet'       => $request['pmaxBet'],
                        'min_bet'       => $request['pminBet'],
                        'max_tie'       => $request['pmaxTie'],
                        'min_tie'       => $request['pminTie'],
                        'op_parent_idx' => Auth::user()->op_idx,
                        'op_idx'        => (integer)$currentIds[$i]
                    );

                    $updateParams = array(
                        'max_bet'       => $max_bet,
                        'min_bet'       => $min_bet,
                        'max_tie'       => $max_tie,
                        'min_tie'       => $min_tie,
                        'update_date'   => Carbon::now(),
                    );

                    //dd($whereParams, $updateParams);

                    $results['result'] = $this->gamesModel->updateBettingLimits($whereParams, $updateParams);
                }


            }
        }
        //dd($results);
        return $results;
    }

    /**
     * @param $request
     * @return array
     *
     * 베팅리밋 삭제
     */
    public function deleteBettingLimits($request)
    {
        $results = array(
            'message' => null,
            'result' => null
        );

        $whereParams = array(
            //'limit_idx' => $request['reqLimitId']
            'max_bet'       => $request['maxbet'],
            'min_bet'       => $request['minbet'],
            'max_tie'       => $request['maxtie'],
            'min_tie'       => $request['mintie'],
            'op_parent_idx' => Auth::user()->op_idx,
            'op_idx'        => null,

        );

        $result = $this->gamesModel->deleteBettingLimits($whereParams);
        $results['result'] = $result;

        return $results;
    }

    /**
     * @return mixed|null
     *
     * 게임 리스트
     */
    public function getGameList()
    {
        return $this->gamesModel->selectGameList();

    }

    /**
     * @return mixed
     *
     * 딜러 리스트
     */
    public function getDealerList()
    {
        return $this->gamesModel->selectDealerList();
    }


    /**
     * @param $gIdx
     * @return mixed
     *
     * 게임 상세정보
     */
    public function getGameDetailInfo($gIdx)
    {
        $params = array(
            'g_idx' => $gIdx
        );

        return $this->gamesModel->selectGameDetailInfo($params);
    }


    /**
     * @param $params
     * @return array
     *
     * 게임 상세정보 업데이트
     */
    public function updateGameInfo($params)
    {
        //dd($params);
        $results = array(
            'message' => null,
            'result' => null
        );

        $whereParams = array(
            'g_idx' => $params['g_idx']
        );

        $updateParams = array(
            'g_id' => $params['g_id'],
            'g_title' => $params['g_title'],
            'g_status' => $params['g_status'],
            'g_delaytime' => $params['g_delaytime'],
            'g_org_status' => $params['g_org_status'],
            'g_mov_url' => $params['g_mov_url'],
        );

        $result = $this->gamesModel->updateGameInfo($whereParams, $updateParams);
        //dd($result);
        $results['result'] = $result;

        return $results;
    }

    /**
     * @return mixed
     *
     * 게임 테이블 리스트
     */
    public function getGameTableList()
    {
        $params = array(
            'op_parent_idx' => Auth::user()->op_idx
        );
        return $this->gamesModel->selectGameTableList($params);
    }


    /**
     * @param $request
     * @return array
     *
     * 테이블 정보 등록
     */
    public function registerGameTableInfo($request)
    {
        $results = array(
            'message' => null,
            'result' => null
        );

        if( strcmp($request['opIds'], "") == 0 ){
            $params = array('op_idx'=>Auth::user()->op_idx);
            $opList = $this->operatorModel->selectOperatorListByAdminOperatorIdx($params);
        }else{
            $opList = explode(',', $request['opIds']);
        }

        //dd($opList);

        for ($i = 0; $i < count($opList); $i++){
            $params = array(
                'op_idx'            => (integer)$opList[$i],
                'op_parent_idx'     => Auth::user()->op_idx,
                'tb_name'           => $request['tb_name'],
                'tb_currency'       => $request['tb_currency'],
                'tb_status'         => $request['tb_status'],
                'tb_created_date'   => Carbon::now(),
                'tb_updated_date'   => Carbon::now(),
                'g_idx'             => $request['g_idx'],
                'd_idx'             => $request['d_idx'],
            );

            //dd($params);

            $results['result'] += $this->gamesModel->insertGameTableInfo($params);
        }


        return $results;

    }

    /**
     * @param $tbIdx
     * @return mixed
     *
     * 테이블 상세정보
     */
    public function getTableDetailInfo($tbIdx)
    {
        $params = array(
            'tb_idx' => $tbIdx
        );

        return $this->gamesModel->selectTableDetailInfo($params);
    }

    /**
     * @param $request
     * @return array
     *
     * 테이블 정보 업데이트
     */
    public function updateGameTableInfo($request)
    {
        //dd($request);
        $previousIds = explode(',', $request['opIdsArr']);
        $currentIds = explode(',', $request['opIds']);

        //dd($previousIds, $currentIds);

        $results = array(
            'message' => null,
            'result' => null
        );

        if( count($previousIds) > count($currentIds) ) {
            // 에이전트 삭제 됨
            $diffOpIds = array_values(array_diff($previousIds, $currentIds));
            //dd('del',$diffOpIds);
            for ( $i = 0; $i < count($diffOpIds); $i++){

                $deleteParams = array(
                    'tb_name'       => $request['ptbName'],
                    'tb_currency'   => $request['ptbCurrency'],
                    'g_idx'         => $request['pgIdx'],
                    'd_idx'         => $request['pdIdx'],
                    'tb_status'     => $request['pStatus'],
                    'op_parent_idx' => Auth::user()->op_idx,
                    'op_idx'        => (integer)$diffOpIds[$i]
                );
                //dd($deleteParams);
                $results['result'] += $this->gamesModel->deleteGameTable($deleteParams);
            }

        }elseif(count($previousIds) < count($currentIds)){
            // 에이전트 추가 됨
            $diffOpIds = array_values(array_diff($currentIds, $previousIds));
            //dd('add',$diffOpIds);
            for ( $i = 0; $i < count($diffOpIds); $i++){
                $insertParams = array(
                    'tb_name'         => $request['tb_name'],
                    'tb_currency'     => $request['tb_currency'],
                    'g_idx'           => $request['g_idx'],
                    'd_idx'           => $request['d_idx'],
                    'tb_status'       => $request['tb_status'],
                    'tb_created_date' => Carbon::now(),
                    'tb_updated_date' => Carbon::now(),
                    'op_parent_idx'   => Auth::user()->op_idx,
                    'op_idx'          => (integer)$diffOpIds[$i]
                );

                $results['result'] += $this->gamesModel->insertGameTableInfo($insertParams);

            }
        }

        for ($i = 0; $i < count($currentIds); $i++ ){

            $whereParams = array(
                'tb_name'           => $request['ptbName'],
                'tb_currency'       => $request['ptbCurrency'],
                'g_idx'             => $request['pgIdx'],
                'd_idx'             => $request['pdIdx'],
                'tb_status'         => $request['pStatus'],
                'op_parent_idx'     => Auth::user()->op_idx,
                'op_idx'            => (integer)$currentIds[$i],
            );

            $updateParams = array(
                'tb_name'           => $request['tb_name'],
                'tb_currency'       => $request['tb_currency'],
                'g_idx'             => $request['g_idx'],
                'd_idx'             => $request['d_idx'],
                'tb_status'         => $request['tb_status'],
                'op_parent_idx'     => Auth::user()->op_idx,
                'op_idx'            => (integer)$currentIds[$i],
                'tb_updated_date'   => Carbon::now(),
            );

            $results['result'] += $this->gamesModel->updateGameTableInfo($whereParams, $updateParams);

        }
        return $results;
    }

    /**
     * @param $request
     * @return array
     *
     * 테이블 정보삭제
     */
    public function deleteGameTable($request)
    {
        //dd('delete', $request);
        $results = array(
            'message' => null,
            'result' => null
        );

        $whereParams = array(
            'tb_name'       => $request['tbName'],
            'tb_currency'   => $request['tbCurrency'],
            'g_idx'         => $request['tbGIdx'],
            'd_idx'         => $request['tbDIdx'],
            'tb_status'     => $request['tbStatus'],
            'op_parent_idx' => Auth::user()->op_idx,
        );

        //dd($whereParams);
        $result = $this->gamesModel->deleteGameTable($whereParams);
        $results['result'] = $result;

        return $results;
    }

    /**
     * @param $request
     * @return mixed
     *
     * 테이블 에이전트 리스트
     */
    public function getTableOperatorList($request)
    {
        //dd($request);

        $whereParams = array(
            'tb_name'       => $request['tbName'],
            'tb_currency'   => $request['tbCurrency'],
            'g_idx'         => $request['tbGIdx'],
            'd_idx'         => $request['tbDIdx'],
            'op_parent_idx' => Auth::user()->op_idx,
        );

        return $this->gamesModel->selectTableOperatorList($whereParams);
    }
}