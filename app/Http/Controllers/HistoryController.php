<?php

namespace App\Http\Controllers;

use App\Services\HistoryService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\View;

class HistoryController extends DragonController
{
    protected $historyService;

    /**
     * HistoryController constructor.
     * @param HistoryService $historyService
     */
    public function __construct(HistoryService $historyService)
    {
        parent::__construct();
        $this->setApp();

        $this->historyService = $historyService;
    }


    /**
     * @param $opLevel
     * @param $opIdx
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewGameHistory($opLevel, $opIdx)
    {
        if (!empty($opLevel))   \request()->request->set('op_level', $opLevel);
        if (!empty($opIdx))     \request()->request->set('op_idx', $opIdx);

        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.js',
            'assets/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/form-plugins.demo.js',

            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
            'FormPlugins.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $returnParams = $this->historyService->getBetWinHistory(\request()->request->all());
        return view('history.betwinHistory',
            [
                'betwinHistoryList' => $returnParams['betwinList'],
                'operatorList' => $returnParams['operatorList'],
                'searchParam' => \request()->query()
            ]
        );

    }

    /**
     * @param $trsId
     * @param $trsType
     * @param $checkResult
     * @param $periodId
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewGameHistoryDetail($trsId, $trsType, $checkResult, $periodId)
    {
        $params = array(
            'trsId' => $trsId,
            'trsType' => $trsType,
            'checkResult' => $checkResult,
            'periodId' => $periodId
        );

        $result = $this->historyService->getBetWinDetailInfo($params);

        //dd($result);
        if ($result == null) {
            return response()->json(['statusCode' => 204,
                'statusMessage' => 'No Data',
            ], 200);
        } else {
            return response()->json(['statusCode' => 200,
                'statusMessage' => 'Success',
                'result' => $result], 200);
        }
    }

    /*
    public function setGameHistoryConfirm()
    {
        //game/result/{gameResult}/machine/{machineId}/period/{period}/episode/{episode}/uid/{uniqueId}
        $params = explode('_', \request()->request->get('periodId'));
        //dd($params);
        //dd(request()->request->all());
    }
    */


}
