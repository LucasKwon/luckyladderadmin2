<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\View;

class DragonController extends Controller
{
    public $assetUrl;
    public $js = [];
    public $css = [];
    public $initJs = [];

    /**
     * DragonController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->getBaseInfo();
    }


    /**
     *
     */
    public function setApp()
    {
        $this->assetUrl = 'http://' . request()->getHttpHost() . '/';

        $this->js['basicscripts'] = [
            'assets/plugins/gritter/js/jquery.gritter.js',
            'assets/js/ui-modal-notification.demo.js'
        ];

        $this->css['basicpages'] = [
            'assets/plugins/gritter/css/jquery.gritter.css'
        ];

        $this->initJs = [
            'Notification.init();'
        ];

        View::share('assetUrl', $this->assetUrl);
        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);
    }

    /**
     * DashBoard 정보
     */
    public function getBaseInfo()
    {
        $resultRightBar = array(
            'depositsAmountToday' => (object)[
                'DEPOSITS' => '0.00',
                'MANUAL_DEPOSITS' => '0.00',
                'PAYOUT' => '0.00',
                'MANUAL_PAYOUT' => '0.00'
            ],
            'depositRequestCnt' => (object)[
                'dcnt' => 0,
                'pcnt' => 0
            ],
            'todayBetWin' => (object)[
                'BETS' => '0.00',
                'WINS' => '0.00'
            ],
            'resultThisMonth' => (object)[
                'BETS' => '0.00',
                'WINS' => '0.00'
            ],
            'vendorThisMonthBet' => [
                (object)[
                    'siteid' => 'oriental2',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ],
                (object)[
                    'siteid' => 's3939',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ],
                (object)[
                    'siteid' => 'm5656',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ],
                (object)[
                    'siteid' => 'a76',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ]
            ],
            'vendorLastMonthBet' => [
                (object)[
                    'siteid' => 'casa',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ],
                (object)[
                    'siteid' => 'rio365',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ],
                (object)[
                    'siteid' => 'master',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ],
                (object)[
                    'siteid' => 'oriental',
                    'BETS' => '0.00',
                    'WINS' => '0.00'
                ]
            ]

        );

        View::share('depositAmountToday', $resultRightBar['depositsAmountToday']);
        View::share('depositRequestCnt', $resultRightBar['depositRequestCnt']);
        View::share('todayBetWin', $resultRightBar['todayBetWin']);
        View::share('resultThisMonth', $resultRightBar['resultThisMonth']);
        View::share('vendorThisMonthBet', $resultRightBar['vendorThisMonthBet']);
        View::share('vendorLastMonthBet', $resultRightBar['vendorLastMonthBet']);
    }

}
