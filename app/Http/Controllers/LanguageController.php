<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class LanguageController extends Controller
{
    /**
     * @param $lang
     * @return mixed
     */
    public function switchLang($lang)
    {
        //dd($lang, Config::get('languages'));

        if(array_key_exists($lang, Config::get('languages')))
        {
            Session::set('applocale', $lang);
        }
        return Redirect::back();
    }
}
