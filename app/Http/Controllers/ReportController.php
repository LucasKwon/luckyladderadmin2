<?php

namespace App\Http\Controllers;

use App\Services\ReportService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class ReportController extends DragonController
{
    //
    protected $reportService;

    /**
     * @param $accounts
     * @param $operators
     * AccountService constructor.
     */
    public function __construct(ReportService $reportService)
    {
        parent::__construct();
        $this->setApp();

       $this->reportService = $reportService;
    }

    /**
     * @param $opIdx
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 뱃윈 리포트
     */
    public function BetWinReport($opIdx)
    {
        if(!empty($opIdx))
            request()->request->set('op_idx', $opIdx);

        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/table-manage-buttons.demo.js',
            'assets/plugins/parsley/dist/parsley.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
            'FormPlugins.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $result = $this->reportService->getViewBetWinReport(\request()->request->all());


        return view('report.betwinReport',
            [
                'accountingDetail'  => $result['accountingDetail'],
                'searchParam'       => request()->query()
            ]);

    }

}
