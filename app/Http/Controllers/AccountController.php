<?php

namespace App\Http\Controllers;

use App\Services\AccountService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class AccountController extends DragonController
{
    protected $accountService;

    /**
     * @param $accountService
     * AccountController constructor.
     */
    public function __construct(AccountService $accountService)
    {
        parent::__construct();
        $this->setApp();

        $this->accountService = $accountService;
    }

    /**
     * @param $opIdx
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 정산조회 및 요청
     */
    public function ViewAccountRequest($opIdx)
    {
        if(!empty($opIdx))
            request()->request->set('op_idx', $opIdx);

        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/table-manage-buttons.demo.js',
            'assets/plugins/parsley/dist/parsley.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
            'FormPlugins.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $result = $this->accountService->getViewAccountRequest(\request()->request->all());


        return view('account.request',
            [
                'accountingList'    => $result['accountingList'],
                'accountingDetail'  => $result['accountingDetail'],
                'accRollingList'    => $result['accRollingList'],
                'accRollingDetail'  => $result['accRollingDetail'],
                'searchParam'       => request()->query()
            ]);

    }

    /**
     * @return mixed
     *
     * 정산요청 등록 프로세스
     */
    public function addAccountRequest()
    {
        $result = $this->accountService->addAccountRequest(\request()->request->all());

        if($result['result'] === 0){
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        }else{
            return Redirect::to(URL::previous())->with('processResult', $result['message']);
        }
    }

    /**
     * @param $opIdx
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 정산요청 리스트
     */
    public function ViewAccountRequested($opIdx)
    {
        if(!empty($opIdx))
            request()->request->set('opIdx', $opIdx);

        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/table-manage-buttons.demo.js',
            'assets/plugins/parsley/dist/parsley.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
            'FormPlugins.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $result = $this->accountService->getViewAccountRequested(\request()->request->all());

        //dd($result);
        return view('account.requested', ['accountingList' => $result['accountingList']]);

    }

    /**
     * @return mixed
     */
    public function updateAccountRequest()
    {
        $result = $this->accountService->updateAccountRequestStatus(\request()->request->all());

        if($result['result'] === 0){
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        }else{
            return Redirect::to(URL::previous())->with('processResult', $result['message']);
        }
    }


    /**
     *
     */
    public function updateRollingAccountRequest()
    {
        $result = $this->accountService->addRollAccountRequest(\request()->request->all());

        if($result['result'] === 0){
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        }else{
            return Redirect::to(URL::previous())->with('processResult', $result['message']);
        }
    }
}
