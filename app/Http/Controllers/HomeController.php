<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HomeController extends DragonController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setApp();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->js['scripts'] = [
            'assets/plugins/morris/raphael.min.js',
            'assets/plugins/morris/morris.min.js',
            'assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js',
            'assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js',
            'assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js',
            'assets/plugins/gritter/js/jquery.gritter.min.js',
            'assets/js/dashboard-v2.js'
        ];

        $this->css['pages'] = [
            'assets/plugins/morris/morris.css',
        ];

        $returnResult = array(
            'resultThisMonth' => (object)[
                'BETS' => '0.00',
                'WINS' => '0.00',
            ],
            'resultLastMonth' => (object)[
                'BETS' => '0.00',
                'WINS' => '0.00',
            ],
            'resultTotalMembers' => (object)[
                'cnt' => 0
            ],
            'resultMonthMembers' => (object)[
                'cnt' => 0
            ],
            'resultLastMonthMembers' => (object)[

                'cnt' => 0
            ],
            'resultTodayMembers' => (object)[
                'cnt' => 0
            ],
            'resultThirdparty' => [],
            'resultChartBetWin' => '[
            {"dtv_date":"2017-04","BETS":"0.00","WINS":"0.00"}
            ,{"dtv_date":"2017-03","BETS":"0.00","WINS":"0.00"}
            ,{"dtv_date":"2017-02","BETS":"0.00","WINS":"0.00"}
            ,{"dtv_date":"2017-01","BETS":"0.00","WINS":"0.00"}
            ,{"dtv_date":"2016-12","BETS":"0.00","WINS":"0.00"}
            ,{"dtv_date":"2016-11","BETS":"0.00","WINS":"0.00"}
            ]',
            'resultThisMonthDeposits' => (object)[
                'DEPOSITS' => '0.00',
                'MANUAL_DEPOSITS' => '0.00',
                'PAYOUT' => '0.00',
                'MANUAL_PAYOUT' => '0.00'
            ],
            'resultLastMonthDeposits' => (object)[
                'DEPOSITS' => '0.00',
                'MANUAL_DEPOSITS' => '0.00',
                'PAYOUT' => '0.00',
                'MANUAL_PAYOUT' => '0.00'
            ],
            'resultChartDeposits' => '[
            {"dtv_date":"2017-04","DEPOSITS":"0.00","MANUAL_DEPOSITS":"0.00","PAYOUT":"0.00","MANUAL_PAYOUT":"0.00","TOT_DEPOSITS":"0.00","TOT_PAYOUTS":"0.00"}
            ,{"dtv_date":"2017-03","DEPOSITS":"0.00","MANUAL_DEPOSITS":"0.00","PAYOUT":"0.00","MANUAL_PAYOUT":"0.00","TOT_DEPOSITS":"0.00","TOT_PAYOUTS":"0.00"}
            ,{"dtv_date":"2017-02","DEPOSITS":"0.00","MANUAL_DEPOSITS":"0.00","PAYOUT":"0.00","MANUAL_PAYOUT":"0.00","TOT_DEPOSITS":"0.00","TOT_PAYOUTS":"0.00"}
            ,{"dtv_date":"2017-01","DEPOSITS":"0.00","MANUAL_DEPOSITS":"0.00","PAYOUT":"0.00","MANUAL_PAYOUT":"0.00","TOT_DEPOSITS":"0.00","TOT_PAYOUTS":"0.00"}
            ,{"dtv_date":"2016-12","DEPOSITS":"0.00","MANUAL_DEPOSITS":"0.00","PAYOUT":"0.00","MANUAL_PAYOUT":"0.00","TOT_DEPOSITS":"0.00","TOT_PAYOUTS":"0.00"}
            ,{"dtv_date":"2016-11","DEPOSITS":"0.00","MANUAL_DEPOSITS":"0.00","PAYOUT":"0.00","MANUAL_PAYOUT":"0.00","TOT_DEPOSITS":"0.00","TOT_PAYOUTS":"0.00"}
            ]',
            'resultChartThirdpartyBet'=>'[
            {"label":"MicroGaming","value":"0.00"}
            ,{"label":"Midas","value":"0.00"}
            ,{"label":"Taishan","value":"0.00"}
            ,{"label":"AllBet","value":"0.00"}
            ,{"label":"AsiaGaming","value":"0.00"}
            ,{"label":"BBIN","value":"0.00"}
            ,{"label":"MoCaGame","value":"0.00"}
            ,{"label":"Oriental","value":"0.00"}
            ,{"label":"NewAsia","value":"0.00"}
            ,{"label":"StarGame","value":"0.00"}]',
            'resultThirdpartyBet' => [
                (object)[
                    'label' => 'MicroGaming',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'Midas',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'Taishan',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'AllBet',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'AsiaGaming',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'BBIN',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'MoCaGame',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'Oriental',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'NewAsia',
                    'value' => '0.00'
                ],
                (object)[
                    'label' => 'StarGame',
                    'value' => '0.00'
                ],
            ]

        );

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);
        return view('home', ['results' => $returnResult]);
    }

    public function apiGuideDoc()
    {

        $this->js['scripts'] = [
//            'assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js',

        ];

        $this->css['pages'] = [
//            'assets/plugins/morris/morris.css',
//            'assets/plugins/bootstrap/css/bootstrap.css',
        ];


        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);
        return view('/api/apiGuideDoc');

    }
}
