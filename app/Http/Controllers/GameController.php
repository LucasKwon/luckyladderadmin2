<?php

namespace App\Http\Controllers;

use App\Services\GameService;
use App\Services\MemberService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class GameController extends DragonController
{

    protected $gameService;
    protected $memberService;


    /**
     * GameController constructor.
     * @param GameService $gameService
     * @param MemberService $memberService
     */
    public function __construct(GameService $gameService, MemberService $memberService)
    {
        parent::__construct();
        $this->setApp();

        $this->gameService = $gameService;
        $this->memberService = $memberService;
    }

    public function viewBetLimits()
    {
        //dd(\request()->request->get('limitIdx'));

        $limit_idx = \request()->request->get('limitIdx');

        //dd($limit_idx, $max_bet, $min_bet, $max_tie, $min_tie, $op_parent_idx);
        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css',

            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'TableManageButtons.init();',
            'FormWizardValidation.init();',
            'FormPlugins.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $limitList = $this->gameService->getBettingLimits(Auth::user()->op_idx);
        $operatorList = $this->memberService->getOperatorListByOperatorIdx(Auth::user()->op_idx);

        $betLimitData = null;
        $betOperatorList = null;
        $betOpList = '';
        if(!is_null($limit_idx)){
            $betLimitData = $this->gameService->getBettingLimitByIdx($limit_idx);
            $betOperatorList = $this->gameService->getBettingLimitOperatorListByOppIdx(\request()->request->all());
            if(count($betOperatorList) > 0){
                for ($i = 0; $i < count($betOperatorList); $i++ ){
                    if( $i == 0 ){
                        $betOpList = $betOperatorList[$i]->op_idx;
                    } else {
                        $betOpList .= ',' . $betOperatorList[$i]->op_idx;
                    }
                }
            }
        }

        //dd($betOperatorList, $betOpList);

        return view('games.limitList', [
            'betLimitList'  => $limitList,
            'opList'        =>$operatorList,
            'betLimitData'  => $betLimitData,
            'opIdsArr'      => $betOpList]);

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * 베팅제한 등록
     */
    public function registerBetLimits()
    {
        //dd(\request()->request->all());
        $result = $this->gameService->registerBettingLimits(\request()->request->all());

        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            return redirect('/games/betlimits')->with('processResult', '베팅제한 정보를 등록했습니다.');
        }

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * 베팅리밋 업데이트
     */
    public function updateBetLimits()
    {
        //dd(\request()->request->all());
        $result = $this->gameService->updateBettingLimits(\request()->request->all());
        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            return redirect('/games/betlimits')->with('processResult', '베팅제한 정보를 변경했습니다.');
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * 베팅리밋 삭제
     */
    public function deleteBetLimits()
    {
        //dd(\request()->request->all());
        $result = $this->gameService->deleteBettingLimits(\request()->request->all());
        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            return redirect('/games/betlimits')->with('processResult', '베팅제한 정보를 삭제했습니다.');
        }

    }


    public function viewGameList()
    {
        //게임 리스트
        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $gameList = $this->gameService->getGameList();

        //dd($gameList);
        return view('games.gameList', ['gameList' => $gameList]);
    }

    public function viewGameDetail($gIdx)
    {
        //dd($gIdx);
        $this->js['scripts'] = [

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $gameDetailInfo = $this->gameService->getGameDetailInfo($gIdx);

        // dd($gameDetailInfo);

        return view('games.gameDetail', ['detail' => $gameDetailInfo]);


    }


    /**
     * @param $opIdx
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 게임 설정 -> 오퍼레이터 별 게임설정
     */
    public function viewOperatorList($opIdx)
    {

        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $memberList = $this->memberService->getOperatorListByOperatorIdx($opIdx);
        return view('games.operatorList', ['operatorList' => $memberList]);

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * 게임정보 업데이트
     */
    public function updateGameInfo()
    {
        //dd(\request()->request->all());

        $result = $this->gameService->updateGameInfo(\request()->request->all());

        //dd($result);
        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            //return Redirect('/games/status/detail/'.\request()->request->get('g_idx'))->with('processResult', '게임정보를 변경했습니다.');
            return redirect('/games/status/list')->with('processResult', '게임정보를 변경했습니다.');
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 게임테이블(리스트) 정보
     */
    public function viewGameTableList()
    {
        //dd(\request()->request->all());

        $tb_idx = \request()->request->get('tbIdx');

        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css',

            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'TableManageButtons.init();',
            'FormWizardValidation.init();',
            'FormPlugins.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $tableList = $this->gameService->getGameTableList();
        $gameList = $this->gameService->getGameList();
        $dealerList = $this->gameService->getDealerList();
        $operatorList = $this->memberService->getOperatorListByOperatorIdx(Auth::user()->op_idx);

        $tableData = null;
        $tabOpList ='';
        if(!is_null($tb_idx)){
            $tableData = $this->gameService->getTableDetailInfo($tb_idx);
            $tabOperatorList = $this->gameService->getTableOperatorList(\request()->request->all());
            if(count($tabOperatorList) > 0){
                for ($i = 0; $i < count($tabOperatorList); $i++ ){
                    if( $i == 0 ){
                        $tabOpList = $tabOperatorList[$i]->op_idx;
                    } else {
                        $tabOpList .= ',' . $tabOperatorList[$i]->op_idx;
                    }
                }
            }
            //dd($tabOpList);
        }

        return view('games.tableList', [
            'tableList'     => $tableList,
            'gameList'      => $gameList,
            'dealerList'    => $dealerList,
            'tableData'     => $tableData,
            'opList'        => $operatorList,
            'opIdsArr'      => $tabOpList,
        ]);

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * 테이블 정보 등록
     */
    public function registerGameTable()
    {
        //dd(\request()->request->all());
        $result = $this->gameService->registerGameTableInfo(\request()->request->all());
        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            return redirect('/games/table/list')->with('processResult', '테이블 정보를 등록했습니다.');
        }

    }


    /**
     * @param $tbIdx
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 테이블 상세정보
     */
    public function viewGameTableDetail($tbIdx)
    {
        $this->js['scripts'] = [

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $tableInfo = $this->gameService->getTableDetailInfo($tbIdx);
        $gameList = $this->gameService->getGameList();
        $dealerList = $this->gameService->getDealerList();


        return view('games.tableDetail', ['detail' => $tableInfo, 'gameList' => $gameList, 'dealerList' => $dealerList]);
    }

    public function updateGameTableInfo()
    {
        //dd(\request()->request->all());

        $result = $this->gameService->updateGameTableInfo(\request()->request->all());

        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            return redirect('/games/table/list')->with('processResult', '테이블정보를 변경했습니다.');
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * 테이블 정보 삭제
     */
    public function deleteGameTable()
    {
        $result = $this->gameService->deleteGameTable(\request()->request->all());
        if ($result['result'] == null) {
            return Redirect::to(URL::previous())->withInput()->withErrors($result['message']);
        } else {
            return redirect('/games/table/list')->with('processResult', '테이블정보를 삭제했습니다.');
        }
    }

    public function viewGameTableSetting($tbIdx)
    {
        $this->js['scripts'] = [

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $tableInfo = $this->gameService->getTableDetailInfo($tbIdx);
        $limitList = $this->gameService->getBettingLimits(Auth::user()->op_idx);
        $operatorList = $this->memberService->getOperatorListByOperatorIdx(Auth::user()->op_idx);

        return view('games.tableSetting', ['detail' => $tableInfo, 'opList' => $operatorList, 'limitList' => $limitList]);
    }
}
