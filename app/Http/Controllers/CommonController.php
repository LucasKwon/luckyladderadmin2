<?php

namespace App\Http\Controllers;

use App\Services\CommonService;
use App\Http\Requests;
use Illuminate\Http\Request;

class CommonController extends DragonController
{
    protected $commonService;


    /**
     * CommonController constructor.
     * @param CommonService $commonService
     */
    public function __construct(CommonService $commonService)
    {
        parent::__construct();
        $this->setApp();

        $this->commonService = $commonService;
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkOperatorNameDuplicate()
    {
        $userName = urldecode(request()->request->get('op_name'));

        //dd($userName);
        $result = $this->commonService->checkOperatorNameDuplicate($userName);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }


    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkPlayerNameDuplicate()
    {
        $userName = urldecode(request()->request->get('op_name'));

        $result = $this->commonService->checkPlayerNameDuplicate($userName);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * 아이디 중복확인
     */
    public function checkOperatorIdDuplicate()
    {
        $userId = request()->request->get('op_id');

        $result = $this->commonService->checkOperatorIdDuplicate($userId);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }

    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * 이메일 중복확인
     */
    public function checkOperatorEmailDuplicate()
    {
        $userEmail = urldecode(request()->request->get('op_email'));

        $result = $this->commonService->checkOperatorEmailDuplicate($userEmail);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }

    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * 게임플레이어 이메일 중복확인
     */
    public function checkPlayerEmailDuplicate()
    {
        $userEmail = urldecode(request()->request->get('op_email'));

        $result = $this->commonService->checkPlayerEmailDuplicate($userEmail);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }

    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkPlayerIdDuplicate()
    {
        $userId = urldecode(request()->request->get('op_id'));

        $result = $this->commonService->checkPlayerIdDuplicate($userId);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }


    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkOperatorPrefixDuplicate()
    {
        $userPrefix = request()->request->get('op_prefix');

        $result = $this->commonService->checkOperatorPrefixDuplicate($userPrefix);

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }

    /**
     * @param $opLevel
     * @param $opIdx
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function  checkMembersIdDuplicate($opLevel, $opIdx)
    {
        $mbId = \request()->request->get('user_id');
        //$opIdx = \request()->request->get('op');

        if ($opLevel == 1){
            $result = $this->commonService->checkOperatorIdDuplicate($mbId);
        }else{
            $result = $this->commonService->checkMemberIdDuplicate($mbId, $opIdx);
        }

        if (!empty($result)) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }


    /**
     * @param $gId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkGameIdDuplicate($gId)
    {
        $gameId = \request()->request->get('g_id');
        $result = $this->commonService->checkGameIdDuplicate($gameId);

        if ($result > 0 && strcmp($gId, $gameId) != 0) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }

    /**
     * @param $gTitle
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkGameTitleDuplicate($gTitle)
    {

        $gameTitle = \request()->request->get('g_title');
        //dd($gTitle, $gameTitle);
        $result = $this->commonService->checkGameTitleDuplicate($gameTitle);

        if ($result > 0 && strcmp($gTitle, $gameTitle) != 0) {
            return response('Duplicate', 200);
        } else {
            return response('OK', 404);
        }
    }


    /**
     * @param null $tName
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     *
     */
    public function checkTableNameDuplicate($tName = null)
    {
        $tableName = \request()->request->get('tb_name');

        //dd(\request()->request->all());
        $result = $this->commonService->checkTableNameDuplicate($tableName);

        if(is_null($tName)){
            if ($result > 0 ) {
                return response('Duplicate', 200);
            } else {
                return response('OK', 404);
            }
        }else{
            //dd($tName, $tableName, strcmp($tName, $tableName), $result);
            if ($result > 0 && strcmp($tName, $tableName) != 0) {
                return response('Duplicate', 200);
            } else {
                return response('OK', 404);
            }
        }


    }

}
