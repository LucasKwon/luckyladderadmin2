<?php

namespace App\Http\Controllers;

use App\Services\MemberService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class MemberController extends DragonController
{
    protected $memberService;

    public function __construct(MemberService $memberService)
    {

        //$this->middleware('auth');
        parent::__construct();
        $this->setApp();

        $this->memberService = $memberService;
    }


    public function getOperatorList($opIdx, $depth)
    {
        //dd(\request(), $opIdx, $depth);
        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);


        //
        if ($depth == 0) {
            $memberList = $this->memberService->getNewOperatorListByOperatorIdx($opIdx);

            return view('members.operatorList', ['operatorList' => $memberList]);
        } else if ($depth == 1) {
            $memberList = $this->memberService->getNewOperatorListByOperatorIdx($opIdx);
            $operatorDetailInfo = $this->memberService->getOperatorDetailByOperatorIdx($opIdx);
            // dd('player',$memberList, $depth, $operatorDetailInfo);
            return view('members.playerList', ['detail' => $operatorDetailInfo, 'playerList' => $memberList]);
        } else {
            $memberList = $this->memberService->getOperatorPlayerListByOperatorIdx($opIdx);
            $operatorDetailInfo = $this->memberService->getOperatorDetailByOperatorIdx($opIdx);

            //dd('player',$memberList, $depth, $operatorDetailInfo);
            return view('members.playerList', ['detail' => $operatorDetailInfo, 'playerList' => $memberList]);
        }

    }

    public function getGamePlayerList($opIdx, $depth)
    {
        //dd($opIdx);
        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $playerList = $this->memberService->getOperatorPlayerListByOperatorIdx($opIdx);
        $operatorDetailInfo = $this->memberService->getOperatorDetailByOperatorIdx($opIdx);

        //dd($playerList, $operatorDetailInfo, $depth);
        return view('members.operatorList', ['detail' => $operatorDetailInfo, 'playerList' => $playerList]);
    }

    public function getGamePlayerDetails($opIdx, $mbIdx)
    {
        // dd($opIdx, $mbIdx);
        $this->js['scripts'] = [

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $playerDetailInfo = $this->memberService->getOperatorPlayerDetailByOperatorIdx($opIdx, $mbIdx);
        $playerDetailInfo->admin_level = 4;
        $playerDetailInfo->role_name = 'PLAYER';
        //dd($playerDetailInfo);
        return view('members.playerDetail', ['detail' => $playerDetailInfo]);
    }

    public function setGamePlayerDetails()
    {

        $this->memberService->setGamePlayerDetails(\request()->request->all());

        return redirect('/members/list/player/' . request()->request->get('mb_idx') . '/0')
            ->with('processResult', '상용자 정보를 성공적으로 수정하였습니다.');

        /*
        if ($result == 1) {
            return redirect('/members/list/player/' . request()->request->get('mb_idx') . '/0')
                ->with('processResult', '상용자 정보를 성공적으로 수정하였습니다.');
        } else {
            return redirect::to(URL::previous())->withInput()->withErrors('사용자 정보를 수정할 수 없습니다.');
        }
        */
    }

    public function getOperatorDetails($opIdx, $opLvl)
    {
        $this->js['scripts'] = [
            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $memberDetail = $this->memberService->getOperatorDetailByOperatorIdx($opIdx);

//        dd($opIdx, $opLvl, $memberDetail);
        return view('members.operatorDetail', ['detail' => $memberDetail]);

    }

    /**
     *
     * @param null $operatorIdx
     * @return \Illuminate\Http\RedirectResponse
     *
     * Operator 상세정보 갱신
     */
    public function setOperatorDetails($operatorIdx = null)
    {
        //dd($operatorIdx);
        $validator = Validator::make(request()->request->all(), [
            'op_name' => 'unique:operators,op_name,' . request()->request->get('op_idx'),
            'op_email' => 'unique:operators,op_email,' . request()->request->get('op_idx'),
            'op_prefix' => 'unique:operators,op_prefix,' . request()->request->get('op_idx'),
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        } else {
            $this->memberService->setOperatorDetails(request()->request->all());
            return redirect('/members/list/detail/basic_info/g/' . request()->request->get('op_idx') . '/1')
                ->with('processResult', '정보를 성공적으로 갱신하였습니다.');
        }
    }

    public function createNewSimpleMember($opIdx)
    {
        //dd($opIdx, $opLvl, $depth);
        $this->js['scripts'] = [
            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',

        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $operatorDetailInfo = $this->memberService->getOperatorDetailByOperatorIdx($opIdx);

        return view('members.playerCreate', ['detail' => $operatorDetailInfo]);

    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * (간편)_사용자(벤더)생성 프로세스
     */
    public function createNewSimpleMemberProcess()
    {
        //dd( \request()->request->all());
        $opLvl = \request()->request->get('user_level');
        $adLvl = \request()->request->get('admin_level');
        $depth = 1;
        if ($opLvl == 1 && ($adLvl == 1 || $adLvl == 2)) {
            //최상위 레벨(SYSTEM)에서 SYSTEM 에 ADMIN 추가 시 => operators
            //최상위 레벨(SYSTEM)에서 ADMIN 에 OPERATOR 추가 시 => operators
            \request()->request->set('op_level', $adLvl + 1);
            $result = $this->memberService->createTemporaryOperatorProcess(\request()->request->all());
        } else if ($opLvl == 2 && $adLvl == 2) {
            //관리자 레벨(ADMIN)에서 ADMIN 에 OPERATOR 추가시 => operators
            \request()->request->set('op_level', $adLvl + 1);
            $result = $this->memberService->createTemporaryOperatorProcess(\request()->request->all());
        } else if ($opLvl == 2 && $adLvl == 3) {
            //관리자 레벨(ADMIN)에서 OPERATOR 에 PLAYER 추가시 => members
            $result = $this->memberService->createTemporaryGamePlayerProcess(\request()->request->all());
            $depth = 3;
        } else {
            $result = $this->memberService->createTemporaryGamePlayerProcess(\request()->request->all());
            $depth = 3;
        }

        if (is_null($result)) {
            return Redirect::to(URL::previous())->withInput()->withErrors('사용자를 생성할 수 없습니다.');
        } else {
            return redirect('/members/list/detail/user_list/' . \request()->request->get('op_idx') . '/' . $depth)
                ->with('processResult', '사용자를 성공적으로 생성하였습니다.');
        }

    }

    public function createNewDetailMember()
    {
        $this->js['scripts'] = [
            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',

        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        return view('members.createForm');

    }

    public function createNewDetailMemberProcess()
    {
        $result = $this->memberService->createNewDetailMemberProcess(request()->request->all());
        if ($result == 0) {
            return Redirect::to(URL::previous())->withInput()->withErrors('사용자를 생성할 수 없습니다.');
        } else {
            if (request()->request->get('op_level') < 3) {
                return redirect('/members/list/' . Auth::user()->op_idx . '/0')
                    ->with('processResult', '사용자를 성공적으로 생성하였습니다.');
            } else {
                return redirect('/members/list/player/' . Auth::user()->op_idx . '/0')
                    ->with('processResult', '플레이어를 성공적으로 생성하였습니다.');
            }

        }
    }

    public function getOperatorPointCalculationPage($type, $opIdx)
    {
        $this->js['scripts'] = [

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',

        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $operatorDetailInfo = $this->memberService->getOperatorDetailByOperatorIdx($opIdx);

        // dd(Auth::user(), $operatorDetailInfo);
        if ($type == "add") {
            return view('members.addPoint', ['detail' => $operatorDetailInfo]);
        } else {
            return view('members.subPoint', ['detail' => $operatorDetailInfo]);
        }

    }

    public function getPlayerPointCalculationPage($type, $opIdx, $mbIdx)
    {
        $this->js['scripts'] = [

            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',

        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',
        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        $playerDetailInfo = $this->memberService->getOperatorPlayerDetailByOperatorIdx($opIdx, $mbIdx);
        $playerDetailInfo->admin_level = 4;
        $playerDetailInfo->role_name = 'PLAYER';
        //dd($type, $opIdx, $mbIdx, Auth::user(), $playerDetailInfo);

        if ($type == "add") {
            return view('members.addPoint', ['detail' => $playerDetailInfo]);
        } else {
            return view('members.subPoint', ['detail' => $playerDetailInfo]);
        }
    }

    public function pointAddSubtractProcess4Operator()
    {
//        dd(\request()->request->all());
        $text = (\request()->request->get('process') == 'add') ? '지급' : '차감';
        $points = str_replace(",", "", \request()->request->get('op_point'));

        if ($points < 0) {
            return redirect::to(URL::previous())->withInput()->withErrors('0 포인트는 ' . $text . '할 수 없습니다.');
        } else {
            \request()->request->set('op_point', $points);
            $targetIdx = \request()->request->get('targetIdx');
            $loginIdx = \request()->request->get('loginIdx');

            $targetMember = $this->memberService->getOperatorDetailByOperatorIdx($targetIdx);
            $loginMember = $this->memberService->getOperatorDetailByOperatorIdx($loginIdx);

            if ($targetMember->op_status === "ACTIVATE") {
                if ($targetMember->op_cash_type === "PRE") {
                    if (\request()->request->get('process') == 'add') {
                        $result = $this->memberService->addPointFromVendor2Vendor(\request()->request->all(), $loginMember, $targetMember);
                    } else {
                        if ($targetMember->op_point - $points < 0 && Auth::user()->op_level > 1) {
                            return Redirect::to(URL::previous())->withInput()->withErrors('회수 포인트가 보유포인트 보다 많습니다. 보유포인트 : ' . (integer)$targetMember->op_point);
                        } else {
                            $result = $this->memberService->subPointFromVendor2Vendor(\request()->request->all(), $loginMember, $targetMember);
                        }
                    }

                    if ($result != null) {

                        if(\request()->request->get('rolName') == 'SUPER MASTER AGENT')
                        {
                            return redirect('/members/site/list/' . \request()->request->get('loginIdx') . '/0')->with('processResult', '성공적으로 포인트를 ' . $text . '하였습니다.');
                        }
                        else
                        {
                            return redirect('/members/list/' . \request()->request->get('loginIdx') . '/0')->with('processResult', '성공적으로 포인트를 ' . $text . '하였습니다.');
                        }

                    } else {
                        return Redirect::to(URL::previous())->withInput()->withErrors('사용자에게 포인트를 ' . $text . '할 수 없습니다.');
                    }

                } else {
                    return Redirect::to(URL::previous())->withInput()->withErrors('후불정산 사용자에게 포인트를 ' . $text . '할 수 없습니다.');
                }

            } else {
                return Redirect::to(URL::previous())->withInput()->withErrors('비활성 사용자에게 포인트를 ' . $text . '할 수 없습니다.');
            }
        }
    }

    public function pointAddSubtractProcess4Player()
    {
//                dd(\request()->request->all());
        $text = (\request()->request->get('process') == 'add') ? '지급' : '차감';
        $points = str_replace(",", "", \request()->request->get('op_point'));

        if ($points < 0) {
            return Redirect::to(URL::previous())->withInput()->withErrors('0 포인트는 ' . $text . '할 수 없습니다.');
        } else {
            \request()->request->set('op_point', $points);
            $targetIdx = \request()->request->get('targetIdx');
            $loginIdx = \request()->request->get('loginIdx');

            $targetPlayer = $this->memberService->getOperatorPlayerDetailByOperatorIdx($loginIdx, $targetIdx);
            $loginMember = $this->memberService->getOperatorDetailByOperatorIdx($loginIdx);

        }

        if ($targetPlayer->mb_status === "ACTIVATE") {
            if (\request()->request->get('process') == 'add') {
                //add
                if ($points > $loginMember->op_point) {
                    return Redirect::to(URL::previous())->withInput()->withErrors('지급 포인트가 보유포인트 보다 많습니다. 보유포인트 : ' . (integer)$loginMember->op_point);
                } else {
                    $result = $this->memberService->addPointFromVendor2Player(\request()->request->all(), $loginMember, $targetPlayer);
                }
            } else {
                //sub
                // dd($targetPlayer->mb_point , $points);
                if ($targetPlayer->mb_point < $points) {
                    return Redirect::to(URL::previous())->withInput()->withErrors('회수 포인트가 보유포인트 보다 많습니다. 보유포인트 : ' . (integer)$targetPlayer->mb_point);
                } else {
                    $result = $this->memberService->subPointFromPlayer2Vendor(\request()->request->all(), $loginMember, $targetPlayer);
                }
            }

            if ($result != null) {
                return redirect('/members/list/player/' . \request()->request->get('loginIdx') . '/0')->with('processResult', '성공적으로 포인트를 ' . $text . '하였습니다.');
            } else {
                return Redirect::to(URL::previous())->withInput()->withErrors('사용자에게 포인트를 ' . $text . '할 수 없습니다.');
            }
        } else {
            return Redirect::to(URL::previous())->withInput()->withErrors('비활성 사용자에게 포인트를 ' . $text . '할 수 없습니다.');
        }
    }


    public function createNewPlayer()
    {
        $this->js['scripts'] = [
            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',

        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        return view('members.createForm');

    }

    /**
     * @return View
     *
     * 멤버 생성 선택 페이지
     */
    public function createMemberSelect()
    {
        return view('members.createMemberSelect');
    }


    public function createAgentForm($levelName)
    {

        $this->js['scripts'] = [
            'assets/plugins/parsley/dist/parsley.js',
            'assets/plugins/parsley/dist/parsley.remote.js',
            'assets/plugins/bootstrap-wizard/js/bwizard.js',
            'assets/js/form-wizards-validation.demo.min.js',
            'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
            'assets/plugins/masked-input/masked-input.min.js',
            'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'assets/plugins/password-indicator/js/password-indicator.js',
            'assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
            'assets/plugins/bootstrap-select/bootstrap-select.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
            'assets/plugins/jquery-tag-it/js/tag-it.min.js',
            'assets/plugins/bootstrap-daterangepicker/moment.js',
            'assets/plugins/bootstrap-daterangepicker/daterangepicker.js',
            'assets/plugins/select2/dist/js/select2.min.js',
            'assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'assets/js/form-plugins.demo.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
            'assets/plugins/parsley/src/parsley.css',

        ];

        $this->initJs = [
            'FormWizardValidation.init();',
            'FormPlugins.init();',
            'Notification.init();'
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);

        return view('members.createAgentForm');
    }

    public function createAgentMemberProcess()
    {
        $result = $this->memberService->createAgentMemberProcess(request()->request->all());
        if ($result == 0) {
            return Redirect::to(URL::previous())->withInput()->withErrors('사용자를 생성할 수 없습니다.');
        } else {
        return redirect('/members/site/list/' . Auth::user()->op_idx . '/0')
            ->with('processResult', '사용자를 성공적으로 생성하였습니다.');
        }
    }


    public function siteList($opIdx, $depth)
    {
        //dd($opIdx);
        $this->js['scripts'] = [
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',
            'assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',
            'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
            'assets/js/table-manage-buttons.demo.min.js',
        ];

        $this->css['pages'] = [
            'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
            'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            'assets/plugins/simple-line-icons/simple-line-icons.css'
        ];

        $this->initJs = [
            'TableManageButtons.init();',
        ];

        View::share('js', $this->js);
        View::share('css', $this->css);
        View::share('initJs', $this->initJs);


        $params = array(
            'op_idx' => $opIdx,
            'op_level' => 4
        );

        $memberList = $this->memberService->getAgentListByOperatorIdx($params);

        return view('members.siteList', ['operatorList' => $memberList]);

    }


}
