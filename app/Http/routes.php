<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::get('/', 'HomeController@index');


Route::group(['prefix' => 'members'], function () {

    Route::get('/list/{opIdx}/{depth}', 'MemberController@getOperatorList');

    Route::get('/list/detail/basic_info/g/{opIdx}/{depth}', 'MemberController@getOperatorDetails');

    Route::post('/list/detail/basic_info/p/update', 'MemberController@setOperatorDetails');

    Route::get('/list/detail/user_list/{opIdx}/{depth}', 'MemberController@getOperatorList');

    Route::get('/list/detail/user_create/g/{opIdx}', 'MemberController@createNewSimpleMember');

    Route::post('/list/detail/user_create/p/process', 'MemberController@createNewSimpleMemberProcess');

    Route::get('/create/', 'MemberController@createNewDetailMember');

    Route::post('/create/process', 'MemberController@createNewDetailMemberProcess');

    Route::get('/list/detail/point/g/{type}/{opIdx}', 'MemberController@getOperatorPointCalculationPage');

    Route::post('/list/detail/point/p/add', 'MemberController@pointAddSubtractProcess4Operator');

    Route::post('/list/detail/point/p/sub', 'MemberController@pointAddSubtractProcess4Operator');

    Route::get('/list/player/{opIdx}/{depth}', 'MemberController@getGamePlayerList');

    Route::get('/list/detail/basic_info/g/player/{opIdx}/{mbIdx}/{depth}', 'MemberController@getGamePlayerDetails');

    Route::post('/list/detail/basic_info/p/player/update', 'MemberController@setGamePlayerDetails');

    Route::get('/list/detail/point/g/{type}/player/{opIdx}/{mbIdx}', 'MemberController@getPlayerPointCalculationPage');

    Route::post('/list/detail/point/p/add/player', 'MemberController@pointAddSubtractProcess4Player');

    Route::post('/list/detail/point/p/sub/player', 'MemberController@pointAddSubtractProcess4Player');

    Route::get('/create/player', 'MemberController@createNewDetailMember');

    Route::get('/select', 'MemberController@createMemberSelect');

    Route::get('/create/{levelName}/{opIdx?}', 'MemberController@createAgentForm');

    Route::post('/create/agent/process', 'MemberController@createAgentMemberProcess');

    Route::get('/site/list/{opIdx}/{depth}', 'MemberController@siteList');

});

Route::group(['prefix' => 'check'], function () {
    Route::get('/operators/id', 'CommonController@checkOperatorIdDuplicate');

    Route::get('/operators/name', 'CommonController@checkOperatorNameDuplicate');

    Route::get('/operators/email', 'CommonController@checkOperatorEmailDuplicate');

    Route::get('/operators/prefix', 'CommonController@checkOperatorPrefixDuplicate');

    Route::get('/members/id/{opLevel}/{opIdx}', 'CommonController@checkMembersIdDuplicate');

    Route::get('/player/name', 'CommonController@checkPlayerNameDuplicate');

    Route::get('/player/email', 'CommonController@checkPlayerEmailDuplicate');

    Route::get('/player/id', 'CommonController@checkPlayerIdDuplicate');

    Route::get('/game/id/{gId}', 'CommonController@checkGameIdDuplicate');

    Route::get('/game/title/{gTitle}', 'CommonController@checkGameTitleDuplicate');

    Route::get('/game/table/{tTitle?}', 'CommonController@checkTableNameDuplicate');

});

Route::group(['prefix' => 'account'], function () {
    Route::get('/request/{opIdx}', 'AccountController@ViewAccountRequest');

    Route::post('/addAccounting', 'AccountController@addAccountRequest');

    Route::get('/requested/{opIdx}', 'AccountController@ViewAccountRequested');

    Route::post('/updateAccounting', 'AccountController@updateAccountRequest');

    Route::post('/addRollingAccounting', 'AccountController@updateRollingAccountRequest');

});

Route::group(['prefix' => 'games'], function () {
    Route::get('/betlimits', 'GameController@viewBetLimits');

    Route::post('/betlimits/register', 'GameController@registerBetLimits');

    Route::post('/betlimits/update', 'GameController@updateBetLimits');

    Route::post('/betlimits/delete', 'GameController@deleteBetLimits');

    Route::get('/setting/{opIdx}', 'GameController@viewOperatorList');

    Route::get('/status/list', 'GameController@viewGameList');

    Route::get('/status/detail/index/{gIdx}', 'GameController@viewGameDetail');

    Route::post('/status/detail/update', 'GameController@updateGameInfo');

    Route::get('/table/list', 'GameController@viewGameTableList');

    Route::post('/table/register', 'GameController@registerGameTable');

    Route::get('/table/detail/index/{tbIdx}','GameController@viewGameTableDetail');

    Route::get('/table/setting/{tbIdx}', 'GameController@viewGameTableSetting');

    Route::post('/table/detail/update', 'GameController@updateGameTableInfo');

    Route::post('/table/delete', 'GameController@deleteGameTable');
});


Route::group(['prefix' => 'history'], function () {
    Route::get('/betwin/{opLevel}/{opIdx}', 'HistoryController@viewGameHistory');

    Route::get('/betwin/details/{trsId}/{trsType}/{checkResult}/{periodId}', 'HistoryController@viewGameHistoryDetail');

    // Route::post('/betwin/confirm', 'HistoryController@setGameHistoryConfirm');

});

Route::group(['prefix' => 'apiGuide'], function () {
    Route::get('/Doc', 'HomeController@apiGuideDoc');

});

Route::group(['prefix' => 'report'], function () {
    Route::get('/betwin/{opIdx}', 'ReportController@BetWinReport');

});