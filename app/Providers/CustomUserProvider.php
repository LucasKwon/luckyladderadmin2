<?php

namespace App\Providers;

use Carbon\Carbon;
use App\Models\Operators;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\ServiceProvider;

class CustomUserProvider implements UserProvider
{
    /**
     * @param mixed $identifier
     * @return null
     */
    public function retrieveById($identifier)
    {
        $query = Operators::where('operators.id', '=', $identifier);
        if ($query->count() > 0) {
            $user = Operators::where('operators.id', '=', $identifier)
                ->join('roles', 'roles.admin_level', '=', 'operators.op_level')
                ->first();
            $user->last_login_date = Carbon::now();
            $user->save();
            return $user;
        }
        return null;
    }

    /**
     * @param mixed $identifier
     * @param string $token
     * @return null
     */
    public function retrieveByToken($identifier, $token)
    {
        $query = Operators::where('operators.id', '=', $identifier)->where('remember_token', '=', $token);
        if ($query->count() > 0) {
            $user = Operators::where('operators.id', '=', $identifier)
                ->join('roles', 'roles.admin_level', '=', 'operators.op_level')
                ->first();
            return $user;
        }
        return null;
    }

    /**
     * @param Authenticatable $user
     * @param string $token
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);
        $user->save();
    }

    /**
     * @param array $credentials
     * @return null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $query = Operators::where('operators.op_id', '=', $credentials['email']);
        if ($query->count() > 0) {
            $user = Operators::where('operators.op_id', '=', $credentials['email'])->first();
            return $user;
        }
        return null;
    }

    /**
     * @param Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        //dd($user);
        if($user->op_id == $credentials['email']
            && $user->getAuthPassword() == sha1($credentials['password'])
            && $user->op_status == 'ACTIVATE' ){

            $user->last_login_date = Carbon::now();
            $user->save();

            return true;

        }
        return false;
    }
}
