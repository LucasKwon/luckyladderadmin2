@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li class="active">@lang('message.dashboard')</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.dashboard') <small>@lang('message.fixedDate') : {{date('Y-m-d',mktime(0,0,0,date('m'),1,date('Y')))}} ~ {{date('Y-m-d', strtotime('-1 days'))}}</small></h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon stats-icon-lg"><i style="font-size: 35px">BET</i></div>
                <div class="stats-title">@lang('message.currentMonthBets')</div>
                <div class="stats-number">{{number_format($results['resultThisMonth']->BETS,2)}}</div>
                <div class="stats-title">@lang('message.lastMonthBets')</div>
                <div class="stats-number">{{number_format($results['resultLastMonth']->BETS,2)}}</div>
                @if($results['resultLastMonth']->BETS == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonth']->BETS/$results['resultLastMonth']->BETS*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultThisMonth']->BETS/$results['resultLastMonth']->BETS*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon stats-icon-lg"><i style="font-size: 35px">WIN</i></div>
                <div class="stats-title">@lang('message.currentMonthWins')</div>
                <div class="stats-number">{{number_format($results['resultThisMonth']->WINS,2)}}</div>
                <div class="stats-title">@lang('message.lastMonthWins')</div>
                <div class="stats-number">{{number_format($results['resultLastMonth']->WINS,2)}}</div>
                @if($results['resultLastMonth']->WINS == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonth']->WINS/$results['resultLastMonth']->WINS*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultThisMonth']->WINS/$results['resultLastMonth']->WINS*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-purple">
                <div class="stats-icon stats-icon-lg"><i style="font-size: 40px">%</i></div>
                <div class="stats-title">@lang('message.currentMonthPayout')</div>
                <div class="stats-number">
                    @if($results['resultThisMonth']->BETS == 0)
                    0.0 %
                    @else
                    {{floor(($results['resultThisMonth']->WINS/$results['resultThisMonth']->BETS*100)*100)*.01}} %
                    @endif
                </div>
                <div class="stats-title">@lang('message.lastMonthPayout')</div>
                <div class="stats-number">
                    @if($results['resultLastMonth']->BETS == 0)
                    0.00 %
                    @else
                    {{floor(($results['resultLastMonth']->WINS/$results['resultLastMonth']->BETS*100)*100)*.01}} %
                    @endif
                </div>
                <div class="stats-progress progress">
                    @if($results['resultThisMonth']->BETS == 0)
                    <div class="progress-bar" style="width: 0.0%;"></div>
                    @else
                    @if($results['resultLastMonth']->BETS == 0)
                    <div class="progress-bar" style="width: 100.0%;"></div>
                    @else
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonth']->WINS/$results['resultThisMonth']->BETS*100)*100)*.01/floor(($results['resultLastMonth']->WINS/$results['resultLastMonth']->BETS*100)*100)*.01*100}}%;"></div>
                    @endif
                    @endif
                </div>
                <div class="stats-desc">
                    @if($results['resultThisMonth']->BETS == 0)
                    @lang('message.betterThanLastMonth') (0%)
                    @else
                    @if($results['resultLastMonth']->BETS == 0)
                    @lang('message.betterThanLastMonth') (100%)
                    @else
                    @lang('message.betterThanLastMonth') ({{number_format((floor(($results['resultThisMonth']->WINS/$results['resultThisMonth']->BETS*100)*100)*.01)/(floor(($results['resultLastMonth']->WINS/$results['resultLastMonth']->BETS*100)*100)*.01)*100,2)}}%)
                    @endif
                    @endif
                </div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-black">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
                <div class="stats-title">@lang('message.totalMembers')</div>
                <div class="stats-number">{{$results['resultTotalMembers']->cnt}}</div>
                <div class="stats-title">@lang('message.lastMonth') / @lang('message.currentMonth') / @lang('message.today')</div>
                <div class="stats-number">{{$results['resultLastMonthMembers']->cnt}} / {{$results['resultMonthMembers']->cnt}} / {{$results['resultTodayMembers']->cnt}}</div>
                @if($results['resultLastMonthMembers']->cnt == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultMonthMembers']->cnt/$results['resultLastMonthMembers']->cnt*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultMonthMembers']->cnt/$results['resultLastMonthMembers']->cnt*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-8">
            <div class="widget-chart with-sidebar bg-black">
                <div class="widget-chart-content-bet">
                    <h4 class="chart-title">
                        @lang('message.last6monthBetWin')
                        <small></small>
                    </h4>
                    <div id="visitors-line-chart" class="morris-inverse" style="height: 260px;"></div>
                </div>

            </div>
        </div>

    </div>

    <!-- Deposits -->
    <div class="row">
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-black">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
                <div class="stats-title">@lang('message.currentMonthDeposit')</div>
                <div class="stats-number">{{number_format($results['resultThisMonthDeposits']->DEPOSITS,2)}}</div>
                <div class="stats-title">@lang('message.lastMonthDeposit')</div>
                <div class="stats-number">{{number_format($results['resultLastMonthDeposits']->DEPOSITS,2)}}</div>
                @if($results['resultLastMonthDeposits']->DEPOSITS == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonthDeposits']->DEPOSITS/$results['resultLastMonthDeposits']->DEPOSITS*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultThisMonthDeposits']->DEPOSITS/$results['resultLastMonthDeposits']->DEPOSITS*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-purple">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
                <div class="stats-title">@lang('message.currentMonthWithdrawal')</div>
                <div class="stats-number">{{number_format($results['resultThisMonthDeposits']->PAYOUT,2)}}</div>
                <div class="stats-title">@lang('message.lastMonthWithdrawal')</div>
                <div class="stats-number">{{number_format($results['resultLastMonthDeposits']->PAYOUT,2)}}</div>
                @if($results['resultLastMonthDeposits']->PAYOUT == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonthDeposits']->PAYOUT/$results['resultLastMonthDeposits']->PAYOUT*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultThisMonthDeposits']->PAYOUT/$results['resultLastMonthDeposits']->PAYOUT*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
                <div class="stats-title">@lang('message.currentMonthManualDeposit')</div>
                <div class="stats-number">{{number_format($results['resultThisMonthDeposits']->MANUAL_DEPOSITS,2)}}</div>
                <div class="stats-title">@lang('message.lastMonthManualDeposit')</div>
                <div class="stats-number">{{number_format($results['resultLastMonthDeposits']->MANUAL_DEPOSITS,2)}}</div>
                @if($results['resultLastMonthDeposits']->MANUAL_DEPOSITS == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonthDeposits']->MANUAL_DEPOSITS/$results['resultLastMonthDeposits']->MANUAL_DEPOSITS*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultThisMonthDeposits']->MANUAL_DEPOSITS/$results['resultLastMonthDeposits']->MANUAL_DEPOSITS*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
                <div class="stats-title">@lang('message.currentMonthManualWithdrawal')</div>
                <div class="stats-number">{{number_format($results['resultThisMonthDeposits']->MANUAL_PAYOUT,2)}}</div>
                <div class="stats-title">@lang('message.lastMonthManualWithdrawal')</div>
                <div class="stats-number">{{number_format($results['resultLastMonthDeposits']->MANUAL_PAYOUT,2)}}</div>
                @if($results['resultLastMonthDeposits']->MANUAL_PAYOUT == 0)
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 0%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') (0.00%)</div>
                @else
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: {{floor(($results['resultThisMonthDeposits']->MANUAL_PAYOUT/$results['resultLastMonthDeposits']->MANUAL_PAYOUT*100)*100)*.01}}%;"></div>
                </div>
                <div class="stats-desc">@lang('message.betterThanLastMonth') ({{floor(($results['resultThisMonthDeposits']->MANUAL_PAYOUT/$results['resultLastMonthDeposits']->MANUAL_PAYOUT*100)*100)*.01}}%)</div>
                @endif
            </div>
        </div>
        <!-- end col-3 -->
    </div>
    <!-- begin row -->
    <div class="row">
        <div class="col-md-8">
            <div class="widget-chart with-sidebar bg-black">
                <div class="widget-chart-deposit_content">
                    <h4 class="chart-title">
                        @lang('message.last6monthDepositWithdrawal')
                        <small></small>
                    </h4>
                    <div id="deposits-line-chart" class="morris-inverse" style=" height: 260px;"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <!-- begin row -->

    <!-- end row -->
    <div id="chartField" data-field-id='{!!$results['resultChartBetWin']!!}' ></div>
<div id="chartDepositField" data-field-id='{!!$results['resultChartDeposits']!!}' ></div>
<div id="chartThirdpartyField" data-field-id='{!!$results['resultChartThirdpartyBet']!!}' ></div>
</div>
@endsection