@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gameHistory')</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.gameHistory')
        <small></small>
    </h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.searchBox') - @lang('message.detail') </h4>
                </div>
                <div class="panel-body ">
                    {!! Form::open(['class' => 'form-horizontal form-bordered', 'name' => 'searchForm', 'url'=>
                    '/history/betwin/'.Auth::user()->op_level.'/'.Auth::user()->op_idx.'', 'method' => 'get']) !!}
                    <input type="hidden" name="vendoridxarr" value="{{isset($searchParam['vendoridxarr']) ? $searchParam['vendoridxarr'] : ''}}"/>
                    <input type="hidden" name="vendoridarr" value="{{isset($searchParam['vendoridarr']) ? $searchParam['vendoridarr'] : ''}}"/>
                    <input type="hidden" name="checkynarr" value="{{isset($searchParam['checkynarr']) ? $searchParam['checkynarr'] : ''}}"/>
                    <div class="form-group">
                        <label class="control-label col-md-2">@lang('message.date')</label>
                        <div class="col-md-3">
                            <div class="row row-space-10">
                                <div class="input-group col-md-12">
                                    {!! Form::text('startdate', isset($searchParam['startdate']) ?
                                    $searchParam['startdate'] : '', ['id' => 'datetimepicker3','class' =>
                                    'form-control']) !!}
                                    <span class="input-group-addon">@lang('message.to')</span>
                                    {!! Form::text('enddate', isset($searchParam['enddate']) ? $searchParam['enddate']
                                    :'', ['id' => 'datetimepicker4','class' => 'form-control', 'maxlength' => '10' ])
                                    !!}
                                </div>
                            </div>
                        </div>
                        <label class="control-label col-md-1">@lang('message.lineUp')</label>
                        <div class="col-md-1">
                            <div class="row row-space-0">
                                <div class="controls col-md-12">
                                    {!! Form::select('lineUp',['ASC'=> trans("message.lineUpAsc"),
                                    'DESC'=>trans("message.lineUpDesc")],
                                    isset($searchParam['lineUp'])?$searchParam['lineUp']:'', [ 'class' =>
                                    'form-control', 'data-parsley-group' => 'wizard-step-1']) !!}
                                </div>
                            </div>
                        </div>
                        <label class="control-label col-md-2">@lang('message.playerId')</label>
                        <div class="col-md-3">
                            <div class="row row-space-10">
                                <div class="controls col-md-12">
                                    {!! Form::text('mbLoginId', isset($searchParam['mbLoginId']) ?
                                    $searchParam['mbLoginId'] : '', ['class' => 'form-control', 'maxlength' => '10' ])
                                    !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        @if(Auth::user()->op_level == 2 || Auth::user()->op_level == 3)
                        <label class="control-label col-md-2">@lang('message.operatorId')</label>
                        @else
                        <label class="control-label col-md-2">@lang('message.playerId')</label>
                        @endif
                        <div class="col-md-10">
                            <div class="row row-space-10">
                                <div class="controls col-md-12">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="vendorid_all" name="vendorids" value="">@lang('message.all')</label>

                                        @if(count($operatorList) > 0 )

                                        @foreach($operatorList as $opList)
                                        <label class="btn btn-sm btn-white">
                                            @if(Auth::user()->op_level == 2 ||  Auth::user()->op_level == 3)
                                                @if(isset($opList->op_id))
                                                <input type="checkbox" id="vendorids{{$opList->op_id}}" name="vendorids" value="{{$opList->op_id}}">{{$opList->op_id}}</label>
                                                @endif
                                            @else
                                            <input type="checkbox" id="vendorids{{$opList->mb_id}}" name="vendorids" value="{{$opList->mb_id}}">{{$opList->mb_id}}</label>
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">@lang('message.confirmResult')</label>
                        <div class="col-md-10">
                            <div class="row row-space-10">
                                <div class="controls col-md-12">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="checkyn_all" name="checkyn" value="">@lang('message.all')
                                        </label>
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="checkyn_y" name="checkyn" value="Y">@lang('message.complete')
                                        </label>
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="checkyn_n" name="checkyn" value="N">@lang('message.incomplete')
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <p id="btn_search">
                            <a href="javascript:;" class="btn btn-danger btn-block btn-lg">
                                @lang('message.searching')</a>
                        </p>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <!--                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>-->
                        <!--                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">@lang('message.gameHistory')</h4>
                </div>
                <div class="panel-body">
                    <table id="historyTable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th style="text-align:center;">@lang('message.operatorName')</th>
                            <th style="text-align:center;">@lang('message.playerId')</th>
                            <th style="text-align:center;">@lang('message.betWin')</th>
                            <th style="text-align:center;">@lang('message.beforeAmount')</th>
                            <th style="text-align:center;">@lang('message.betWinAmount')</th>
                            <th style="text-align:center;">@lang('message.totalAmount')</th>
                            <th style="text-align:center;">@lang('message.gameDate')</th>
                            <th style="text-align:center;">@lang('message.confirmResult')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($betwinHistoryList as $list)
                        @if($list->check_result == 'N')
                        {{--*/ $disabled = '' /*--}}
                        {{--*/ $className = 'btn btn-info btn-sm m-r-5 m-b-5' /*--}}
                        @else
                        {{--*/ $disabled = 'disabled=\'disabled\'' /*--}}
                        {{--*/ $className = 'btn btn-default btn-sm m-r-5 m-b-5' /*--}}
                        @endif
                        <tr class="odd gradeX" data-prdid="{{$list->period_id}}" data-trsid="{{$list->trs_id}}"
                            data-trstype="{{$list->trs_type}}" data-check="{{$list->check_result}}">
                            @if(Auth::user()->op_level == 2 )
                            <td>{{$list->op_name}}</td>
                            @else
                            <td>{{$list->op_name}}</td>
                            @endif
                            <td class="details-control" style="text-align:left;">{{$list->mb_id}}

                                <span class="label label-default"><a href="javascript:;"><i class="icon-share-alt"></i></a></span>

                            </td>
                            <td>{{$list->trs_type}}</td>
                            <td style="text-align:right;">{{number_format($list->before_amount,2)}}</td>
                            <td style="text-align:right;">{{number_format($list->trs_amount,2)}}</td>
                            <td style="text-align:right;">{{number_format($list->after_amount,2)}}</td>
                            <td style="text-align:center;">{{$list->trs_date}}</td>
                            <td style="text-align:center;">
                                <button type="button" id="btn{{$list->trs_id}}" class="{{$className}}" {{$disabled}}
                                        onclick="processGameResult('{{$list->period_id}}','{{$list->trs_id}}')">
                                    @if($list->check_result == 'Y')
                                    @lang('message.complete')
                                    @else
                                    @lang('message.confirm')
                                    @endif
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<script>
    $(document).ready(function () {
        //var lang = "/resource/{!! Session::get('applocale') !!}.json";

        var lang = "";
        var table = $('#historyTable').DataTable({
            dom: 'lBfrtip',
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', className: 'btn-sm'},
                {extend: 'excel', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
            responsive: true,
            autoFill: true,
            colReorder: true,
            keys: true,
            rowReorder: true,
            select: true,
            order: [7, 'desc'],
            language: {url: lang}

        });

        $('#historyTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');

            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(tr.data('trsid'), tr.data('trstype'), tr.data('check'))).show();
                tr.addClass('shown');
                loadChildRow(tr.data('trsid'), tr.data('trstype'), tr.data('check'), tr.data('prdid'));
            }
        });
    });

    function loadChildRow(trsid, trstype, check, prdid) {
        //var data = {'transactionId': trsid, 'transactionType': trstype, 'checkResult': check, 'periodId': prdid};
        $.ajax({
            type: 'GET',
            url: '/history/betwin/details/' + trsid + '/' + trstype + '/' + check + '/' + prdid,
            dataType: 'json',
            data: null,
            success: function (data) {
                if (data.result.gameResult != 'no result') {
                    //$('#jsonResult' + trsid + trstype).html(data.result.gameResult);
                    $('#jsonResultImg' + trsid + trstype).attr('src', '/assets/img/result/' + data.result.gameResult + '.png');

                } else {
                    $('#jsonResult' + trsid + trstype).html('@lang("message.noGameResult")');
                }
                if (data.result.gameTrsId != 'no result') {
                    $('#jsonTrsId' + trsid + trstype).html(data.result.gameTrsId);
                } else {
                    $('#jsonTrsId' + trsid + trstype).html('@lang("message.noTransId")');
                }
                if (data.result.gameDate != 'no result') {
                    $('#jsonGameDate' + trsid + trstype).html(data.result.gameDate);
                } else {
                    $('#jsonGameDate' + trsid + trstype).html('@lang("message.noGameDate")');
                }
                $('#jsonBetInfo' + trsid + trstype).html(data.result.betLocation);
                $('#jsonAmount' + trsid + trstype).html(data.result.betWinAmount);
                $('#jsonPrdId' + trsid + trstype).html(data.result.gamePrdId);
            }
        });
    };

    function format(trsid, trstype, check) {
        if (trstype == 'WIN') {
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>@lang("message.gameResult") : </td>' +
                '<td id="jsonResult' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                '<td><img id="jsonResultImg' + trsid + trstype + '" alt="" style="padding-left: 5px;"></td>' +
                '</tr>' +
                '<tr>' +
                '<td>@lang("message.transactionId") : </td>' +
                '<td id="jsonTrsId' + trsid + trstype + '" style="padding-left: 5px;" colspan="2"></td>' +
                '</tr>' +
                '<tr>' +
                '<td>@lang("message.resultDate") : </td>' +
                '<td id="jsonGameDate' + trsid + trstype + '" style="padding-left: 5px;" colspan="2"></td>' +
                '</tr>' +
                '<tr>' +
                '<td>@lang("message.betLocation") : </td>' +
                '<td id="jsonBetInfo' + trsid + trstype + '" style="padding-left: 5px;" colspan="2"></td>' +
                '</tr>' +
                '<tr>' +
                '<td>@lang("message.winAmount") :</td>' +
                '<td id="jsonAmount' + trsid + trstype + '" style="padding-left: 5px;" colspan="2"></td>' +
                '</tr>' +
                '</table>';
        } else {
            if (check == 'Y') {
                return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                    '<tr>' +
                    '<td>@lang("message.transactionId") : </td>' +
                    '<td id="jsonTrsId' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.resultDate") : </td>' +
                    '<td id="jsonGameDate' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.betLocation") : </td>' +
                    '<td id="jsonBetInfo' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.betAmount") : </td>' +
                    '<td id="jsonAmount' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '</table>';
            } else {
                return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                    '<tr>' +
                    '<td>@lang("message.gameResult") : </td>' +
                    '<td id="jsonResult' + trsid + trstype + '" style="padding-left: 5px;"><img id="jsonResultImg' + trsid + trstype + '" alt="" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.transactionId") : </td>' +
                    '<td id="jsonTrsId' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.resultDate") : </td>' +
                    '<td id="jsonGameDate' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.periodId") : </td>' +
                    '<td id="jsonPrdId' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.betLocation") : </td>' +
                    '<td id="jsonBetInfo' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>@lang("message.betAmount") : </td>' +
                    '<td id="jsonAmount' + trsid + trstype + '" style="padding-left: 5px;"></td>' +
                    '</tr>' +
                    '</table>';
            }
        }
    }

    function processGameResult(prdId, trsId) {
        var url = window.location.host;
        var target = url.split('.')[1] + "." + url.split('.')[2];

        //var data = {'periodId': prdId,'transactionId': trsId};

        $.ajax({
            type: 'GET',
            url: 'http://game.' + target + '/api/v1/omit/period/' + prdId + '/transaction/' + trsId,
            dataType: 'json',
            data: null,
            success: function (data) {
                if (data.statusCode == 200) {
                    //$('#btn' + trsId ).html('완료');
                    //btn-info
                    if (data.results != null) {
                        $('#btn' + trsId).html('@lang("message.complete")');
                        $('#btn' + trsId).removeClass('btn-info');
                        $('#btn' + trsId).addClass('btn-default');
                        $('#btn' + trsId).attr('disabled', 'disabled');
                    } else {
                        $('#btn' + trsId).html('@lang("message.incomplete")');
                        $('#btn' + trsId).removeClass('btn-info');
                        $('#btn' + trsId).addClass('btn-default');
                        $('#btn' + trsId).attr('disabled', 'disabled');

                        alert('@lang("message.noProcessing")');
                    }
                }
            }
        });
    }
</script>
<script>
    $(function () {
        var vendoridArray = $("[name=vendoridarr]").val().split(",");
        $.each(vendoridArray, function (i, item) {
            $("[name=vendorids]").each(function () {
                if ($(this).attr("value") == item) {
                    $(this).prop("checked", true);
                    $(this).parent().addClass("active");
                }
            });
        });

        $("[name=vendorids]").each(function () {
            $(this).parent().click(function () {
                var $btn_all = $("#vendorid_all").parent();
                if ($(this).index() == 0) {
                    $(this).siblings().removeClass("active");
                    $(this).siblings().find("[name=vendorids]").prop("checked", false);
                } else {
                    $btn_all.removeClass("active");
                    $btn_all.find("[name=vendorids]").prop("checked", false);
                }
            });
        });

        var checkYnArray = $("[name=checkynarr]").val().split(",");
        $.each(checkYnArray, function (i, item) {
            $("[name=checkyn]").each(function () {
                if ($(this).attr("value") == item) {
                    $(this).prop("checked", true);
                    $(this).parent().addClass("active");
                }
            });
        });

        $("[name=checkyn]").each(function () {
            $(this).parent().click(function () {
                var $btn_all = $("#checkyn_all").parent();
                if ($(this).index() == 0) {
                    $(this).siblings().removeClass("active");
                    $(this).siblings().find("[name=checkyn]").prop("checked", false);
                } else {
                    $btn_all.removeClass("active");
                    $btn_all.find("[name=checkyn]").prop("checked", false);
                }
            });
        });

        $("#btn_search").click(goSearch);
        $("[name=mbLoginId]").keypress(function (e) {
            if (event.keyCode == 13) {
                goSearch(e);
            }
        });

        function goSearch(e) {
            var f = document.searchForm;
            var arr = new Array();

            $("[name=vendorids]:checked").each(function () {
                arr.push($(this).val());
                $(this).prop("checked", false);
            });

            f.vendoridarr.value = arr.toString();

            var arr2 = new Array();

            $("[name=checkyn]:checked").each(function () {
                arr2.push($(this).val());
                $(this).prop("checked", false);
            });

            f.checkynarr.value = arr2.toString();

            f.submit();
            if (e) {
                e.preventDefault();
            }
        }
    });

</script>
@endsection