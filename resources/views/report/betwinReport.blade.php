@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.reports')</a></li>
        <li>@lang('menu.BETWIN')</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('menu.BETWIN')<small></small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" >
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.searchCondition') </h4>
                </div>
                <div class="panel-body ">
                    {!! Form::open(['class' => 'form-horizontal form-bordered',
                    'name' => 'searchForm', 'url'=> '/report/betwin/'.Auth::user()->op_idx.'',
                    'method' => 'get', 'data-parsley-validate' => 'true']) !!}
                    <div class="form-group">
                        <label class="control-label col-md-2">@lang('message.searchDuration')</label>
                        <div class="col-md-4">
                            <div class="row row-space-10">
                                <div class="input-group col-md-12">
                                    {!! Form::text('startdate', isset($searchParam['startdate']) ? $searchParam['startdate'] : '',
                                    ['id' => 'datetimepicker3','class' => 'form-control',
                                    'data-parsley-maxlength' => '10',
                                    'required']) !!}
                                    <span class="input-group-addon">@lang('message.to')</span>
                                    {!! Form::text('enddate', isset($searchParam['enddate']) ? $searchParam['enddate'] : '',
                                    ['id' => 'datetimepicker4','class' => 'form-control',
                                    'data-parsley-maxlength' => '10',
                                    'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-danger btn-block btn-lg">@lang('message.searching')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>


            <div class="panel panel-inverse" >
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.betWin')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table1" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.assignedMember')</th>
                                <th>@lang('message.betAmount')</th>
                                <th>@lang('message.winAmountReport')</th>
                                <th>@lang('message.bet-winReport')</th>
                                <th>@lang('message.payout') (%)</th>
                                <th>@lang('message.commissionReport') (%)</th>
                                <th>@lang('message.accountingAmount')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($accountingDetail)
                            @foreach($accountingDetail as $list)

                            <tr>
                                <td>{{$list->op_name}}</td>
                                <td>{{$list->op_parent_name}}</td>
                                <td style="text-align:right;">{{ number_format($list->bets, 2) }}</td>
                                <td style="text-align:right;">{{ number_format($list->wins, 2) }}</td>
                                <td style="text-align:right;">
                                    @if( $list->betwin > 0)
                                    <p class="text-info">{{ number_format($list->betwin, 2) }}</p>
                                    @else
                                    <p class="text-danger">{{ number_format($list->betwin, 2) }}</p>
                                    @endif
                                </td>
                                <td style="text-align:right;">{{ number_format( $list->payout, 2 ) }}%</td>
                                <td style="text-align:right;">{{ $list->op_profit }}%</td>
                                @if($list->op_cash_type == 'POST')
                                <td style="text-align:right;">{{ number_format( $list->amount, 2 ) }}</td>
                                @else
                                <td>@lang('message.PREPAIDReport')</td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<script>

    $("#btn_search").click(goSearch);
    $("[name=inpSearchKeyword]").keypress(function(e){
        if (event.keyCode==13) {
            goSearch(e);
        }
    });

    function goSearch(e){
        var f = document.searchForm;

        if(f.startdate.value == '' || f.enddate.value == '') {
            alert('please input Start Date & End Date');
            return false;
        }

        f.submit();
        if (e) {
            e.preventDefault();
        }
    }


</script>
@endsection