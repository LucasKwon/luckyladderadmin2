@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gamemanagement')</a></li>
        <li><a>@lang('message.gameTableList')</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">
        @lang('message.gameTableList')
        <small></small>
    </h1>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>-->
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">
                        @lang('message.gameTableList')
                    </h4>
                </div>

                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th style="text-align: center;">@lang('message.tableName')</th>
                            <th style="text-align: center;">@lang('message.tableCurrency')</th>
                            <th style="text-align: center;">@lang('message.tableStatus')</th>
                            <th style="text-align: center;">@lang('message.gameId')</th>
                            <th style="text-align: center;">@lang('message.machineId')</th>
                            <th style="text-align: center;">@lang('message.gameStatus')</th>
                            <th style="text-align: center;">@lang('message.registerDate')</th>
                            <th style="text-align: center;">@lang('message.statusChange')</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tableList as $list)
                        <tr class="odd gradeX">
                            <td style="text-align: center;">
                                <!--<a href="/games/table/detail/{{$list->tb_idx}}">-->
                                    {{$list->tb_name}}
                                <!--</a>-->
                            </td>
                            <td style="text-align: center;">{{$list->tb_currency}}</td>
                            <td style="text-align: center;">

                                @lang("message.$list->tb_status")
                            </td>
                            <td style="text-align: center;">{{$list->g_id}}</td>
                            <td style="text-align: center;">{{$list->machine_id}}</td>
                            <td style="text-align: center;">@lang("message.$list->g_status")</td>
                            <td style="text-align: center;">{{$list->tb_created_date}}</td>
                            <td style="text-align: center;">
                                <button type="button" id="btnDelete"
                                        onclick="updateTable('{{$list->tb_idx}}', '{{$list->tb_name}}', '{{$list->tb_currency}}', '{{$list->g_idx}}', '{{$list->d_idx}}', '{{$list->tb_status}}')"
                                        class="btn btn-primary btn-sm m-r-5 m-b-5">
                                    @lang('message.edit')
                                </button>
                                <button type="button" id="btnDelete"
                                        onclick="deleteTable('{{$list->tb_idx}}', '{{$list->tb_name}}', '{{$list->tb_currency}}', '{{$list->g_idx}}', '{{$list->d_idx}}', '{{$list->tb_status}}')"
                                        class="btn btn-danger btn-sm m-r-5 m-b-5">
                                    @lang('message.delete')
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <h4 class="panel-title">
                        @lang('message.registerTable')
                    </h4>
                </div>
                <div class="panel-body">
                    @if(isset($tableData->tb_name))
                    {{--*/ $tableUrl = '/games/table/detail/update' /*--}}
                    {{--*/ $checkUrl = '/check/game/table/'.$tableData->tb_name /*--}}
                    @else
                    {{--*/ $tableUrl = '/games/table/register' /*--}}
                    {{--*/ $checkUrl = '/check/game/table' /*--}}
                    @endif
                    <form action="{{$tableUrl}}" method="POST" data-parsley-validate="true" name="form-wizard">
                        @if(isset($tableData->tb_name))
                        {!! Form::hidden('tb_idx', $tableData->tb_idx) !!}
                        {!! Form::hidden('ptbName', $tableData->tb_name) !!}
                        {!! Form::hidden('ptbCurrency',  $tableData->tb_currency) !!}
                        {!! Form::hidden('pgIdx',  $tableData->g_idx) !!}
                        {!! Form::hidden('pdIdx',  $tableData->d_idx) !!}
                        {!! Form::hidden('pStatus',  $tableData->tb_status) !!}
                        {!! Form::hidden('opIdsArr', isset($opIdsArr)?$opIdsArr:'') !!}
                        @endif
                        {!! Form::hidden('opIds') !!}
                        {{ csrf_field() }}
                        <div id="wizard">
                            <ol>
                                <li>
                                    @lang('message.tableBasicInfo')
                                    <small>테이블 기본정보 설정.</small>
                                </li>
                                <li>
                                    @lang('message.gameInfo')
                                    <small>연결정보 1</small>
                                </li>
                                <li>
                                    @lang('message.agentList')
                                    <small>에이전트 선택</small>
                                </li>
                                <li>
                                    @lang('message.complete')
                                    <small></small>
                                </li>
                            </ol>

                            <div class="wizard-step-1">
                                <legend class="pull-left width-full">@lang('message.tableBasicInfo')</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group block1">
                                            <label>@lang('message.tableName')</label>
                                            {!!
                                            Form::text('tb_name',
                                            isset($tableData->tb_name)?$tableData->tb_name:'',
                                            ['class' => 'form-control',
                                            'data-parsley-pattern' => '/[^가-힣ㄱ-ㅎㅏ-ㅣ]/',
                                            'data-parsley-group' => 'wizard-step-1',
                                            'data-parsley-minlength'=>'6',
                                            'data-parsley-maxlength' => '45',
                                            'data-parsley-trigger' => 'change',
                                            'data-parsley-remote' => $checkUrl,
                                            'data-parsley-remote-validator' => 'reverse',
                                            'data-parsley-remote-message' => '사용중입니다.',
                                            'data-parsley-minlength-message' => '최소 6자 이상이어야 합니다.',
                                            'data-parsley-maxlength-message' => '최대 45자 이하이어야 합니다.',
                                            'data-parsley-pattern-message' => '허용하지 않는 문자입니다.',
                                            'data-parsley-required-message' => '필수항목입니다.'
                                            , 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.tableCurrency')</label>
                                            {!! Form::select('tb_currency',
                                            ['KRW'=>'KRW', 'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'],
                                            isset($tableData->tb_currency) ? $tableData->tb_currency : '',
                                            [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.tableStatus')</label>
                                            {!! Form::select('tb_status',
                                            [ 'ACTIVATE'=>trans('message.activate'), 'DEACTIVATE'=>trans('message.deactivate') ],
                                            isset($tableData->tb_status) ? $tableData->tb_status : '',
                                            ['class' => 'form-control', 'data-parsley-group' => 'wizard-step-1', 'required']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-step-2">
                                <legend class="pull-left width-full">@lang('message.gameInfo')</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.gameList')</label>
                                            <select id="g_idx" name="g_idx" class="form-control"
                                                    data-parsley-group="wizard-step-2" required>
                                                @foreach($gameList as $list)
                                                <option value="{{$list->g_idx}}">{{$list->g_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.dealerList')</label>
                                            <select id="d_idx" name="d_idx" class="form-control"
                                                    data-parsley-group="wizard-step-2" required>
                                                @foreach($dealerList as $list)
                                                <option value="{{$list->d_idx}}">{{$list->d_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-step-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>@lang('message.agentList')</label><br>
                                            <div data-toggle="buttons" class="btn-group" style="width: 88%;">
                                                @foreach($opList as $list)
                                                <label class="btn btn-sm btn-white">
                                                    <input type="checkbox" id="limitids{{$list->op_idx}}" name="opids"
                                                           value="{{$list->op_idx}}">{{$list->op_name}}
                                                </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="jumbotron m-b-0 text-center">
                                    <button type="submit" class="btn btn-success btn-lg">
                                        @if(isset($tableData->tb_name))
                                        @lang('message.modify')
                                        @else
                                        @lang('message.register')
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<form name="updateForm" method="POST">
    <input type="hidden" name="tbIdx">
    <input type="hidden" name="tbName">
    <input type="hidden" name="tbCurrency">
    <input type="hidden" name="tbGIdx">
    <input type="hidden" name="tbDIdx">
    <input type="hidden" name="tbStatus">
    {{ csrf_field() }}
</form>
<script>
    $(document).ready(function () {
        @if(isset($tableData->g_idx))
        setTimeout(function () {
            $("#g_idx").val({{isset($tableData->g_idx)?$tableData->g_idx:''}}).attr("selected", 'selected');
            $("#d_idx").val({{isset($tableData->d_idx)?$tableData->d_idx:''}}).attr("selected", 'selected');
        }, 200);

        var opIdsArray = $("[name=opIdsArr]").val().split(",");

        $.each(opIdsArray, function (i, item) {
            $("[name=opids]").each(function () {
                if ($(this).attr("value") == item) {
                    $(this).prop("checked", true);
                    $(this).parent().addClass("active");

                    opidCheck()
                }
            });
        });

        @endif

        $("[name=opids]").each(function () {
            $(this).parent().click(function () {
                //var $btn_all = $("#opids_all").parent();
                //$btn_all.removeClass("active");
                //$btn_all.find("[name=opids]").prop("checked", false);
                opidCheck();
                /*
                if ($(this).index() == 0) {
                    $(this).siblings().removeClass("active");
                    $(this).siblings().find("[name=opids]").prop("checked", false);
                    opidCheck();
                } else {
                    $btn_all.removeClass("active");
                    $btn_all.find("[name=opids]").prop("checked", false);
                    opidCheck();
                }
                */
            });
        });

        function opidCheck() {
            var limitArray = new Array();
            setTimeout(function () {
                $("[name=opids]:checked").each(function () {
                    limitArray.push($(this).val());
                });
                $("[name=opIds]").val(limitArray.toString());
            }, 200);
        }
    });

    function deleteTable(tbIdx, tbName, tbCurrency, tbGIdx, tbDIdx, tbStatus) {
        var f = document.updateForm;
        f.tbIdx.value = tbIdx;
        f.tbName.value = tbName;
        f.tbCurrency.value = tbCurrency;
        f.tbGIdx.value = tbGIdx;
        f.tbDIdx.value = tbDIdx;
        f.tbStatus.value = tbStatus;
        f.action = "/games/table/delete";
        f.submit();
    }

    function updateTable(tbIdx, tbName, tbCurrency, tbGIdx, tbDIdx, tbStatus) {
        var f = document.updateForm;
        f.method = 'GET';
        f.tbIdx.value = tbIdx;
        f.tbName.value = tbName;
        f.tbCurrency.value = tbCurrency;
        f.tbGIdx.value = tbGIdx;
        f.tbDIdx.value = tbDIdx;
        f.action = "/games/table/list";
        f.submit();
    }
</script>
@endsection