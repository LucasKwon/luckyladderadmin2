@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gamemanagement')</a></li>
        <li><a>@lang('message.betLimitList')</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">
        @lang('message.betLimitList')
        <small></small>
    </h1>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>-->
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">
                        @lang('message.betLimitList')
                    </h4>
                </div>

                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <!--<th style="text-align: center;">@lang('message.operator')</th>-->
                            <th style="text-align: center;">@lang('message.maxBetting')</th>
                            <th style="text-align: center;">@lang('message.minBetting')</th>
                            <th style="text-align: center;">@lang('message.maxTieBetting')</th>
                            <th style="text-align: center;">@lang('message.minTieBetting')</th>
                            <th style="text-align: center;">@lang('message.registerDate')</th>
                            <th style="text-align: center;">@lang('message.statusChange')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($betLimitList as $list)
                        <tr class="odd gradeX">
                            <!--<td style="text-align: center;">{{$list->op_name}}</td>-->
                            <td style="text-align: right;">{{number_format($list->max_bet,0)}}</td>
                            <td style="text-align: right;">{{number_format($list->min_bet,0)}}</td>
                            <td style="text-align: right;">{{number_format($list->max_tie,0)}}</td>
                            <td style="text-align: right;">{{number_format($list->min_tie,0)}}</td>
                            <td style="text-align: center;">{{$list->register_date}}</td>
                            <td style="text-align: center;">
                                <button type="button" id="btnDelete"
                                        onclick="updateLimtsStatus({{$list->limit_idx}},{{$list->max_bet}}, {{$list->min_bet}},{{$list->max_tie}}, {{$list->min_tie}}, {{$list->op_parent_idx}})"
                                        class="btn btn-primary btn-sm m-r-5 m-b-5">
                                    @lang('message.edit')
                                </button>
                                <button type="button" id="btnDelete" onclick="deleteLimtsStatus({{$list->limit_idx}},{{$list->max_bet}}, {{$list->min_bet}},{{$list->max_tie}}, {{$list->min_tie}}, {{$list->op_parent_idx}})"
                                        class="btn btn-danger btn-sm m-r-5 m-b-5">
                                    @lang('message.delete')
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <h4 class="panel-title">
                        @lang('message.registerBetLimit')
                    </h4>
                </div>
                <div class="panel-body">
                    @if(isset($betLimitData->max_bet))
                    {{--*/ $actionUrl = '/games/betlimits/update' /*--}}
                    {!! Form::hidden('limitIdx', $betLimitData->limit_idx) !!}
                    @else
                    {{--*/ $actionUrl = '/games/betlimits/register' /*--}}
                    @endif
                    <form action="{{$actionUrl}}" method="POST" data-parsley-validate="true" name="form-wizard">
                        @if(isset($betLimitData->max_bet))
                        {!! Form::hidden('limitIdx', $betLimitData->limit_idx) !!}
                        {!! Form::hidden('pmaxBet',  $betLimitData->max_bet) !!}
                        {!! Form::hidden('pminBet',  $betLimitData->min_bet) !!}
                        {!! Form::hidden('pmaxTie',  $betLimitData->max_tie) !!}
                        {!! Form::hidden('pminTie',  $betLimitData->min_tie) !!}
                        {!! Form::hidden('opIdsArr', isset($opIdsArr)?$opIdsArr:'') !!}
                        @endif
                        {!! Form::hidden('opIds') !!}
                        {{ csrf_field() }}
                        <div id="wizard">
                            <ol>
                                <li>
                                    @lang('message.maxBetting')
                                    <small>최대/최소 베팅 설정</small>
                                </li>
                                <li>
                                    @lang('message.tieMaxBetting')
                                    <small>최대/최소 타이 베팅 설정</small>
                                </li>
                                <li>
                                    @lang('message.agentList')
                                    <small>에이전트 선택</small>
                                </li>
                                <li>
                                    @lang('message.complete')
                                    <small></small>
                                </li>
                            </ol>

                            <div class="wizard-step-1">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('message.maxBetting')</label>
                                            {!! Form::text('max_bet',
                                            isset($betLimitData->max_bet)?number_format($betLimitData->max_bet,0):''
                                            , [
                                            'class' => 'form-control',
                                            'data-parsley-trigger' => 'input',
                                            'data-parsley-pattern' => '/^[0-9,.]*$/',
                                            'data-parsley-pattern-message' => '숫자만 입력가능합니다.',
                                            'data-parsley-required-message' => '필수 입력항목입니다.',
                                            'data-parsley-group' => 'wizard-step-1',
                                            'style' => 'text-align:right',
                                            'required'
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('message.minBetting')</label>
                                            {!! Form::text('min_bet',
                                            isset($betLimitData->min_bet)?number_format($betLimitData->min_bet,0):''
                                            , [
                                            'class' => 'form-control',
                                            'data-parsley-trigger' => 'input',
                                            'data-parsley-pattern' => '/^[0-9,.]*$/',
                                            'data-parsley-pattern-message' => '숫자만 입력가능합니다.',
                                            'data-parsley-required-message' => '필수 입력항목입니다.',
                                            'data-parsley-group' => 'wizard-step-1',
                                            'style' => 'text-align:right',
                                            'required'
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-step-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('message.maxTieBetting')</label>
                                            {!! Form::text('max_tie',
                                            isset($betLimitData->max_tie)?number_format($betLimitData->max_tie,0):''
                                            , [
                                            'class' => 'form-control',
                                            'data-parsley-trigger' => 'input',
                                            'data-parsley-pattern' => '/^[0-9,.]*$/',
                                            'data-parsley-pattern-message' => '숫자만 입력가능합니다.',
                                            'data-parsley-required-message' => '필수 입력항목입니다.',
                                            'data-parsley-group' => 'wizard-step-2',
                                            'style' => 'text-align:right',
                                            'required'
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('message.minTieBetting')</label>
                                            {!! Form::text('min_tie',
                                            isset($betLimitData->min_tie)?number_format($betLimitData->min_tie,0):''
                                            , [
                                            'class' => 'form-control',
                                            'data-parsley-trigger' => 'input',
                                            'data-parsley-pattern' => '/^[0-9,.]*$/',
                                            'data-parsley-pattern-message' => '숫자만 입력가능합니다.',
                                            'data-parsley-required-message' => '필수 입력항목입니다.',
                                            'data-parsley-group' => 'wizard-step-2',
                                            'style' => 'text-align:right',
                                            'required'
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-step-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>@lang('message.agentList')</label><br>
                                            <div data-toggle="buttons" class="btn-group" style="width: 88%;">
                                                @foreach($opList as $list)
                                                <label class="btn btn-sm btn-white">
                                                    <input type="checkbox" id="limitids{{$list->op_idx}}" name="opids"
                                                           value="{{$list->op_idx}}">{{$list->op_name}}
                                                </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="jumbotron m-b-0 text-center">
                                    <button type="submit" class="btn btn-success btn-lg">
                                        @if(isset($betLimitData->max_bet))
                                        @lang('message.modify')
                                        @else
                                        @lang('message.register')
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<form name="updateForm" method="POST">
    <input type="hidden" name="limitIdx">
    <input type="hidden" name="maxbet">
    <input type="hidden" name="minbet">
    <input type="hidden" name="maxtie">
    <input type="hidden" name="mintie">
    <input type="hidden" name="oppIdx">
    {{ csrf_field() }}
</form>
<script>
    $(document).ready(function () {
        @if (isset($betLimitData->max_bet))

        var opIdsArray = $("[name=opIdsArr]").val().split(",");

        $.each(opIdsArray, function (i, item) {
            $("[name=opids]").each(function () {
                if ($(this).attr("value") == item) {
                    $(this).prop("checked", true);
                    $(this).parent().addClass("active");

                    opidCheck()
                }
            });
        });

        @endif

        $("[name=opids]").each(function () {
            $(this).parent().click(function () {
                //var $btn_all = $("#opids_all").parent();
                //$btn_all.removeClass("active");
                //$btn_all.find("[name=opids]").prop("checked", false);
                opidCheck();
                /*
                if ($(this).index() == 0) {
                    $(this).siblings().removeClass("active");
                    $(this).siblings().find("[name=opids]").prop("checked", false);
                    opidCheck();
                } else {
                    $btn_all.removeClass("active");
                    $btn_all.find("[name=opids]").prop("checked", false);
                    opidCheck();
                }
                */
            });
        });

        function opidCheck() {
            var limitArray = new Array();
            setTimeout(function () {
                $("[name=opids]:checked").each(function () {
                    limitArray.push($(this).val());
                });
                $("[name=opIds]").val(limitArray.toString());
            }, 200);
        }
    });
    //    function changeLimitsStatus(midx, state) {
    //
    //        //console.log(midx, status);
    //        var f = document.updateForm;
    //        f.limitIdx.value = midx;
    //        f.reqStatus.value = state;
    //        f.action = "/games/betlimits/update";
    //        f.submit();
    //
    //    }


    function deleteLimtsStatus(midx, maxbet, minbet, maxtie, mintie, oppidx) {
        var f = document.updateForm;
        f.limitIdx.value = midx;
        f.maxbet.value = maxbet;
        f.minbet.value = minbet;
        f.maxtie.value = maxtie;
        f.mintie.value = mintie;
        f.oppIdx.value = oppidx;
        f.action = "/games/betlimits/delete";
        f.submit();
    }

    function updateLimtsStatus(midx, maxbet, minbet, maxtie, mintie, oppidx) {
        var f = document.updateForm;
        f.method = "GET";
        f.limitIdx.value = midx;
        f.maxbet.value = maxbet;
        f.minbet.value = minbet;
        f.maxtie.value = maxtie;
        f.mintie.value = mintie;
        f.oppIdx.value = oppidx;
        f.action = "/games/betlimits";
        f.submit();
    }

    $("[name=max_bet]").css("ime-mode", "disabled");
    $("[name=max_bet]").keyup(function () {
        var str = this.value.replace(/[\,]/g, "");
        $("[name=max_bet]").val(str.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    })

    $("[name=min_bet]").css("ime-mode", "disabled");
    $("[name=min_bet]").keyup(function () {
        var str = this.value.replace(/[\,]/g, "");
        $("[name=min_bet]").val(str.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    })

    $("[name=max_tie]").css("ime-mode", "disabled");
    $("[name=max_tie]").keyup(function () {
        var str = this.value.replace(/[\,]/g, "");
        $("[name=max_tie]").val(str.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    })

    $("[name=min_tie]").css("ime-mode", "disabled");
    $("[name=min_tie]").keyup(function () {
        var str = this.value.replace(/[\,]/g, "");
        $("[name=min_tie]").val(str.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    })
</script>
@endsection