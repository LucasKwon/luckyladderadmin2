@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gamemanagement')</a></li>
        <li><a href="javascript:;">
                @lang('message.gameStatus')
            </a>
        </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.gameDetails')
        <small>@lang('message.basicInfo')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <div class="vertical-box-column width-250">
            @include('layouts._gameDetail_leftMenu')
        </div>
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-GAME">GAME</span>
                    {{$detail->g_title}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.machineId') : {{$detail->machine_id}}
                                <span class="label label-inverse"></span>
                            </span>

                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.bettime') :
                                {{$detail->g_bettime}}
                            </span>

                        </div>
                    </li>

                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.resttime') :
                                 {{$detail->g_resttime}}
                            </span>
                        </div>
                    </li>
                </ul>
                <div class="table-responsive">
                    <form action="/games/status/detail/update" method="POST" data-parsley-validate="true"
                          name="form-wizard">
                        {!! Form::hidden('g_idx', $detail->g_idx) !!}
                        {{ csrf_field() }}
                        <table class="table table-profile">
                            <tbody>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.id')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_id', $detail->g_id, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'6',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-remote' => '/check/game/id/'.$detail->g_id,
                                        'data-parsley-remote-validator' => 'reverse',
                                        'data-parsley-pattern' => '/^[A-Z,a-z,0-9,-,_]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-remote-message' => '사용중인 게임 ID 입니다.',
                                        'data-parsley-minlength-message' => '게임 ID는 6자 이상입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '45', 'required' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.gameTitle')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_title', $detail->g_title, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'6',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-remote' => '/check/game/title/'.$detail->g_title,
                                        'data-parsley-remote-validator' => 'reverse',
                                        'data-parsley-pattern' => '/^[A-Z,a-z,0-9\s]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-remote-message' => '사용중인 게임 ID 입니다.',
                                        'data-parsley-minlength-message' => '게임 ID는 6자 이상입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '45', 'required' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.gameStatus')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('g_status',
                                        ['MAINTENANCE'=>'MAINTENANCE', 'READY'=>'READY','ACTIVE'=>'ACTIVE'],
                                        isset($detail->g_status)?$detail->g_status:'',
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.betTime')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_bettime', $detail->g_bettime, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'1',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-pattern' => '/^[0-9]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '3', 'required', 'disabled']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.restTime')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_resttime', $detail->g_resttime, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'1',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-pattern' => '/^[0-9]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '3', 'required', 'disabled']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.delayTime')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_delaytime', $detail->g_delaytime, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'1',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-pattern' => '/^[0-9]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '5', 'required']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.machineStatus')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('g_org_status',
                                        ['DISCONNECTED'=>'DISCONNECTED', 'CONNECTED'=>'CONNECTED'],
                                        isset($detail->g_org_status)?$detail->g_org_status:'',
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.gameShoe')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_shoe', $detail->g_shoe, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'1',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-pattern' => '/^[0-9]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '2', 'required', 'disabled']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.gameCount')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_count', $detail->g_count, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'1',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-pattern' => '/^[0-9]*$/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '2', 'required', 'disabled']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.gameVideo')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('g_mov_url', $detail->g_mov_url, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-required-message' => '필수항목입니다.', 'required' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @lang('message.modifyInformation')
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
<script>
    $("#btn_update").click(checkRequestForm);
    function checkRequestForm() {
        if (confirm('@lang("message.proceedConfirm")')) {
        } else return false;
    }
</script>
@endsection