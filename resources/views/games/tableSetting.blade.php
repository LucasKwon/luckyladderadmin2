@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gamemanagement')</a></li>
        <li><a href="javascript:;">
                @lang('message.tableDetails')
            </a>
        </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.tableDetails')
        <small>@lang('message.tableSetting')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <div class="vertical-box-column width-250">
            @include('layouts._tableDetail_leftMenu')
        </div>
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-GAME">TABLE</span>
                    {{$detail->tb_name}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.machineId') : {{$detail->machine_id}}
                                <span class="label label-inverse"></span>
                            </span>

                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.bettime') :
                                {{$detail->g_bettime}}
                            </span>

                        </div>
                    </li>

                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.resttime') :
                                 {{$detail->g_resttime}}
                            </span>
                        </div>
                    </li>
                </ul>
                <div class="table-responsive">
                    <form action="/games/table/detail/update" method="POST" data-parsley-validate="true"
                          name="form-wizard">
                        {!! Form::hidden('tb_idx', $detail->tb_idx) !!}
                        {!! Form::hidden('limitIds', '') !!}
                        {!! Form::hidden('opIds', '') !!}
                        {{ csrf_field() }}
                        <table class="table table-profile">
                            <tbody>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.operatorList')</td>
                                <td>
                                    <div data-toggle="buttons" class="btn-group" style="width: 88%;">
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="opids_all" name="opids" value="">@lang('message.all')
                                        </label>
                                        @foreach($opList as $list)
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="limitids{{$list->op_idx}}" name="opids"
                                                   value="{{$list->op_idx}}">{{$list->op_name}}
                                        </label>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.limitList')</td>
                                <td>
                                    <div data-toggle="buttons" class="btn-group" style="width: 88%;">
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="limitid_all" name="limitids" value="">@lang('message.all')
                                        </label>
                                        @foreach($limitList as $list)
                                        <label class="btn btn-sm btn-white">
                                            <input type="checkbox" id="limitids{{$list->limit_idx}}" name="limitids"
                                                   value="{{$list->limit_idx}}">{{number_format($list->max_bet,0)}}
                                        </label>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @lang('message.modifyInformation')
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
<script>
    $(document).ready(function () {
        //$("#g_idx").val({{$detail->g_idx}}).attr("selected", 'selected');
        //$("#d_idx").val({{$detail->d_idx}}).attr("selected", 'selected');

        $("[name=limitids]").each(function () {
            $(this).parent().click(function () {
                var $btn_all = $("#limitid_all").parent();
                if ($(this).index() == 0) {
                    $(this).siblings().removeClass("active");
                    $(this).siblings().find("[name=limitids]").prop("checked", false);
                    limitIdCheck();
                } else {
                    $btn_all.removeClass("active");
                    $btn_all.find("[name=limitids]").prop("checked", false);
                    limitIdCheck();
                }
            });
        });

        function limitIdCheck() {
            var limitArray = new Array();
            setTimeout(function () {
                $("[name=limitids]:checked").each(function () {
                    limitArray.push($(this).val());
                });
                $("[name=limitIds]").val(limitArray.toString());

                console.log($("[name=limitIds]").val());
            }, 200);
        }


        $("[name=opids]").each(function () {
            $(this).parent().click(function () {
                var $btn_all = $("#opids_all").parent();
                if ($(this).index() == 0) {
                    $(this).siblings().removeClass("active");
                    $(this).siblings().find("[name=opids]").prop("checked", false);
                    opidCheck();
                } else {
                    $btn_all.removeClass("active");
                    $btn_all.find("[name=opids]").prop("checked", false);
                    opidCheck();
                }
            });
        });

        function opidCheck() {
            var limitArray = new Array();
            setTimeout(function () {
                $("[name=opids]:checked").each(function () {
                    limitArray.push($(this).val());
                });
                $("[name=opIds]").val(limitArray.toString());

                console.log($("[name=opIds]").val());
            }, 200);
        }
    });
    
    $("#btn_update").click(checkRequestForm);
    function checkRequestForm() {
        if (confirm('@lang("message.proceedConfirm")')) {
        } else return false;
    }
</script>
@endsection