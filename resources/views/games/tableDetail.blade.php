@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gamemanagement')</a></li>
        <li><a href="javascript:;">
                @lang('message.tableDetails')
            </a>
        </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.tableDetails')
        <small>@lang('message.basicInfo')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <div class="vertical-box-column width-250">
            @include('layouts._tableDetail_leftMenu')
        </div>
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-GAME">TABLE</span>
                    {{$detail->tb_name}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.machineId') : {{$detail->machine_id}}
                                <span class="label label-inverse"></span>
                            </span>

                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.bettime') :
                                {{$detail->g_bettime}}
                            </span>

                        </div>
                    </li>

                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.resttime') :
                                 {{$detail->g_resttime}}
                            </span>
                        </div>
                    </li>
                </ul>
                <div class="table-responsive">
                    <form action="/games/table/detail/update" method="POST" data-parsley-validate="true"
                          name="form-wizard">
                        {!! Form::hidden('tb_idx', $detail->tb_idx) !!}
                        {{ csrf_field() }}
                        <table class="table table-profile">
                            <tbody>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.tableName')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('tb_name', $detail->tb_name, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'6',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-remote' => '/check/game/table/'.$detail->tb_name,
                                        'data-parsley-remote-validator' => 'reverse',
                                        'data-parsley-pattern' => '/[^가-힣ㄱ-ㅎㅏ-ㅣ]/',
                                        'data-parsley-pattern-message' => '사용 할 수 없는 문자입니다.',
                                        'data-parsley-remote-message' => '사용중인 테이블명 입니다.',
                                        'data-parsley-minlength-message' => '테이블명은 6자 이상입니다.',
                                        'data-parsley-maxlength-message' => '테이블명은 45자 이내입니다.',
                                        'data-parsley-required-message' => '필수항목입니다.',
                                        'data-parsley-maxlength' => '45', 'required' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.tableCurrency')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('tb_currency', ['KRW'=>'KRW',
                                        'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'], isset($detail->tb_currency) ?
                                        $detail->tb_currency : '', [ 'class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'required']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.tableStatus')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('tb_status',
                                        ['ACTIVATE'=>trans("message.activate"), 'DEACTIVATE'=>trans("message.deactivate")],
                                        isset($detail->tb_status)?$detail->tb_status:'',
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1', 'required']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.gameList')</td>
                                <td>
                                    <div class="input-group">
                                        <div class="form-group">
                                            <select id="g_idx" name="g_idx" class="form-control"
                                                    data-parsley-group="wizard-step-2" required>
                                                @foreach($gameList as $list)
                                                <option value="{{$list->g_idx}}">{{$list->g_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.dealerList')</td>
                                <td>
                                    <div class="input-group">
                                        <div class="form-group">
                                            <select id="d_idx" name="d_idx" class="form-control"
                                                    data-parsley-group="wizard-step-2" required>
                                                @foreach($dealerList as $list)
                                                <option value="{{$list->d_idx}}">{{$list->d_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @lang('message.modifyInformation')
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
<script>
    $(document).ready(function () {
        $("#g_idx").val({{$detail->g_idx}}).attr("selected", 'selected');
        $("#d_idx").val({{$detail->d_idx}}).attr("selected", 'selected');
    });
    
    $("#btn_update").click(checkRequestForm);
    function checkRequestForm() {
        if (confirm('@lang("message.proceedConfirm")')) {
        } else return false;
    }
</script>
@endsection