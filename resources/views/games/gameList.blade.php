@extends('layouts.app')
@section('content')
<div id="content" class="content">

    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.gamemanagement')</a></li>
        <li><a>@lang('message.gameStatus')</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">
        @lang('message.gameStatus')
        <small></small>
    </h1>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                           data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>-->
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">
                        @lang('message.gamelist')
                    </h4>
                </div>

                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th style="text-align: center;">@lang('message.id')</th>
                            <th style="text-align: center;">@lang('message.gameTitle')</th>
                            <th style="text-align: center;">@lang('message.gameShoe')</th>
                            <th style="text-align: center;">@lang('message.gameCount')</th>
                            <th style="text-align: center;">@lang('message.gameStatus')</th>
                            <th style="text-align: center;">@lang('message.machineStatus')</th>
                            <th style="text-align: center;">@lang('message.tableStatus')</th>
<!--                            <th style="text-align: center;">@lang('message.dealerName')</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($gameList as $list)
                        <tr class="odd gradeX">
                            <td style="text-align: center;"><a href="/games/status/detail/index/{{$list->g_idx}}">{{$list->g_id}}</a></td>
                            <td style="text-align: center;">{{$list->g_title}}</td>
                            <td style="text-align: center;">{{$list->g_shoe}}</td>
                            <td style="text-align: center;">{{$list->g_count}}</td>
                            <td style="text-align: center;">@lang("message.$list->g_status")</td>
                            <td style="text-align: center;">@lang("message.$list->g_org_status")</td>
                            <td style="text-align: center;">@lang("message.$list->tb_status")</td>
<!--                            <td style="text-align: center;">{{$list->d_name}}</td>-->
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->

</div>
@endsection