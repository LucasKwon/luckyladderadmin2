@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li><a href="javascript:;">@lang('message.memberDetails')</a></li>
        <li>
                @if(Auth::user()->op_level == 1 )
                @lang('message.adminlist')
                @elseif(Auth::user()->op_level == 2)
                @lang('message.operatorlist')
                @else
                @lang('message.playerlist')
                @endif
        </li>

        <!--        <li class="active">@lang('message.playerList')</li>-->
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.memberDetails')
        <small>@lang('message.playerList')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column width-250">
            <!-- begin wrapper -->

            <!-- end wrapper -->
            <!-- begin wrapper -->
            @include('layouts._memberDetail_leftMenu')
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">

            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-{{preg_replace('/\s+/', '', $detail->role_name)}}">@lang('message.'.$detail->role_name)</span>
                    {{$detail->op_id}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.point') : {{number_format($detail->op_point,0)}}
                                <span class="label label-inverse">{{$detail->op_currency}}</span>
                            </span>
                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.contact') :
                                <i class="fa fa-mobile fa-lg m-r-5"></i>
                                {{$detail->op_mobile}}
                            </span>
                        </div>
                    </li>

                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.registedDate') :
                                 {{$detail->signup_date}}
                            </span>
                        </div>
                    </li>
                </ul>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-10 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse" style="border-radius: 0px;">
                            <div class="panel-body">
                                <table id="data-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">@lang('message.id')</th>
                                        <th style="text-align: center;">@lang('message.name')</th>
                                        <th style="text-align: center;">@lang('message.balance')</th>
                                        <th style="text-align: center;">@lang('message.status')</th>
                                        <th style="text-align: center;">@lang('message.signUpDate')</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if(Auth::user()->op_level == 1)
                                    @foreach($playerList as $list)
                                    <tr class="odd gradeX">
                                        <td><a href="#">{{$list->op_id}}</a></td>
                                        <td>{{$list->op_name}}</td>
                                        <td style="text-align:right;">{{number_format($list->op_point,0)}}
                                            <span class="label label-default">{{$list->op_currency}}</span>
                                        </td>
                                        <td style="text-align:right;">
                                            @if($list->op_status == 'ACTIVATE')
                                            @lang('message.activate')
                                            @else
                                            @lang('message.deactivate')
                                            @endif
                                        </td>
                                        <td style="text-align: center;">{{$list->signup_date}}</td>
                                    </tr>
                                    @endforeach
                                    @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 2)
                                    @foreach($playerList as $list)
                                    <tr class="odd gradeX">
                                        <td><a href="/members/list/detail/basic_info/g/{{$list->op_idx}}/1">{{$list->op_id}}</a></td>
                                        <td>{{$list->op_name}}</td>
                                        <td style="text-align:right;">{{number_format($list->op_point,0)}}
                                            <span class="label label-default">{{$list->op_currency}}</span>
                                        </td>
                                        <td style="text-align:right;">
                                            @if($list->op_status == 'ACTIVATE')
                                            @lang('message.activate')
                                            @else
                                            @lang('message.deactivate')
                                            @endif
                                        </td>
                                        <td style="text-align: center;">{{$list->signup_date}}</td>
                                    </tr>
                                    @endforeach
                                    @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 3)
                                    @foreach($playerList as $list)
                                    <tr class="odd gradeX">
                                        <td>{{$list->mb_id}}</td>
                                        <td>{{$list->mb_name}}</td>
                                        <td style="text-align:right;">{{number_format($list->mb_point,0)}}
                                            <span class="label label-default">{{$list->mb_currency}}</span>
                                        </td>
                                        <td style="text-align:right;">
                                            @if($list->mb_status == 'ACTIVATE')
                                            @lang('message.activate')
                                            @else
                                            @lang('message.deactivate')
                                            @endif
                                        </td>
                                        <td style="text-align: center;">{{$list->mb_created_date}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    @foreach($playerList as $list)
                                    <tr class="odd gradeX">
                                        <td><a href="#">{{$list->mb_id}}</a></td>
                                        <td>{{$list->mb_name}}</td>
                                        <td style="text-align:right;">{{number_format($list->mb_point,0)}}
                                            <span class="label label-default">{{$list->mb_currency}}</span>
                                        </td>
                                        <td style="text-align:right;">
                                            @if($list->mb_status == 'ACTIVATE')
                                            @lang('message.activate')
                                            @else
                                            @lang('message.deactivate')
                                            @endif
                                        </td>
                                        <td style="text-align: center;">{{$list->mb_created_date}}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-10 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
@endsection