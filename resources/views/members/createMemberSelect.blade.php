@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.members')</a></li>
        <li><a href="javascript:;">@lang('message.createDownLine') </a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.createDownLine') <small></small></h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="vertical-box">
        <div class="vertical-box-column">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter">
                <!-- begin btn-toolbar -->

                <!-- end btn-toolbar -->
            </div>
            <!-- end wrapper -->
            <!-- begin list-email -->
            <ul class="list-group list-group-lg no-radius list-email">

                @if(Auth::user()->op_level < 3)

                <li class="list-group-item danger">

                    <div class="email-info">
                        <h5 class="email-title">
                            <span class="label label-danger" style="padding:8px; font-size: 12px;">@if(Auth::user()->op_level == 1 ) @lang('menu.createAdmin')@elseif(Auth::user()->op_level == 2) @lang('menu.OPERATORS') @lang('message.level')@endif</span>
                            <a href="/members/create"><button type="button" name="btn_request" class="btn btn-info btn-sm m-r-5 m-b-5">@lang('message.create')</button></a>

                        </h5>
                        <p class="email-desc">

                        </p>
                    </div>
                </li>

                @endif

                @if(Auth::user()->op_level < 4)
                <li class="list-group-item success">

                    <div class="email-info">
                        <h5 class="email-title">
                            <span class="label label-success " style="padding:8px; font-size: 12px;">@lang('message.superMasterAgentLevelName') @lang('message.level')</span>
                            <a href="/members/create/site/{{Auth::user()->op_idx}}">
                                <button type="button" name="btn_request" class="btn btn-info btn-sm m-r-5 m-b-5">@lang('message.create')</button>
                            </a>

                        </h5>
                        <p class="email-desc">

                        </p>
                    </div>
                </li>
                @endif

                @if(Auth::user()->op_level >= 3)

                <li class="list-group-item warning">

                    <div class="email-info">
                        <h5 class="email-title">
                            <span class="label label-warning" style="padding:8px; font-size: 12px;">@lang('message.PLAYER') @lang('message.level')</span>
                            <a href="/members/create/player"><button type="button" name="btn_request" class="btn btn-info btn-sm m-r-5 m-b-5">@lang('message.create')</button></a>

                        </h5>
                        <p class="email-desc">
<!--                            @lang('message.createMemberInfo3')-->
                        </p>
                    </div>
                </li>
                @endif

            </ul>
            <!-- end list-email -->
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">

            </div>
            <!-- end wrapper -->
        </div>
    </div>
    <!-- end row -->
</div>
@endsection