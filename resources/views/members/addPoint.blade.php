@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li><a href="javascript:;">@lang('message.memberDetails')</a></li>
        <li>@lang('message.addPoint')</li>
<!--        <li>@lang('message.memberDetails')</a></li>-->
        <!--        <li class="active">@lang('message.createPlayer')</li>-->
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.memberDetails')
        <small>@lang('message.addPoint')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column width-250">
            <!-- begin wrapper -->

            <!-- end wrapper -->
            <!-- begin wrapper -->
            @include('layouts._memberDetail_leftMenu')
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">

            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-{{preg_replace('/\s+/', '', $detail->role_name)}}">@lang('message.'.$detail->role_name)</span>
                    @if(Auth::user()->op_level < 3 || $detail->role_name !='PLAYER' )
                    {{$detail->op_id}}
                    @else
                    {{$detail->mb_id}}
                    @endif
                    <small></small>
                </h4>
                @if(Auth::user()->op_level < 3 || $detail->role_name !='PLAYER' )
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.point') : {{number_format($detail->op_point,0)}}
                                <span class="label label-inverse">{{$detail->op_currency}}</span>
                            </span>
                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.contact') :
                                <i class="fa fa-mobile fa-lg m-r-5"></i>
                                {{$detail->op_mobile}}
                            </span>
                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.registedDate') :
                                 {{$detail->signup_date}}
                            </span>
                        </div>
                    </li>
                </ul>
                @else
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.point') : {{number_format($detail->mb_point,0)}}
                                <span class="label label-inverse">{{$detail->mb_currency}}</span>
                            </span>
                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.registedDate') :
                                 {{$detail->mb_created_date}}
                            </span>
                        </div>
                    </li>
                </ul>
                @endif

                @if( Auth::user()->op_level < 3 || $detail->role_name !='PLAYER' )
                {{--*/ $url = '/members/list/detail/point/p/add' /*--}}
                @else
                {{--*/ $url = '/members/list/detail/point/p/add/player' /*--}}
                @endif

                <form action="{{$url}}" method="POST" data-parsley-validate="true" name="form-wizard">
                    @if(Auth::user()->op_level < 3 || $detail->role_name !='PLAYER' )
                    {!! Form::hidden('targetIdx', $detail->op_idx) !!}
                    @else
                    {!! Form::hidden('targetIdx', $detail->mb_idx) !!}
                    @endif
                    {!! Form::hidden('loginIdx', Auth::user()->op_idx) !!}
                    {!! Form::hidden('loginCashType', Auth::user()->op_cash_type) !!}
                    {!! Form::hidden('process', 'add') !!}
                    {!! Form::hidden('rolName', $detail->role_name) !!}

                    {{ csrf_field() }}
                    <div class="table-responsive  " id="collapseOne2">
                        <table class="table table-profile">
                            <thead>

                            </thead>
                            <tbody>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">
                                    @lang('message.amount')
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::text('op_point', '', ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1',
                                        'data-parsley-pattern' => '/^[0-9,\,]*$/',
                                        'data-parsley-maxlength' => '13',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-pattern-message' => '숫자만 입력하세요.',
                                        'data-parsley-required-message' => '필수 입력항목입니다.',
                                        'data-parsley-maxlength-message' => '입력금액이 너무 많습니다.',
                                        'required'
                                        ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.memo')</td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::text('op_memo','', ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1',
                                        'data-parsley-maxlength' => '50',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-minlength-message' => '메모는 50자 이내로 작성하세요.'
                                        ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @lang('message.addPoint')
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
                <!-- end row -->
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
@endsection