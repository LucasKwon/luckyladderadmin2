@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li><a href="javascript:;">
                @if(Auth::user()->op_level == 1 )
                @lang('message.adminlist')
                @elseif(Auth::user()->op_level == 2)
                @lang('message.operatorlist')
                @endif

            </a>
        </li>
        <li>@lang('message.memberDetails')</a></li>
        <!--        <li class="active">@lang('message.basicInfo')</li>-->
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.memberDetails')
        <small>@lang('message.basicInfo')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <div class="vertical-box-column width-250">
            @include('layouts._memberDetail_leftMenu')
        </div>
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-{{preg_replace('/\s+/', '', $detail->role_name)}}">@lang('message.'.$detail->role_name)</span>
                    {{$detail->mb_id}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.point') : {{number_format($detail->mb_point,0)}}
                                <span class="label label-inverse">{{$detail->mb_currency}}</span>
                            </span>

                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.registedDate') :
                                 {{$detail->mb_created_date}}
                            </span>
                        </div>
                    </li>
                </ul>
                <div class="table-responsive">
                    <form action="/members/list/detail/basic_info/p/player/update" method="POST"
                          data-parsley-validate="true" name="form-wizard">
                        {!! Form::hidden('mb_idx', $detail->op_idx) !!}
                        {!! Form::hidden('mb_id', $detail->mb_id) !!}
                        {{ csrf_field() }}
                        <table class="table table-profile">
                            <tbody>
<!--                            @if(Auth::user()->op_level < 3 && Auth::user()->op_level < $detail->op_level)-->
<!--                            {{--*/ $classValue = 'required' /*--}}-->
<!--                            @else-->
<!--                            {{--*/ $classValue = 'disabled' /*--}}-->
<!--                            @endif-->

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.name')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('mb_name', $detail->mb_name, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-2',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-minlength' => '3', 'data-parsley-maxlength' => '16',
                                        'data-parsley-maxlength-message' => '3~16자 이내로 입력하세요.',
                                        'data-parsley-required-message' => '필수 입력항목입니다.',
                                        'required' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">@lang('message.password')</td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::password('password', ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'maxlength' => '12',
                                        'data-parsley-maxlength' => '12', 'data-parsley-minlength' => '6',
                                        'placeholder'=>'주의)입력하면 비밀번호가 변경됩니다.',
                                        'data-parsley-minlength-message' => '비밀번호는 6 ~ 12자 입니다.',
                                        'data-parsley-maxlength-message' => '비밀번호는 6 ~ 12자 입니다.',
                                        ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.currencyType')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('mb_currency', ['KRW'=>'KRW',
                                        'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'],
                                        isset($detail->mb_currency)?$detail->mb_currency:'',
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.activate-deactivate')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('op_status', ['ACTIVATE'=>trans('message.activate'),
                                        'DEACTIVATE'=>trans('message.deactivate')], isset($detail->mb_status) ?
                                        $detail->mb_status : '', [ 'class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1']) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @lang('message.modifyInformation')
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
<script>
    $("#btn_update").click(checkRequestForm);
    function checkRequestForm() {
        if (confirm('현재 정보로 저장하시겠습니까?')) {
        } else return false;
    }
</script>
@endsection