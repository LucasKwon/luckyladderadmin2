@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li><a href="javascript:;">@lang('message.superMasterAgent') @lang('message.list')</a> </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.superMasterAgent') @lang('message.list')<small></small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>-->
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">
                        @lang('message.superMasterAgent')

                    </h4>
                </div>
                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered">

                        <thead>
                        <tr>
                            <th style="text-align: center;">@lang('message.id')</th>
                            <th style="text-align: center;">@lang('message.email')</th>
                            <th style="text-align: center;">@lang('message.balance')</th>
                            <th style="text-align: center;">@lang('message.dealSet')</th>
                            <th style="text-align: center;">@lang('message.signUpDate')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($operatorList as $list)
                        <tr class="odd gradeX">
                            <td><a href="/members/list/detail/basic_info/g/{{$list->op_idx}}/1">{{$list->op_id}}</a></td>
                            <td>{{$list->op_email}}</td>
                            <td style="text-align:right;">{{number_format($list->op_point,0)}}
                                <span class="label label-default">{{$list->op_currency}}</span>
                            </td>
                            <td style="text-align:right;">{{number_format($list->op_profit,2)}} %</td>
                            <td style="text-align: center;">{{$list->signup_date}}</td>
                        </tr>
                        @endforeach

                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<script>
    $(document).ready(function () {
//        var lang = "/resource/{!! Session::get('applocale') !!}.json";
//        var table = $('#dataTable').DataTable({
//            dom: 'lBfrtip',
//            buttons: [
//                { extend: 'copy', className: 'btn-sm' },
//                { extend: 'csv', className: 'btn-sm' },
//                { extend: 'excel', className: 'btn-sm' },
//                { extend: 'pdf', className: 'btn-sm' },
//                { extend: 'print', className: 'btn-sm' }
//            ],
//            responsive: true,
//            autoFill: true,
//            colReorder: true,
//            keys: true,
//            rowReorder: true,
//            select: true,
//            order: [7, 'desc'],
//            language: {url:lang}
//
//        });
    });



</script>
@endsection