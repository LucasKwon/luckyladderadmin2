@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li class="active">@lang('message.create')</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">  @lang('message.createNewDownLine') <small></small>  </h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <h4 class="panel-title">
                        @lang('message.createNewDownLine')
                    </h4>
                </div>
                <div class="panel-body">

                    {{--*/ $classValue = 'required' /*--}}
                    {{--*/ $idUrl = '/check/operators/id/' /*--}}
                    {{--*/ $nameUrl = '/check/operators/name/' /*--}}
                    {{--*/ $emailUrl = '/check/operators/email/' /*--}}

                    <form action="/members/create/agent/process" method="POST" data-parsley-validate="true"
                          name="form-wizard">
                        {!! Form::hidden('op_parent_idx', Auth::user()->op_idx) !!}
                        {!! Form::hidden('op_level', Auth::user()->op_level) !!}
                        {!! Form::hidden('op_levelName', Request::segment(3)) !!}
                        @if(Auth::user()->op_level == 3 )
                        {!! Form::hidden('op_parent_prefix', Auth::user()->op_prefix) !!}
                        @endif
                        {{ csrf_field() }}
                        <div id="wizard">
                            <ol>
                                <li>
                                    @lang('message.memberBasicInformation')
                                    <small>전체 시스템에서 사용될 대표적인 이름 및 <br/>게임 사이트 운영을 위한 기본정보 설정.</small>
                                </li>
                                <li>
                                    @lang('message.contactInformation')
                                    <small>전화번호, 이메일 연락처를 설정.</small>
                                </li>
                                <li>
                                    @lang('message.loginInformation')
                                    <small>관리자 사이트에 로그인할 사용자 계정을 설정.</small>
                                </li>
                                <li>
                                    @lang('message.complete')
                                    <small></small>
                                </li>
                            </ol>
                            <!-- begin wizard step-1 -->
                            <div class="wizard-step-1">
                                <legend class="pull-left width-full">@lang('message.memberBasicInformation')</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group block1">
                                            <label> @lang('message.memberName')</label>
                                            {!! Form::text('op_name', '', ['class' => 'form-control',
                                            'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'3',
                                            'data-parsley-trigger' => 'change',
                                            'data-parsley-remote' => $nameUrl,
                                            'data-parsley-remote-validator' => 'reverse',
                                            'data-parsley-remote-message' => '사용중인 사용자명입니다.',
                                            'data-parsley-minlength-message' => '사용자명은 3자 이상입니다.',
                                            'data-parsley-required-message' => '사용자명은 필수항목입니다.',
                                            'data-parsley-maxlength' => '45', 'required' ]) !!}
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.memberCurrency')</label>
                                            @if(Auth::user()->op_level == 1)
                                            {!! Form::select('op_currency', ['MUL' => 'MUL', 'KRW'=>'KRW',
                                            'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'], isset($data['op_currency']) ?
                                            $data['op_currency'] : '', ['class' => 'form-control',
                                            'data-parsley-group' => 'wizard-step-1', 'required']) !!}
                                            @else
                                            {!! Form::select('op_currency', ['KRW'=>'KRW',
                                            'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'], isset($data['op_currency']) ?
                                            $data['op_currency'] : '', [ 'class' => 'form-control',
                                            'data-parsley-group' => 'wizard-step-1', 'required']) !!}
                                            @endif
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.memberCashType')</label>
                                            {!! Form::select('op_cash_type',
                                            [ 'PRE'=>trans('message.PREPAID'), 'POST'=>trans('message.POSTPAID') ],
                                            isset($data['op_cash_type']) ? $data['op_cash_type'] : '', ['class' =>
                                            'form-control', 'data-parsley-group' => 'wizard-step-1', 'required', $classValue]) !!}
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="checkbox" name="accounting_betwin" value="Y" checked
                                                   disabled="disabled">
                                            <label>@lang('message.bet/win')</label>
                                            <div class="input-group">
                                                {!! Form::text('op_profit', '', [ 'class' => 'form-control',
                                                'data-parsley-group' => 'wizard-step-1',
                                                'data-parsley-required' => 'true',
                                                'data-parsley-trigger' => 'input',
                                                'data-parsley-pattern' => '/^[0-9,.]*$/',
                                                'data-parsley-range' => '[0,100]',
                                                'data-parsley-error-message' => '숫자 0~100사이를 입력하세요.'
                                                ]) !!}
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="checkbox" name="accounting_rolling" value="Y">
                                            <label>@lang('message.rolling')</label>
                                            <div class="input-group">
                                                {!! Form::text('op_rolling', '', [ 'class' => 'form-control',
                                                'data-parsley-group' => 'wizard-step-1',
                                                'data-parsley-trigger' => 'input',
                                                'data-parsley-pattern' => '/^[0-9,.]*$/',
                                                'data-parsley-range' => '[0,100]',
                                                'data-parsley-error-message' => '숫자 0~100사이를 입력하세요.',
                                                'disabled' => 'disabled' ]) !!}
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- end wizard step-1 -->
                            <!-- begin wizard step-2 -->
                            <div class="wizard-step-2">
                                <legend class="pull-left width-full">@lang('message.contactInformation')</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-6 -->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.phoneNumber')</label>
                                            {!! Form::text('op_tel', '', ['id'=>'phone', 'class' => 'form-control',
                                            'data-parsley-group' => 'wizard-step-2',
                                            'data-parsley-maxlength'=>'13',
                                            'data-parsley-minlength'=>'11',
                                            'placeholder'=>'ex) 012-345-6789, 012-3456-7890',
                                            'data-parsley-trigger' => 'input' ,
                                            'data-parsley-pattern' => '/^[0-9,-]*$/',
                                            'data-parsley-pattern-message' => '전화번호 형식에 맞게 입력하세요.',
                                            'data-parsley-minlength-message' => '전화번호 형식에 맞게 입력하세요.',
                                            'data-parsley-maxlength-message' => '전화번호 형식에 맞게 입력하세요.',
                                            'data-parsley-required-message' => '전화번호는 필수항목입니다.',
                                            'required' => 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.mobileNumber')</label>
                                            {!! Form::text('op_mobile', '', [ 'class' => 'form-control',
                                            'data-parsley-group' => 'wizard-step-2',
                                            'data-parsley-maxlength'=>'13',
                                            'data-parsley-minlength'=>'11',
                                            'placeholder'=>'ex) 012-3456-7890',
                                            'data-parsley-trigger' => 'input' ,
                                            'data-parsley-pattern-message' => '휴대전화번호 형식에 맞게 입력하세요.',
                                            'data-parsley-minlength-message' => '휴대전화번호 형식에 맞게 입력하세요.',
                                            'data-parsley-maxlength-message' => '휴대전화번호 형식에 맞게 입력하세요.',
                                            'data-parsley-pattern' => '/^[0-9,-]*$/',
                                            ]) !!}
                                        </div>
                                    </div>

                                    <!-- end col-6 -->
                                    <!-- begin col-6 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.emailAddress')</label>
                                            {!! Form::email('op_email', '', ['class' => 'form-control',
                                            'data-parsley-group' => 'wizard-step-2', 'data-parsley-type' => 'email',
                                            'data-parsley-trigger' => 'change',
                                            'data-parsley-required-message' => '이메일은 필수항목입니다.',
                                            'data-parsley-type-message' => '이메일 형식에 맞게 입력하세요.'
                                             ]) !!}
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end wizard step-2 -->
                            <!-- begin wizard step-3 -->
                            <div class="wizard-step-3">
                                <legend class="pull-left width-full">@lang('message.login')</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.operatorLoginAccount')</label>
                                            <div class="controls">
                                                {!! Form::text('op_id', '', ['class' => 'form-control',
                                                'data-parsley-trigger' => 'change',
                                                'data-parsley-remote' => $idUrl,
                                                'data-parsley-remote-validator' => 'reverse',
                                                'data-parsley-remote-message' => '사용중인 아이디입니다.',
                                                'data-parsley-minlength-message' => '계정은 5자 이상입니다.',
                                                'data-parsley-required-message' => '아이디는 필수 입력항목입니다.',
                                                'data-parsley-group' => 'wizard-step-3',
                                                'data-parsley-minlength' => '5',
                                                'data-parsley-maxlength' => '16',
                                                'data-parsley-pattern' => '/^[0-9,a-z,A-Z]*$/',
                                                'required' ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.password')</label>
                                            <div class="controls">
                                                {!! Form::password('password', ['class' => 'form-control',
                                                'data-parsley-group' => 'wizard-step-3', 'maxlength' => '12',
                                                'data-parsley-required-message' => '패스워드를 입력하세요.',
                                                'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>@lang('message.prefix')</label>
                                            <div class="controls">
                                                {!! Form::text('op_prefix', '', ['class' => 'form-control',
                                                'data-parsley-trigger' => 'change',
                                                'data-parsley-remote' => '/check/operators/prefix/',
                                                'data-parsley-remote-validator' => 'reverse',
                                                'data-parsley-remote-message' => '사용중인 Prefix 입니다.',
                                                'data-parsley-minlength-message' => 'Prefix 는 3자 이상입니다.',
                                                'data-parsley-required-message' => '필수 입력항목입니다.',
                                                'data-parsley-group' => 'wizard-step-3',
                                                'data-parsley-minlength' => '3',
                                                'data-parsley-maxlength' => '10',
                                                'data-parsley-pattern' => '/^[0-9,a-z,A-Z]*$/',
                                                'required' ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- begin col-4 -->
                                    <!-- end col-6 -->
                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end wizard step-3 -->
                            <!-- begin wizard step-4 -->
                            <div>
                                <div class="jumbotron m-b-0 text-center">
                                    <h1>@lang('message.confirmCreateMember')</h1>
                                    <button type="submit" class="btn btn-success btn-lg">@lang('message.confirm')
                                    </button>
                                </div>
                            </div>
                            <!-- end wizard step-4 -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<script>
    $("#phone").keyup(function (event) {

        // event.target.value = autoHypenPhone(event.target.value);

        // console.log(event.keyCode, event.target.value);

    });

//
//    $("[name=op_cash_type]").change(function (event) {
//        if (event.target.value === 'POST') {
//            $("[name=op_point]").attr("disabled", "disabled");
//            $("[name=op_point]").val('');
//        } else {
//            $("[name=op_point]").attr("disabled", false);
//            $("[name=op_point]").val('0');
//        }
//    });

    $("[name=accounting_betwin]").change(function () {
        if ($("input:checkbox[name='accounting_betwin']").is(":checked") === true) {
            $("input:input[name='op_profit']").attr("data-parsley-required", true);
        }
        if ($('input:checkbox[name="accounting_betwin"]').is(":checked") === false) {
            $('input:input[name="op_profit"]').attr("data-parsley-required", false);
        }
    });
    $("[name=accounting_rolling]").change(function () {
        if ($('input:checkbox[name="accounting_rolling"]').is(":checked") === true) {
            $('input:input[name="op_rolling"]').attr("disabled", false);
            $('input:input[name="op_rolling"]').attr("data-parsley-required", true);
        }
        if ($('input:checkbox[name="accounting_rolling"]').is(":checked") === false) {
            $('input:input[name="op_rolling"]').val("");
            $('input:input[name="op_rolling"]').attr("disabled", "disabled");
            $('input:input[name="op_rolling"]').attr("data-parsley-required", false);
        }
    });

</script>
@endsection