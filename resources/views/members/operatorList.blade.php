@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li>
                @if(Auth::user()->op_level == 1 )
                @lang('message.adminlist')
                @elseif(Auth::user()->op_level == 2)
                @lang('message.operatorlist')
                @else
                @lang('message.playerlist')
                @endif

            </a>
        </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">
        @if(Auth::user()->op_level == 1 )
        @lang('message.adminlist')
        @elseif(Auth::user()->op_level == 2)
        @lang('message.operatorlist')
        @else
        @lang('message.playerlist')
        @endif
        <small></small>
    </h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>-->
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">
                        @if(Auth::user()->op_level == 1 )
                        @lang('message.admin')
                        @elseif(Auth::user()->op_level == 2)
                        @lang('message.operator')
                        @else
                        @lang('message.player')
                        @endif
                    </h4>
                </div>
                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered">
                        @if(Auth::user()->op_level < 3)
                        <thead>
                        <tr>
                            <th style="text-align: center;">@lang('message.id')</th>
                            <th style="text-align: center;">@lang('message.email')</th>
                            <th style="text-align: center;">@lang('message.balance')</th>
                            <th style="text-align: center;">@lang('message.dealSet')</th>
                            <th style="text-align: center;">@lang('message.signUpDate')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($operatorList as $list)
                        <tr class="odd gradeX">
                            <td><a href="/members/list/detail/basic_info/g/{{$list->op_idx}}/1">{{$list->op_id}}</a></td>
                            <td>{{$list->op_email}}</td>
                            <td style="text-align:right;">{{number_format($list->op_point,0)}}
                                <span class="label label-default">{{$list->op_currency}}</span>
                            </td>
                            <td style="text-align:right;">{{number_format($list->op_profit,2)}} %</td>
                            <td style="text-align: center;">{{$list->signup_date}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                        @else
                        <thead>
                        <tr>
                            <th style="text-align: center;">@lang('message.id')</th>
                            <th style="text-align: center;">@lang('message.name')</th>
                            <th style="text-align: center;">@lang('message.status')</th>
                            <th style="text-align: center;">@lang('message.balance')</th>
                            <th style="text-align: center;">@lang('message.signUpDate')</th>
                            <th style="text-align: center;">@lang('message.desc')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($playerList as $list)
                        <tr class="odd gradeX">
                            <td><a href="/members/list/detail/basic_info/g/player/{{$list->op_idx}}/{{$list->mb_idx}}/1">{{$list->mb_id}}</a></td>
                            <td>{{$list->mb_name}}</td>
                            <td>
                                @if($list->mb_status == 'ACTIVATE')
                                @lang('message.activate')
                                @else
                                @lang('message.deactivate')
                                @endif
                            </td>
                            <td style="text-align:right;">{{number_format($list->mb_point,0)}}
                                <span class="label label-default">{{$list->mb_currency}}</span>
                            </td>
                            <td style="text-align: center;">{{$list->mb_created_date}}</td>
                            <td style="text-align: center;">{{$list->mb_desc}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<script>
    $(document).ready(function () {
//        var lang = "/resource/{!! Session::get('applocale') !!}.json";
//        var table = $('#dataTable').DataTable({
//            dom: 'lBfrtip',
//            buttons: [
//                { extend: 'copy', className: 'btn-sm' },
//                { extend: 'csv', className: 'btn-sm' },
//                { extend: 'excel', className: 'btn-sm' },
//                { extend: 'pdf', className: 'btn-sm' },
//                { extend: 'print', className: 'btn-sm' }
//            ],
//            responsive: true,
//            autoFill: true,
//            colReorder: true,
//            keys: true,
//            rowReorder: true,
//            select: true,
//            order: [7, 'desc'],
//            language: {url:lang}
//
//        });
    });



</script>
@endsection