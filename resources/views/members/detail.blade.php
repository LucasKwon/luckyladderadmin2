@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li><a href="javascript:;">
                @if(Auth::user()->op_level == 1 )
                @lang('message.adminlist')
                @elseif(Auth::user()->op_level == 2)
                @lang('message.operatorlist')
                @endif

            </a>
        </li>
        <li>@lang('message.memberDetails')</a></li>
        <!--        <li class="active">@lang('message.basicInfo')</li>-->
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.memberDetails')
        <small>@lang('message.basicInfo')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <div class="vertical-box-column width-250">
            @include('layouts._memberDetail_leftMenu')
        </div>
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-{{preg_replace('/\s+/', '', $detail->role_name)}}">@lang('message.'.$detail->role_name)</span>
                    {{$detail->op_id}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                                    <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                        @lang('message.point') : {{number_format($detail->op_point,0)}}
                                        <span class="label label-inverse">{{$detail->op_currency}}</span>
                                    </span>

                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                                    <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                        @lang('message.contact') :
                                        <i class="fa fa-mobile fa-lg m-r-5"></i>
                                        {{$detail->op_mobile}}
                                    </span>

                        </div>
                    </li>

                    <li class="media media-sm clearfix">
                        <div class="media-body">
                                    <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                        @lang('message.registedDate') :
                                         {{$detail->signup_date}}

                                    </span>

                        </div>
                    </li>
                </ul>
                <div class="table-responsive">
                    <form action="/members/list/detail/basic_info/p/update" method="POST" data-parsley-validate="true"
                          name="form-wizard">
                        {!! Form::hidden('op_idx', $detail->op_idx) !!}
                        {!! Form::hidden('op_id', $detail->op_id) !!}
                        {{ csrf_field() }}
                        <table class="table table-profile">
                            <tbody>
                            @if(Auth::user()->op_level < 3 && Auth::user()->op_level < $detail->op_level)
                            {{--*/ $classValue = 'required' /*--}}
                            @else
                            {{--*/ $classValue = 'disabled' /*--}}
                            @endif
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.name')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('op_name', $detail->op_name, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'data-parsley-minlength'=>'3',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-minlength-message' => '사용자명은 3자 이상입니다.',
                                        'data-parsley-required-message' => '사용자명은 필수항목입니다.',
                                        'data-parsley-maxlength' => '45', 'required' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.phoneNumber')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('op_tel', $detail->op_tel, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-2',
                                        'data-parsley-maxlength'=>'13',
                                        'data-parsley-minlength'=>'11',
                                        'placeholder'=>'ex) 012-345-6789, 012-3456-7890',
                                        'data-parsley-trigger' => 'input' ,
                                        'data-parsley-pattern' => '/^[0-9,-]*$/',
                                        'data-parsley-pattern-message' => '전화번호 형식에 맞게 입력하세요.',
                                        'data-parsley-minlength-message' => '전화번호 형식에 맞게 입력하세요.',
                                        'data-parsley-maxlength-message' => '전화번호 형식에 맞게 입력하세요.',
                                        'data-parsley-required-message' => '전화번호는 필수항목입니다.',
                                        'required' => 'required']) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.mobileNumber')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('op_mobile', $detail->op_mobile, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-2',
                                        'data-parsley-trigger' => 'input', 'data-parsley-pattern' => '/^[0-9,(,),-]*$/',
                                        'data-parsley-minlength'=>'10']) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.email')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::email('op_email', $detail->op_email, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-2', 'data-parsley-type' => 'email',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-required-message' => '이메일은 필수항목입니다.',
                                        'data-parsley-type-message' => '이메일 형식에 맞게 입력하세요.',
                                        'data-parsley-required-message' => '이메일은 필수 입력항목입니다.',
                                        'required' ]) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.currencyType')</td>
                                <td>
                                    <div class="input-group">
                                        @if(Auth::user()->op_level == 1)
                                        {!! Form::select('op_currency', ['MUL' => 'MUL', 'KRW'=>'KRW',
                                        'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'],
                                        isset($detail->op_currency)?$detail->op_currency:'' ,
                                        ['class' => 'form-control', 'data-parsley-group' => 'wizard-step-1',
                                        $classValue]) !!}
                                        @else
                                        {!! Form::select('op_currency', ['KRW'=>'KRW',
                                        'USD'=>'USD','CNY'=>'CNY','SGD'=>'SGD'],
                                        isset($detail->op_currency)?$detail->op_currency:'',
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1',
                                        $classValue]) !!}
                                        @endif
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.cashType')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('op_cash_type', ['POST'=>trans('message.POSTPAID'),
                                        'PRE'=>trans('message.PREPAID')], isset($detail->op_cash_type) ?
                                        $detail->op_cash_type : '', [ 'class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', $classValue]) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.memberDeal')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::text('op_profit', number_format($detail->op_profit, 2),
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1',
                                        'data-parsley-required' =>'true','data-parsley-trigger' => 'input',
                                        'data-parsley-pattern' => '/^[0-9,.]*$/', $classValue]) !!}
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.memberRoll')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::text('op_rolling', number_format($detail->op_rolling, 2),
                                        [ 'class' => 'form-control', 'data-parsley-group' => 'wizard-step-1',
                                        'data-parsley-required' =>'true','data-parsley-trigger' => 'input',
                                        'data-parsley-pattern' => '/^[0-9,.]*$/', $classValue]) !!}
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.apiKey')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('op_api_key', $detail->op_api_key, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-2', 'data-parsley-trigger' => 'input',
                                        'disabled' ]) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.prefix')</td>
                                <td>
                                    <div class="input-inline">
                                        {!! Form::text('op_prefix', $detail->op_prefix, ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-2',
                                        'data-parsley-trigger' => 'change',
                                        'maxlength'=>'10', 'required',
                                        'data-parsley-required-message' => '필수항목입니다.' ]) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr class="highlight">
                                <td class="field" style="padding-top: 10px;">@lang('message.activate-deactivate')</td>
                                <td>
                                    <div class="input-group">
                                        {!! Form::select('op_status', ['ACTIVATE'=>trans('message.activate'),
                                        'DEACTIVATE'=>trans('message.deactivate')], isset($detail->op_status) ?
                                        $detail->op_status : '', [ 'class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', $classValue]) !!}
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @lang('message.modifyInformation')
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
<script>
    $("#btn_update").click(checkRequestForm);
    function checkRequestForm() {
        if (confirm('@lang("message.proceedConfirm")')) {
        } else return false;
    }
</script>
@endsection