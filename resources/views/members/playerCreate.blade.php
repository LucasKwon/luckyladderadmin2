@extends('layouts.app')
@section('content')
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.management')</a></li>
        <li><a href="javascript:;">@lang('message.memberDetails')</a></li>
        <li>
                @if(Auth::user()->op_level == 1 )
                @lang('message.adminlist')
                @elseif(Auth::user()->op_level == 2)
                @lang('message.operatorlist')
                @else
                @lang('message.createPlayer')
                @endif
        </li>

        <!--        <li class="active">@lang('message.createPlayer')</li>-->
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.memberDetails')
        <small>@lang('message.createPlayer')</small>
    </h1>
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column width-250">
            <!-- begin wrapper -->

            <!-- end wrapper -->
            <!-- begin wrapper -->
            @include('layouts._memberDetail_leftMenu')
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column bg-grey">
            <!-- begin wrapper -->
            <div class="wrapper bg-silver-lighter clearfix">

            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <h4 class="m-b-15 m-t-0 p-b-10 underline">
                    <span class="label label-{{preg_replace('/\s+/', '', $detail->role_name)}}">@lang('message.'.$detail->role_name)</span>
                    {{$detail->op_id}}
                    <small></small>
                </h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.point') : {{number_format($detail->op_point,0)}}
                                <span class="label label-inverse">{{$detail->op_currency}}</span>
                            </span>
                        </div>
                    </li>
                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.contact') :
                                <i class="fa fa-mobile fa-lg m-r-5"></i>
                                {{$detail->op_mobile}}
                            </span>
                        </div>
                    </li>

                    <li class="media media-sm clearfix">
                        <div class="media-body">
                            <span class="email-from text-inverse f-w-600" style="line-height: 18px;">
                                @lang('message.registedDate') :
                                 {{$detail->signup_date}}
                            </span>
                        </div>
                    </li>
                </ul>

                <form action="/members/list/detail/user_create/p/process"
                      method="POST" data-parsley-validate="true" name="form-wizard">
                    {!! Form::hidden('op_idx', $detail->op_idx) !!}
                    {!! Form::hidden('mb_currency', $detail->op_currency )!!}
                    {!! Form::hidden('user_level', Auth::user()->op_level )!!}
                    {!! Form::hidden('admin_level', $detail->admin_level )!!}
                    {{ csrf_field() }}
                    <div class="table-responsive  " id="collapseOne2">

                        <table class="table table-profile">
                            <thead>

                            </thead>
                            <tbody>

                            <tr class="highlight">
                                <td class="field">
                                    @if(Auth::user()->op_level == 1 && $detail->admin_level == 1)
                                    @lang('message.adminId')
                                    @elseif(Auth::user()->op_level == 1 && $detail->admin_level == 2)
                                    @lang('message.operatorId')
                                    @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 2)
                                    @lang('message.operatorId')
                                    @else
                                    @lang('message.playerId')
                                    @endif
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::text('user_id', '', ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'required' => 'required',
                                        'data-parsley-pattern' => '/^[0-9,a-z,A-Z]*$/',
                                        'data-parsley-minlength' => '4', 'data-parsley-maxlength' => '12',
                                        'data-parsley-trigger' => 'change',
                                        'data-parsley-remote' => '/check/members/id/'.Auth::user()->op_level.'/'.$detail->op_idx,
                                        'data-parsley-remote-validator' => 'reverse',
                                        'data-parsley-remote-message' => '사용중인 아이디입니다.',
                                        'data-parsley-required-message' => '아이디는 필수항목입니다.',
                                        'data-parsley-pattern-message' => '아이디형식이 틀립니다.',
                                        'data-parsley-minlength-message' => '아이디는 5 ~ 12자 입니다.',
                                        'data-parsley-maxlength-message' => '아이디는 5 ~ 12자 입니다.'
                                        ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">@lang('message.password')</td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::password('password', ['class' => 'form-control',
                                        'data-parsley-group' => 'wizard-step-1', 'maxlength' => '12',
                                        'data-parsley-maxlength' => '12', 'data-parsley-minlength' => '6',
                                        'data-parsley-required-message' => '비밀번호는 필수항목입니다.',
                                        'data-parsley-minlength-message' => '비밀번호는 6 ~ 12자 입니다.',
                                        'data-parsley-maxlength-message' => '비밀번호는 6 ~ 12자 입니다.',
                                        'required' => 'required']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td>
                                    <button class="btn btn-danger btn-sm" id="btn_update">
                                        @if(Auth::user()->op_level == 1 && $detail->admin_level == 1)
                                        @lang('message.createAdmin')
                                        @elseif(Auth::user()->op_level == 1 && $detail->admin_level == 2)
                                        @lang('message.createOperator')
                                        @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 2)
                                        @lang('message.createOperator')
                                        @else
                                        @lang('message.createPlayer')
                                        @endif
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>

                <!-- end row -->
            </div>
            <!-- end wrapper -->
            <!-- begin wrapper -->
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
</div>
@endsection