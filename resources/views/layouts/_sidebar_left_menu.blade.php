<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="info">
                    <span class="label label-{{preg_replace('/\s+/', '', Auth::user()->role_name)}}">
                        @lang('message.'.Auth::user()->role_name)
                    </span>
                    <a href="/members/list/detail/basic_info/g/{{Auth::user()->op_idx}}/0" style="color:white;padding-left: 8px;">{{Auth::user()->op_name}}</a>
                    <small><i class="fa fa-krw"></i> {{number_format(Auth::user()->op_point, 0)}}</small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="{!! (Request::is('/') ? 'active' : '') !!}">
                <a href="/">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>@lang('menu.DASHBOARD')</span>
                </a>
            </li>

            <!-- MEMBERS -->
            <li class="has-sub {!! (Request::is('members/*') ? 'active' : '') !!}">
                <a href="javascript:">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-users"></i>
                    <span>@lang('menu.MANAGEMENT')</span>
                </a>
                <ul class="sub-menu">
                    <li class="{!! (Request::is('members/list/*') ? 'active' : '') !!}">
                        @if(Auth::user()->op_level < 3)
                        <a href="/members/list/{{Auth::user()->op_idx}}/0">
                            @if(Auth::user()->op_level == 1 )
                            @lang('menu.AdminList')
                            @elseif(Auth::user()->op_level == 2)
                            @lang('menu.OperatorList')
                            @endif
                        </a>
<!--                        else-->
<!--                        <a href="/members/list/player/{{Auth::user()->op_idx}}/0">-->
<!--                            lang('menu.PlayerList')-->
<!--                        </a>-->
                        @endif
                    </li>


                    @if(Auth::user()->op_level < 4)
                    <li class="{!! (Request::is('members/site/list/*') ? 'active' : '') !!}">
                        <a href="/members/site/list/{{Auth::user()->op_idx}}/0">
                                @lang('menu.SITELIST')
                        </a>
                    </li>
                    @endif

                    @if(Auth::user()->op_level < 3)
                    @else
                    <li class="{!! (Request::is('members/list/player/*') ? 'active' : '') !!}">

                        <a href="/members/list/player/{{Auth::user()->op_idx}}/0">
                            @lang('menu.PlayerList')
                        </a>
                    </li>
                    @endif

<!--                    @if(Auth::user()->op_level < 3)-->
<!--                    <li class="{!! (Request::is('members/create') ? 'active' : '') !!}">-->
<!--                        <a href="/members/create">-->
<!--                            @if(Auth::user()->op_level == 1 )-->
<!--                            @lang('menu.createAdmin')-->
<!--                            @elseif(Auth::user()->op_level == 2)-->
<!--                            @lang('menu.createOperator')-->
<!--                            @endif-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    @else-->
<!--                    <li class="{!! (Request::is('members/create/player') ? 'active' : '') !!}">-->
<!--                        <a href="/members/create/player">-->
<!--                            @lang('menu.createPlayer')-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    @endif-->

                    @if(Auth::user()->vendor_level < 4)
                    <li class="{!! (Request::is('members/select') ||  Request::is('members/create/*') || Request::is('members/create')  ? 'active' : '') !!}">
                        <a href="/members/select">
                            @lang('menu.CREATEMEMBER')
                        </a>
                    </li>
                    @endif

                </ul>
            </li>

            <!-- GAME HISTORY -->
            @if(Auth::user()->op_level > 1)
            <li class="{!! (Request::is('history/betwin/*') ? ' active' : '') !!}">
                <a href="/history/betwin/{{Auth::user()->op_level}}/{{Auth::user()->op_idx}}">
                    <i class="fa fa-history"></i>
                    <span>@lang('menu.GAMEHISTORY')</span>
                </a>
            </li>
            @endif

            <!-- ACCOUNTINGS -->
            @if(Auth::user()->op_level > 1)
            <li class="has-sub {!! (Request::is('report/*') ? ' active' : '') !!}">
                <a href="javascript:">
                    <!--                    <span class="badge pull-right">10</span>-->
                    <b class="caret pull-right"></b>
                    <i class="fa fa-money"></i>
                    <span>@lang('menu.REPORTS')</span>
                </a>
                <ul class="sub-menu">
                    <!-- To Upper -->
                    <li class="{!! (Request::is('report/betwin/*') ? ' active' : '') !!}">
                        <a href="/report/betwin/{{Auth::user()->op_idx}}">@lang('menu.BETWIN')</a>
                    </li>
<!--                    <li class="{!! (Request::is('account/requested/*') ? ' active' : '') !!}">-->
<!--                        <a href="/account/requested/{{Auth::user()->op_idx}}">@lang('menu.STATUS')</a>-->
<!--                    </li>-->
                    <!-- To Down -->
                </ul>
            </li>
            @endif

            <!-- GAME SETTINGS -->
            @if(Auth::user()->op_level == 2)
            <li class="has-sub {!! (Request::is('games/*') ? ' active' : '') !!}">
                <a href="javascript:">
                    <!--                    <span class="badge pull-right">10</span>-->
                    <b class="caret pull-right"></b>
                    <i class="fa fa-gamepad"></i>
                    <span>@lang('menu.GAMEMANAGEMENT')</span>
                </a>
                <ul class="sub-menu">
                    <!-- To Upper -->
                    <li class="{!! (Request::is('games/status/*') ? ' active' : '') !!}">
                        <a href="/games/status/list">@lang('menu.gameStatus')</a>
                    </li>
                    <li class="{!! (Request::is('games/betlimits') ? ' active' : '') !!}">
                        <a href="/games/betlimits">@lang('menu.betLimits')</a>
                    </li>
                    <li class="{!! (Request::is('games/table/*') ? ' active' : '') !!}">
                        <a href="/games/table/list">@lang('menu.gameTables')</a>
                    </li>
<!--                    <li class="{!! (Request::is('games/setting/*') ? ' active' : '') !!}">-->
<!--                        <a href="/games/setting/{{Auth::user()->op_idx}}">@lang('menu.gameSetting')</a>-->
<!--                    </li>-->
                    <!-- To Down -->
                </ul>
            </li>
            @endif


            <li class="has-sub {!! (Request::is('apiGuide/*') ? ' active' : '') !!}">
                <a href="javascript:">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-cogs"></i>
                    <span>API</span>
                </a>
                <ul class="sub-menu">
                    <!-- To Upper -->
                    <li class="{!! (Request::is('apiGuide/Doc') ? ' active' : '') !!}">
                        <a href="/apiGuide/Doc">API Document</a>
                    </li>

                    <!-- To Down -->
                </ul>
            </li>


            <li>
                <a href="{{ url('/logout') }}">
                    <i class="fa fa-sign-out"></i>
                    <span>@lang('menu.LOGOUT')</span>
                </a>
            </li>

            <!-- begin sidebar minify button -->
            <li><a href="javascript:" class="sidebar-minify-btn" data-click="sidebar-minify"><i
                            class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>