
<div class="user_info cfx">
    <div class="cfx fr mr20">
        <div class="fl">
            <!-- <span class="operator">OPERATOR</span> -->
            <span class="{{Auth::user()->role_name}}">{{Auth::user()->role_name}}</span>
            <span class="site_address ml10">{{Auth::user()->email}}</span>
            <span class="total ml10"><strong>{{number_format(Auth::user()->vendor_balance,0)}}</strong>{{Auth::user()->vendor_currency}}</span>
        </div>

        <ul class="top_menu">
            <li>
                <div class="slt_lang">
                    <p class="lang_select {{ Config::get('languages')[App::getLocale()] }}"><a href="#none">{{ Config::get('languages')[App::getLocale()] }}</a></p>
                </div>

                <ul class="lang_list">
                    @foreach (Config::get('languages') as $lang => $language)
                    @if ($lang != App::getLocale())
                    <li class="{{$language}}"><a href="{{ route('lang.switch', $lang) }}">{{$language}}</a></li>
                    @endif
                    @endforeach
                </ul>
            </li>
            <li><a href="#none" class="alert_box"><span class="num_alert" id="request_cnt">0</span></a></li>
            <li><a href="{{ url('/logout') }}" class="btn_logout">LOGOUT</a></li>
        </ul>
    </div>
</div>