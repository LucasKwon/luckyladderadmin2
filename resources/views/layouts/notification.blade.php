<div class="modal " id="modal-error">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger m-b-0">
                    @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <h4><i class="fa fa-info-circle"></i> {{ $error }}</h4>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>