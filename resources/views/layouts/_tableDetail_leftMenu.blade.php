<div class="wrapper">
    <p><b>@lang('message.INFORMATION')</b></p>
    <ul class="nav nav-pills nav-stacked nav-sm">
        <li class="{!! (Request::is('games/table/detail/*') ? ' active' : '') !!}">
            <a href="/games/table/detail/index/{{Request::segment(4)}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.basicInfo')
            </a>
        </li>

        <li class="{!! (Request::is('games/table/setting/*') ? ' active' : '') !!}">
            <a href="/games/table/setting/{{Request::segment(4)}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.tableSetting')
            </a>
        </li>

    </ul>
</div>