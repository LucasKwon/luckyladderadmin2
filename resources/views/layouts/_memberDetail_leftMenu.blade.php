<div class="wrapper">
    <p><b>@lang('message.INFORMATION')</b></p>
    <ul class="nav nav-pills nav-stacked nav-sm">
        @if(Auth::user()->op_level < 3 || $detail->role_name !='PLAYER')
        <li class="{!! (Request::is('members/list/detail/basic_info/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/basic_info/g/{{$detail->op_idx}}/1">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>
                @lang('message.basicInfo') <!--<span class="badge pull-right">52</span>-->
            </a>
        </li>
        @else

            @if(Auth::user()->op_level == $detail->admin_level )
                @if( $detail->role_name !='PLAYER' )
                    {{--*/ $url = '/members/list/detail/basic_info/g/'.$detail->op_idx.'/1' /*--}}
                @else
                    {{--*/ $url = '/members/list/detail/basic_info/g/player/'.$detail->op_idx.'/'.$detail->mb_idx.'/1' /*--}}
                @endif
            @else
            {{--*/ $url = '/members/list/detail/basic_info/g/player/'.$detail->op_idx.'/'.$detail->mb_idx.'/1' /*--}}
            @endif

        <li class="{!! (Request::is('members/list/detail/basic_info/*') ? ' active' : '') !!}">
            <a href="{{$url}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>
                @lang('message.basicInfo') <!--<span class="badge pull-right">52</span>-->
            </a>
        </li>
        @endif

        @if(Auth::user()->op_level == 1 && $detail->admin_level == 1)
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/1">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.adminlist')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createAdmin')</a>
        </li>
        @elseif(Auth::user()->op_level == 1 && $detail->admin_level == 2)
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/1">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.operatorlist')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createOperator')</a>
        </li>
        @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 2)
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/1">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.operatorlist')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createOperator')</a>
        </li>
        @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 3)
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/3">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.playerList')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createPlayer')</a>
        </li>
        @elseif(Auth::user()->op_level == 2 && $detail->admin_level == 4)
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/3">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.playerList')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createPlayer')</a>
        </li>
        @elseif(Auth::user()->op_level == 3 && $detail->admin_level == 3)
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/3">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.playerList')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createPlayer')</a>
        </li>
        @elseif(Auth::user()->op_level == 3 && $detail->admin_level == 4 && $detail->role_name !='PLAYER')
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/3">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.playerList')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createPlayer')</a>
        </li>
        @elseif(Auth::user()->op_level == 4 && $detail->admin_level == 4 && $detail->role_name !='PLAYER')
        <li class="{!! (Request::is('members/list/detail/user_list/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_list/{{$detail->op_idx}}/3">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.playerList')</a>
        </li>
        <li class="{!! (Request::is('members/list/detail/user_create/*') ? ' active' : '') !!}">
            <a href="/members/list/detail/user_create/g/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.createPlayer')</a>
        </li>
        @endif

        @if(Auth::user()->op_level != $detail->admin_level)
        <li class="{!! (Request::is('members/list/detail/point/g/add/*') ? ' active' : '') !!}">
            @if(Auth::user()->op_level < 3 || $detail->role_name !='PLAYER')
            <a href="/members/list/detail/point/g/add/{{$detail->op_idx}}">
            <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.addPoint')
            </a>
            @else
            <a href="/members/list/detail/point/g/add/player/{{$detail->op_idx}}/{{$detail->mb_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.addPoint')
            </a>
            @endif
        </li>
        <li class="{!! (Request::is('members/list/detail/point/g/sub/*') ? ' active' : '') !!}">
            @if(Auth::user()->op_level < 3 || $detail->role_name !='PLAYER')
            <a href="/members/list/detail/point/g/sub/{{$detail->op_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.subtractPoint')
            </a>
            @else
            <a href="/members/list/detail/point/g/sub/player/{{$detail->op_idx}}/{{$detail->mb_idx}}">
                <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.subtractPoint')
            </a>
            @endif
        </li>
        @else

            @if($detail->role_name =='PLAYER')
            <li class="{!! (Request::is('members/list/detail/point/g/add/*') ? ' active' : '') !!}">
                <a href="/members/list/detail/point/g/add/player/{{$detail->op_idx}}/{{$detail->mb_idx}}">
                    <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.addPoint')
                </a>

            </li>
            <li class="{!! (Request::is('members/list/detail/point/g/sub/*') ? ' active' : '') !!}">
                <a href="/members/list/detail/point/g/sub/player/{{$detail->op_idx}}/{{$detail->mb_idx}}">
                    <i class="fa fa-fw m-r-5 fa-circle text-danger"></i>@lang('message.subtractPoint')
                </a>

            </li>
            @endif
        @endif
    </ul>
</div>
