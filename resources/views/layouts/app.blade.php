<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <title>GAME ADMIN</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{$assetUrl}}assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/css/animate.min.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/css/style.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/css/style-responsive.min.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/css/theme/default.css" rel="stylesheet" />
    <link href="{{$assetUrl}}assets/css/custom.css" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->
    @if(!empty($css['basicpages']))
    @foreach($css['basicpages'] as $bpage)
    <link href="{{$assetUrl.$bpage}}" rel="stylesheet">
    @endforeach
    @endif

    @if(!empty($css['pages']))
    @foreach($css['pages'] as $page)
    <link href="{{$assetUrl.$page}}" rel="stylesheet">
    @endforeach
    @endif
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{$assetUrl}}assets/plugins/pace/pace.min.js"></script>
    <script src="{{$assetUrl}}assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="{{$assetUrl}}assets/js/jquery.maskMoney.js" type="text/javascript"></script>
    <!-- ================== END BASE JS ================== -->

    <!--    <script type="text/javascript">-->
    <!--        $(document).ready(function () {-->
    <!--            //Disable cut copy paste-->
    <!--            $('body').bind('cut copy paste', function (e) {-->
    <!--                e.preventDefault();-->
    <!--            });-->
    <!--            //Disable mouse right click-->
    <!--            $("body").on("contextmenu",function(e){-->
    <!--                return false;-->
    <!--            });-->
    <!--        });-->
    <!--    </script>-->

</head>
<script type="text/javascript">
    $(function () {
        setInterval(function () {
            countNotification();
        }, 10000);
    })

    function countNotification() {
        if($("#notiCenter").data("noticount") > 0 || $("#notiCenter").html() > 0) {
            $("#newMSG").get(0).play();
        }
    }


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
<!-- ================== BEGIN BASE JS ================== -->

<script src="{{$assetUrl}}assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="{{$assetUrl}}assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="{{$assetUrl}}assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="{{$assetUrl}}assets/crossbrowserjs/html5shiv.js"></script>
<script src="{{$assetUrl}}assets/crossbrowserjs/respond.min.js"></script>
<script src="{{$assetUrl}}assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="{{$assetUrl}}assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="{{$assetUrl}}assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->
@if(!empty($js['basicscripts']))
@foreach($js['basicscripts'] as $bscript)
<script src="{{$assetUrl.$bscript}}"></script>
@endforeach
@endif

@if(!empty($js['scripts']))
@foreach($js['scripts'] as $script)
<script src="{{$assetUrl.$script}}"></script>
@endforeach
@endif
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-header-fixed page-sidebar-fixed page-with-two-sidebar">
    <!-- begin #header -->
    @include('layouts._header')
    <!-- end #header -->

    <!-- begin #sidebar -->
    @include('layouts._sidebar_left_menu')
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #sidebar-right -->
    @include('layouts._sidebar_right')
    <div class="sidebar-bg sidebar-right"></div>
    <!-- end #sidebar-right -->

    <!-- begin #content -->
    @yield('content')
    <!-- end #content -->

    <!-- begin theme-panel -->

    <!-- end theme-panel -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->



<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{$assetUrl}}assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="{{$assetUrl}}assets/plugins/jquery-knob/js/jquery.knob.js"></script>
<script src="{{$assetUrl}}assets/js/page-with-two-sidebar.demo.min.js"></script>
<script src="{{$assetUrl}}assets/js/jquery.alphanumeric.js"></script>
<script src="{{$assetUrl}}assets/js/apps.min.js"></script>
<script src="//js.pusher.com/3.2/pusher.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<script>
    $(document).ready(function() {
        App.init();
        PageWithTwoSidebar.init();
        // DashboardV2.init();
        // TableManageButtons.init();

        <?php if(!empty($initJs)): ?>
            <?php foreach($initJs as $script): ?>
        <?php echo e($script); ?>
        <?php endforeach; ?>
        <?php endif; ?>

        <?php if( $errors->count() > 0): ?>
        $('#modal-error').modal();
        <?php endif; ?>

        <?php if(Session::has('processResult')): ?>
        $('#modal-alert').modal();
        <?php endif; ?>

        $("[name=op_point]").css("ime-mode", "disabled");
        $("[name=op_point]").keyup(function () {
            var str = this.value.replace(/[\,]/g,"");
            $("[name=op_point]").val(str.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        })

        $(".hidden-xs").children(".f-s-14").trigger("click");
    });
</script>
</body>
</html>