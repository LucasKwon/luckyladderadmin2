<div class="wrapper">
    <p><b>@lang('message.INFORMATION')</b></p>
    <ul class="nav nav-pills nav-stacked nav-sm">
        <li class="{!! (Request::is('games/status/detail/*') ? ' active' : '') !!}"><a
                    href="/games/status/detail/{{Request::segment(4)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.basicInfo')</a></li>
<!--        <li class="{!! (Request::is('members/player/detail/betwin/*') ? ' active' : '') !!}"><a-->
<!--                    href="/members/player/detail/betwin/{{Request::segment(5)}}"><i-->
<!--                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.bet/win')</a></li>-->
<!--        <li class="{!! (Request::is('members/player/detail/deposits/*') ? ' active' : '') !!}"><a-->
<!--                    href="/members/player/detail/deposits/{{Request::segment(5)}}"><i-->
<!--                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.deposit/withdrawal')</a></li>-->
<!--        <li class="{!! (Request::is('members/player/detail/bonus/*') ? ' active' : '') !!}"><a-->
<!--                    href="/members/player/detail/bonus/{{Request::segment(5)}}"><i-->
<!--                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.bonus')</a></li>-->
    </ul>
</div>