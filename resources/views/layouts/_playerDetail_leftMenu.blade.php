<div class="wrapper">
    <p><b>@lang('message.INFORMATION')</b></p>
    <ul class="nav nav-pills nav-stacked nav-sm">
        <li class="{!! (Request::is('members/player/detail/summary/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/summary/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.summary')
                <!--<span class="badge pull-right">52</span>--></a></li>
        <li class="{!! (Request::is('members/player/detail/basic/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/basic/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.basicInfo')</a></li>
        <li class="{!! (Request::is('members/player/detail/betwin/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/betwin/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.bet/win')</a></li>
        <li class="{!! (Request::is('members/player/detail/deposits/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/deposits/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.deposit/withdrawal')</a></li>
        <li class="{!! (Request::is('members/player/detail/bonus/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/bonus/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-danger"></i> @lang('message.bonus')</a></li>
        <!--                    <li><a href="email_inbox_v2.html"><i class="fa fa-fw m-r-5 fa-circle text-danger"></i> Login Info</a></li>-->
    </ul>
    @if(Auth::user()->siteid == $playerDetail->upper_siteid)
    <p><b>@lang('message.MANUALTRANSACTIONS')</b></p>
    <ul class="nav nav-pills nav-stacked nav-sm m-b-0">
        <li class="{!! (Request::is('members/player/detail/manualdeposit/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/manualdeposit/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-primary"></i> @lang('message.manualDeposit')</a></li>
        <li class="{!! (Request::is('members/player/detail/manualwithdrawal/*') ? ' active' : '') !!}"><a
                    href="/members/player/detail/manualwithdrawal/{{Request::segment(5)}}"><i
                        class="fa fa-fw m-r-5 fa-circle text-primary"></i> @lang('message.manualWithdrawal')</a></li>
        <!--        <li ><a href="javascript:;"><i class="fa fa-fw m-r-5 fa-circle text-primary"></i> @lang('message.manualBonus')</a></li>-->
    </ul>
    @endif
</div>