<audio id="newMSG">
    <source src="/sound/withdrawal.mp3" type="audio/mpeg" />
</audio>
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle pull-left" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle pull-right" data-click="right-sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- LOGO -->
            <a href="/" class="navbar-brand"><span class="navbar-logo">
                    <!--<img src="/resource/kr/images/common/logo_pink.png" />--></span></a>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Search -->
            <li>
                <div class="list-info">
                    <span>@lang('message.todayDeposit')</span>
                    <h4>
                        <i class="fa fa-money"> {{$depositAmountToday->DEPOSITS}}</i>
                    </h4>
                </div>
            </li>
            <li>
                <div class="list-info">
                    <span>@lang('message.todayWithdrawal')</span>
                    <h4>
                        <i class="fa fa-credit-card"> {{$depositAmountToday->PAYOUT}}</i>
                    </h4>
                </div>
            </li>


            <li class="divider hidden-xs"></li>

            <li class="dropdown">
                <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                    <i class="fa fa-bell-o"></i>
                    <span class="label" id="notiCenter" data-noticount="{{$depositRequestCnt->dcnt + $depositRequestCnt->pcnt}}">{{$depositRequestCnt->dcnt + $depositRequestCnt->pcnt}}</span>
                </a>
                <ul class="dropdown-menu media-list pull-right animated fadeInDown">
                    <li class="dropdown-header">Notifications ({{$depositRequestCnt->dcnt + $depositRequestCnt->pcnt}})</li>
                    <li class="media">
                        <a href="/transaction/deposits/{{Auth::user()->vendor_id}}">
                            <div class="media-left"><i class="fa fa-money media-object bg-red"></i></div>
                            <div class="media-body">
                                <h6 class="media-heading">Deposit Request</h6>
                                <div class="text-muted f-s-11" id="notiCenterDeposit" data-deposit="{{$depositRequestCnt->dcnt}}">{{$depositRequestCnt->dcnt}}</div>
                            </div>
                        </a>
                    </li>
                    <li class="media">
                        <a href="/transaction/withdrawals/{{Auth::user()->vendor_id}}">
                            <div class="media-left"><i class="fa fa-credit-card media-object bg-green"></i></div>
                            <div class="media-body">
                                <h6 class="media-heading"> Withdrawal Request</h6>
                                <div class="text-muted f-s-11" id="notiCenterWithdrawal" data-withdrawal="{{$depositRequestCnt->pcnt}}">{{$depositRequestCnt->pcnt}}</div>
                            </div>
                        </a>
                    </li>
<!--                    <li class="media">-->
<!--                        <a href="javascript:;">-->
<!--                            <div class="media-left"><i class="fa fa-file-powerpoint-o media-object bg-blue"></i></div>-->
<!--                            <div class="media-body">-->
<!--                                <h6 class="media-heading"> Point Request</h6>-->
<!--                                <div class="text-muted f-s-11">2 hour ago</div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="dropdown-footer text-center">-->
<!--                        <a href="javascript:;">View more</a>-->
<!--                    </li>-->
                </ul>
            </li>

            <li class="dropdown navbar-flag">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{$assetUrl}}assets/img/flags/{{ Config::get('languages')[App::getLocale()] }}.jpg" alt="" />
                    <span class="hidden-xs">
                        {{ Config::get('languages')[App::getLocale()] }}
                    </span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li class="arrow"></li>
                    @foreach (Config::get('languages') as $lang => $language)
                    @if ($lang != App::getLocale())
                    <li class="{{$language}}"><a href="{{ route('lang.switch', $lang) }}">{{$language}}</a></li>
                    @endif
                    @endforeach

                </ul>
            </li>
            <li class="divider hidden-xs"></li>
            <li class="hidden-xs">
                <a href="javascript:;" data-click="right-sidebar-toggled" class="f-s-14">
                    <i class="fa fa-th"></i>
                </a>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
@include('layouts.notification')
@include('layouts.processResult')