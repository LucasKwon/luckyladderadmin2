@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.accountings')</a></li>
        <li>@lang('message.accountStatus')</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.accountStatus')<small></small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.betWinStatusList')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table1" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.bet')</th>
                                <th>@lang('message.win')</th>
                                <th>@lang('message.bet-win')</th>
                                <th>@lang('message.payout') (%)</th>
                                <th>@lang('message.dealSet')</th>
                                <th>@lang('message.accountingAmount')</th>
                                <th>@lang('message.startDate')</th>
                                <th>@lang('message.endDate')</th>
                                <th>@lang('message.status')</th>
                                @if(Auth::user()->op_level < 3)
                                <th>@lang('message.command')</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($accountingList as $list)
                            @if($list->acc_type == 'BETWIN')
                            <tr >
                                <td>{{$list->op_id}}</td>
                                <td style="text-align:right;">{{number_format($list->bet_amount,2)}}</td>
                                <td style="text-align:right;">{{number_format($list->win_amount,2)}}</td>
                                <td style="text-align:right;">
                                    @if(($list->bet_amount-$list->win_amount) > 0)
                                    <p class="text-info">{{number_format(($list->bet_amount-$list->win_amount), 2)}}</p>
                                    @else
                                    <p class="text-danger">{{number_format(($list->bet_amount-$list->win_amount), 2)}}</p>
                                    @endif
                                </td>
                                <td style="text-align:right;" >{{number_format($list->pay_amount, 2)}}%</td>
                                <td style="text-align:right;" >{{number_format($list->acc_deal, 2)}}%</td>
                                <td style="text-align:right;">{{number_format($list->acc_amount, 2)}}</td>
                                <td style="text-align:right;" >{{$list->acc_start_date}}</td>
                                <td style="text-align:right;" >{{$list->acc_end_date}}</td>
                                <td style="text-align:center;">{{$list->acc_status}}</td>
                                @if(Auth::user()->op_level < 3)
                                <td style="text-align:center;">
                                    @if($list->acc_status == 'REQUESTED')
                                    <button type="button" name="btn_request" onclick="updateStatus({{$list->acc_id}}, 'C')" class="btn btn-info btn-sm m-r-5 m-b-5">
                                        @lang('message.confirm')
                                    </button>
                                    <button type="button" name="btn_request" onclick="updateStatus({{$list->acc_id}}, 'R')" class="btn btn-danger btn-sm m-r-5 m-b-5">
                                        @lang('message.restore')
                                    </button>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.rollingStatusList')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table2" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.bet')</th>
<!--                                <th>@lang('message.win')</th>-->
                                <th>@lang('message.drawBet')</th>
<!--                                <th>Roll - DrawBet</th>-->
<!--                                <th>@lang('message.payout') (%)</th>-->
                                <th>@lang('message.dealSet')</th>
                                <th>@lang('message.accountingAmount')</th>
                                <th>@lang('message.startDate')</th>
                                <th>@lang('message.endDate')</th>
                                <th>@lang('message.status')</th>
                                @if(Auth::user()->op_level < 3)
                                <th>@lang('message.command')</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($accountingList as $list)
                            @if($list->acc_type == 'ROLLING')
                            <tr >
                                <td>{{$list->op_id}}</td>
                                <td style="text-align:right;">{{number_format($list->bet_amount,2)}}</td>
<!--                                <td style="text-align:right;">{{number_format($list->win_amount,2)}}</td>-->
                                <td style="text-align:right;">
                                    <p class="text-danger">{{number_format(($list->tie_amount), 2)}}</p>
                                </td>
<!--                                <td style="text-align:right;" >{{number_format($list->pay_amount, 2)}}%</td>-->
                                <td style="text-align:right;" >{{number_format($list->acc_deal, 2)}}%</td>
                                <td style="text-align:right;">{{number_format($list->acc_amount, 2)}}</td>
                                <td style="text-align:right;" >{{$list->acc_start_date}}</td>
                                <td style="text-align:right;" >{{$list->acc_end_date}}</td>
                                <td style="text-align:center;">{{$list->acc_status}}</td>
                                @if(Auth::user()->op_level < 3)
                                <td style="text-align:center;">
                                    @if($list->acc_status == 'REQUESTED')
                                    <button type="button" name="btn_request" onclick="updateStatus({{$list->acc_id}}, 'C')" class="btn btn-info btn-sm m-r-5 m-b-5">
                                        @lang('message.confirm')
                                    </button>
                                    <button type="button" name="btn_request" onclick="updateStatus({{$list->acc_id}}, 'R')" class="btn btn-danger btn-sm m-r-5 m-b-5">
                                        @lang('message.restore')
                                    </button>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</div>
<form name="updateForm" method="POST">
    <input type="hidden" name="reqStatus">
    <input type="hidden" name="reqAccId">
    {{ csrf_field() }}

</form>
<script>
    function updateStatus(accId, status) {
        var f = document.updateForm;
        f.reqAccId.value = accId;
        f.reqStatus.value = status;

        if(confirm('@lang("message.proceedConfirm")')) {
            f.action = "/account/updateAccounting";
            f.submit();
        }
    }
</script>
@endsection