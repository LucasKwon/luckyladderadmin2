@extends('layouts.app')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">@lang('message.accountings')</a></li>
        <li>@lang('message.accountRequest')</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">@lang('message.accountRequest')<small></small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" >
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.searchCondition') </h4>
                </div>
                <div class="panel-body ">
                    {!! Form::open(['class' => 'form-horizontal form-bordered',
                    'name' => 'searchForm', 'url'=> '/account/request/'.Auth::user()->op_idx.'',
                    'method' => 'get', 'data-parsley-validate' => 'true']) !!}
                    <div class="form-group">
                        <label class="control-label col-md-2">@lang('message.searchDuration')</label>
                        <div class="col-md-4">
                            <div class="row row-space-10">
                                <div class="input-group col-md-12">
                                    {!! Form::text('startdate', isset($searchParam['startdate']) ? $searchParam['startdate'] : '',
                                    ['id' => 'datetimepicker3','class' => 'form-control',
                                    'data-parsley-maxlength' => '10',
                                    'required']) !!}
                                    <span class="input-group-addon">@lang('message.to')</span>
                                    {!! Form::text('enddate', isset($searchParam['enddate']) ? $searchParam['enddate'] : '',
                                    ['id' => 'datetimepicker4','class' => 'form-control',
                                    'data-parsley-maxlength' => '10',
                                    'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-danger btn-block btn-lg">@lang('message.viewRequest')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if($accountingDetail)
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.searchResultAccBet')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table1" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.bet')</th>
                                <th>@lang('message.win')</th>
                                <th>@lang('message.bet-win')</th>
                                <th>@lang('message.payout') (%)</th>
                                <th>@lang('message.vendorDeal')</th>
                                <th>@lang('message.accountingAmount')</th>
                                <th>@lang('message.startDate')</th>
                                <th>@lang('message.endDate')</th>
                                <th>@lang('message.request')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($accountingDetail as $list)
                            <tr >
                                <td >{{$list->op_name}}</td>
                                <td style="text-align:right;">{{ number_format($list->bets, 2) }}</td>
                                <td style="text-align:right;">{{ number_format($list->wins, 2) }}</td>
                                <td style="text-align:right;">
                                    @if( $list->betwin > 0)
                                    <p class="text-info">{{ number_format($list->betwin, 2) }}</p>
                                    @else
                                    <p class="text-danger">{{ number_format($list->betwin, 2) }}</p>
                                    @endif
                                </td>
                                <td style="text-align:right;">{{ number_format( $list->payout, 2 ) }}%</td>
                                <td style="text-align:right;">{{ $list->op_profit }}%</td>
                                <td style="text-align:right;">{{ number_format( $list->amount, 2 ) }}</td>
                                <td style="text-align:right;">{{$searchParam['startdate']}}</td>
                                <td style="text-align:right;">{{$searchParam['enddate']}}</td>
                                <td>
                                    <button type="button" name="btn_request" onclick="addAccounting('{{$list->op_id}}',
                                                                                                    '{{$list->op_idx}}',
                                                                                                    '{{$list->op_profit}}',
                                                                                                    '{{$list->bets}}',
                                                                                                    '{{$list->wins}}',
                                                                                                    '{{$list->betwin}}',
                                                                                                    '{{$list->payout}}',
                                                                                                    '{{$list->amount}}',
                                                                                                    '{{$searchParam['startdate']}}',
                                                                                                    '{{$searchParam['enddate']}}'
                                                                                                     )" class="btn btn-danger btn-sm m-r-5 m-b-5">정산하기</button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            @if($accRollingDetail)
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.searchResultAccRoll')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table2" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.bet')</th>
                                <th>@lang('message.exceptionAmount')</th>
                                <th>@lang('message.rollingDeal')</th>
                                <th>@lang('message.rollingAmount')</th>
                                <th>@lang('message.accountingAmount')</th>
                                <th>@lang('message.startDate')</th>
                                <th>@lang('message.endDate')</th>
                                <th>@lang('message.request')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($accRollingDetail as $list)
                                <tr>
                                    <td>{{ $list->op_name}}</td>
                                    <td style="text-align:right;">{{ number_format($list->bet_amount, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->tie_amount, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->op_rolling, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->rol_amount, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->bet_amount*$list->op_rolling, 2) }}</td>
                                    <td style="text-align:right;">{{$searchParam['startdate']}}</td>
                                    <td style="text-align:right;">{{$searchParam['enddate']}}</td>
                                    <td style="text-align:center;">
                                        <button type="button" name="btn_request" onclick="addRollingAccounting('{{$list->op_id}}',
                                                                                                               '{{$list->op_idx}}',
                                                                                                               '{{$list->op_rolling}}',
                                                                                                               '{{$list->bet_amount}}',
                                                                                                               '{{$list->tie_amount}}',
                                                                                                               '{{$list->rol_amount}}',
                                                                                                               '{{$list->bet_amount*$list->op_rolling}}',
                                                                                                               '{{$searchParam['startdate']}}',
                                                                                                               '{{$searchParam['enddate']}}'
                                                                                                               )" class="btn btn-danger btn-sm m-r-5 m-b-5">정산하기</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>

            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.accountingBetWin')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table3" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.bet')</th>
                                <th>@lang('message.win')</th>
                                <th>@lang('message.bet-win')</th>
                                <th>@lang('message.payout') (%)</th>
                                <th>@lang('message.vendorDeal')</th>
                                <th>@lang('message.accountingAmount')</th>
                                <th>@lang('message.startDate')</th>
                                <th>@lang('message.endDate')</th>
                                <th>@lang('message.accountingIssuedDate')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($accountingList as $list)
                                <tr >
                                    <td>{{$list->op_id}}</td>
                                    <td style="text-align:right;">{{number_format($list->bet_amount,2)}}</td>
                                    <td style="text-align:right;">{{number_format($list->win_amount,2)}}</td>
                                    <td style="text-align:right;">
                                        @if(($list->bet_amount-$list->win_amount) > 0)
                                        <p class="text-info">{{number_format(($list->bet_amount-$list->win_amount), 2)}}</p>
                                        @else
                                        <p class="text-danger">{{number_format(($list->bet_amount-$list->win_amount), 2)}}</p>
                                        @endif
                                    </td>
                                    <td style="text-align:right;" >{{number_format($list->pay_amount, 2)}}%</td>
                                    <td style="text-align:right;">{{$list->acc_deal}}%</td>
                                    <td style="text-align:right;" >{{number_format( $list->acc_amount, 2)}}</td>
                                    <td style="text-align:right;" >{{$list->acc_start_date}}</td>
                                    <td style="text-align:right;" >{{$list->acc_end_date}}</td>
                                    <td>{{$list->acc_change_date}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->

            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">@lang('message.accountingRolling')</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table4" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('message.operatorName')</th>
                                <th>@lang('message.bet')</th>
                                <th>@lang('message.exceptionAmount')</th>
                                <th>@lang('message.rollingDeal')</th>
                                <th>@lang('message.accountingAmount')</th>
                                <th>@lang('message.startDate')</th>
                                <th>@lang('message.endDate')</th>
                                <th>@lang('message.accountingIssuedDate')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($accRollingList as $list)
                                <tr>
                                    <td>{{ $list->op_id}}</td>
                                    <td style="text-align:right;">{{ number_format($list->bet_amount, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->tie_amount, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->acc_deal, 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($list->bet_amount*$list->acc_deal, 2) }}</td>
                                    <td style="text-align:right;" >{{$list->acc_start_date}}</td>
                                    <td style="text-align:right;" >{{$list->acc_end_date}}</td>
                                    <td>{{$list->acc_change_date}}</td>
                                </tr>
                                @endforeach
                           
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
</div>
@if($accountingDetail)
<form name="accountingForm" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="reqOpId" value=""/>
    <input type="hidden" name="reqOpIdx" value=""/>
    <input type="hidden" name="reqOpProfit" value=""/>
    <input type="hidden" name="reqBets" value=""/>
    <input type="hidden" name="reqWins" value=""/>
    <input type="hidden" name="reqBetWin" value=""/>
    <input type="hidden" name="reqPayout" value=""/>
    <input type="hidden" name="reqAmount" value=""/>
    <input type="hidden" name="reqAccType" value=""/>
    <input type="hidden" name="reqStartDate" value=""/>
    <input type="hidden" name="reqEndDate" value=""/>
</form>
@endif
@if($accRollingDetail)
<form name="accountingRollingForm" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="reqOpId" value=""/>
    <input type="hidden" name="reqOpIdx" value=""/>
    <input type="hidden" name="reqOpRolling" value=""/>
    <input type="hidden" name="reqBetAmount" value=""/>
    <input type="hidden" name="reqTieAmount" value=""/>
    <input type="hidden" name="reqRollAmount" value=""/>
    <input type="hidden" name="reqAccAmount" value=""/>
    <input type="hidden" name="reqAccType" value=""/>
    <input type="hidden" name="reqStartDate" value=""/>
    <input type="hidden" name="reqEndDate" value=""/>
</form>
@endif
<script>

    $("#btn_search").click(goSearch);
    $("[name=inpSearchKeyword]").keypress(function(e){
        if (event.keyCode==13) {
            goSearch(e);
        }
    });

    function goSearch(e){
        var f = document.searchForm;

        if(f.startdate.value == '' || f.enddate.value == '') {
            alert('please input Start Date & End Date');
            return false;
        }

        f.submit();
        if (e) {
            e.preventDefault();
        }
    }

    function addAccounting(opId, opIdx, opProfit, bets, wins, betwin, payout, amount, startDate, endDate) {
        var f = document.accountingForm;
        f.reqOpId.value = opId;
        f.reqOpIdx.value = opIdx;
        f.reqOpProfit.value = opProfit;
        f.reqBets.value = bets;
        f.reqWins.value = wins;
        f.reqBetWin.value = betwin;
        f.reqPayout.value = payout;
        f.reqAmount.value = amount;
        f.reqAccType.value = 'BETWIN';
        f.reqStartDate.value = startDate+' 00:00:00';
        f.reqEndDate.value = endDate+' 23:59:59';

        if(confirm('@lang("message.proceedConfirm")')) {
            f.action = "/account/addAccounting";
            f.submit();
        }
    }

    function addRollingAccounting(opId, opIdx, opRolling, betAmount, tieAmount, rollAmount, accAmount, startDate, endDate) {
        var f = document.accountingRollingForm;
        f.reqOpId.value = opId;
        f.reqOpIdx.value = opIdx;
        f.reqOpRolling.value = opRolling;
        f.reqBetAmount.value = betAmount;
        f.reqTieAmount.value = tieAmount;
        f.reqRollAmount.value = rollAmount;
        f.reqAccAmount.value = accAmount;
        f.reqAccType.value = 'ROLLING';
        f.reqStartDate.value = startDate+' 00:00:00';
        f.reqEndDate.value = endDate+' 23:59:59';

        if(confirm('@lang("message.proceedConfirm")')) {
            f.action = "/account/addRollingAccounting";
            f.submit();
        }
    }

</script>
@endsection