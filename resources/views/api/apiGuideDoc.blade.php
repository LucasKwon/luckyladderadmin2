
@extends('layouts.app')
@section('content')
<style type="text/css">

    h1,h2,h3,h4,h5,h6
    {
        margin: 0 0 .5em;
        font-weight: 500;
        line-height: 1.1;
    }

    h1 { font-size: 2.25em; } /* 36px */
    h2 { font-size: 1.75em; } /* 28px */
    h3 { font-size: 1.375em; } /* 22px */
    h4 { font-size: 1.125em; } /* 18px */
    h5 { font-size: 1em; } /* 16px */
    h6 { font-size: .875em; } /* 14px */

    p
    {
        margin: 0 0 1.5em;
        line-height: 1.5;
    }

    blockquote
    {
        padding: 1em 2em;
        margin: 0 0 2em;
        border-left: 5px solid #eee;
    }

    hr
    {
        height: 0;
        margin-top: 1em;
        margin-bottom: 2em;
        border: 0;
        border-top: 1px solid #ddd;
    }

    table
    {
        background-color: transparent;
        border-spacing: 0;
        border-collapse: collapse;
        border-top: 1px solid #ddd;
    }

    th, td
    {
        padding: .5em 1em;
        vertical-align: top;
        text-align: center;
        border-bottom: 1px solid #ddd;
    }
    .tableHead {
        text-align: center;
        background-color: #C4DEFF !important;
    }

    a:link { color: royalblue; }
    a:visited { color: purple; }
    a:focus { color: black; }
    a:hover { color: green; }
    a:active { color: red; }


    /* -----------------------
    Layout styles
    ------------------------*/
    #containerApi{_display:inline-block;width:100%;overflow: hidden;}
    #containerApi:after{display:block;clear:both;content:''}
    #containerApi{width:auto;padding:10px;border:1px solid #bfbfbf;background:#e5e5e5}
    #containerapi
    {
        max-width: 70em;
        margin: 0 auto;
    }

    .contentapi
    {
        overflow: hidden;
        padding: 1em 1.25em;
        background-color: #fff;
        width:100%;
        padding:10px;
        border:1px solid #fff;
        /*background:#e5e5e5;*/

    }

    .main
    {
        margin-bottom: 1em;
        border: 1px solid #bdbdbd;
    }

    .aside
    {
        margin-bottom: 1em;
        border: 1px solid #bdbdbd;
    }

    /* -----------------------
    Single styles
    ------------------------*/

    .table
    {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }


    /* -----------------------
    Wide styles
    ------------------------*/

    @media (min-width: 55em)
    {

        .contentapi { padding: 2em 3em; }
        .main
        {
            float: left;
            width: 25%;
            margin-right: 1%;
            margin-bottom: 1em;
            padding: 20px;
        }

        .aside
        {
            float: left;
            width: 73%;
            margin-bottom: 1em;
            padding: 20px;
        }

    }


</style>
<script>
    function getOffsetTop(el) {
        var top = 0;
        if (el.offsetParent) {
            do { top += el.offsetTop;
            } while (el = el.offsetParent);
            window.scroll(0, top-100);
        }


    }


</script>
<div id="content" class="content content-full-width">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">@lang('message.home')</a></li>
        <li><a href="javascript:;">Lucky Ladder API Document</a></li>
        </li>
        <!--        <li class="active">@lang('message.basicInfo')</li>-->
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Lucky Ladder API Document</h1>
<!--    <div id="page-container" class="fade page-header-fixed page-sidebar-fixed page-with-two-sidebar">-->
    <!-- begin vertical-box -->
<!--    <div class="vertical-box">-->
<!--        <div class="vertical-box-column width-250">-->
<!--        </div>-->
<!---->
<!--        <!-- end vertical-box-column -->
<!---->
<!--    </div>-->
    <div class="contentapi">
        <div class="containerapi">
            <div class="main">

<!--                <div class="panel panel-default">-->
<!--                    <div class="panel-heading">-->
<!--                        <h4 class="panel-title">Lucky Ladder API Document</h4>-->
<!--                    </div>-->
<!--                    <div class="panel-body">-->
<!--                        <ul>-->
<!--                            <li><a href="#into">API Document</a></li>-->
<!--                            <li><a href="#apiCreateAccount">플레이어 계정생성</a></li>-->
<!--                            <li><a href="#apiFlashSession">Game Flash Session 발급</a></li>-->
<!--                            <li><a href="#apiGameLobbyUrl">게임 로비 URL 호출</a></li>-->
<!--                            <li><a href="#apiBalance">Player Balance 조회</a></li>-->
<!--                            <li><a href="#apiPointCredit">Player Point Credit</a></li>-->
<!--                            <li><a href="#apiPointDebit">Player Point Debit</a></li>-->
<!--                            <li><a href="#apiGameHistory">Game History</a></li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->

                <h3 style="padding-left:10px;" ><b>Lucky Ladder API</b></h3>
                <ul>
                    <li style="padding:2px;"><a href="#into">API Document</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiCreateAccount);">플레이어 계정생성</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiFlashSession);">Game Flash Session 발급</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiGameLobbyUrl);">게임 로비 URL 호출</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiBalance);">Player Balance 조회</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiPointCredit);">Player Point Credit</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiPointDebit);">Player Point Debit</a></li>
                    <li style="padding:2px;"><a href="javascript:getOffsetTop(apiGameHistory);">Game History</a></li>
                </ul>
            </div>
            <div class="aside">
                <br>
                <h2 style="padding-left:10px;"> Lucky Ladder API Document</h2>
                <br>
                <blockquote> <p>LuckyLadder 에서 제공하는 Rest-ful API 관련 정보 구성과 사용법에 대해 설명하고 있다. </p>

                </blockquote>
                <p style="padding-left: 20px;word-break:break-all;word-wrap:break-word;"> 이 문서는 Client 개발자에게 럭키라더(http://game.luckyladder168.com/api/v1/)과의 연동을 위한 응용 프로그램 개발에 필요한 데이터 구조와 game.luckyladder168.com/api/v1/ 에서 제공하는 기능에 대한 정보를 제공한다.
                </p> <br>
                <hr>
                <div id="restFulInfo" >
                <h3 style="padding-left: 10px;"><i class="fa fa-tags"></i> Return Status Code  정의</h3>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="tableHead">상태코드</th>
                        <th class="tableHead">상태메시지</th>
                        <th class="tableHead">설명</th>
                        <th class="tableHead">HTTP Return Code</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>200</th>
                        <td>Success</td>
                        <td>성공</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th>700</th>
                        <td>Not Found</td>
                        <td>요청한 값이 없을때</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th>701</th>
                        <td>Request Duplicate</td>
                        <td style="word-break:break-all;word-wrap:break-word;">요청한값이 중복</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th>702</th>
                        <td>No parameters</td>
                        <td style="word-break:break-all;word-wrap:break-word;">파라미터값 없음</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th >703</th>
                        <td>Different is parameter</td>
                        <td style="word-break:break-all;word-wrap:break-word;">파라미터 값이 동일하지 않음</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th >705</th>
                        <td>Input Point bigger</td>
                        <td>입력값이 기존값보다 큼</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th>800</th>
                        <td>DB Error</td>
                        <td style="word-break:break-all;word-wrap:break-word;">select, inset,update,delete 시 모든 DB Error</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <th>900</th>
                        <td>API Error</td>
                        <td>API 에러</td>
                        <td>500</td>
                    </tr>
                    </tbody>
                </table>
                <h3 style="padding-left: 10px;"><i class="fa fa-tags"></i> Return Code  정의</h3>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="tableHead">상태코드</th>
                        <th class="tableHead">상태메시지</th>
                        <th class="tableHead">설명</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>0</th>
                        <td>Success</td>
                        <td>성공</td>
                    </tr>
                    <tr>
                        <th>99</th>
                        <td>error</td>
                        <td>실패</td>

                    </tr>
                    </tbody>
                </table>
                </div>
                <br>
                <hr >
                <div id="apiCreateAccount" style="offset-top:400;">
                    <br />
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i><b> Create Account</b> </h2>
                    <p style="padding-left: 30px;"> - 럭키라더 게임을 사용하기 위한 플레이어 계정생성한다</p>
                <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : POST 방식으로 호출</h4>
                   <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  " http://game.luckyladder168.com/api/v1/service/account/create/{operatorId}/{API Key}" </p>
                <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Parameter 형식(POST 방식)</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">파라미터명</th>
                            <th class="tableHead">타입</th>
                            <th class="tableHead">필수여부</th>
                            <th class="tableHead">설명</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>operatorId</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>럭키라더에서 발급 받은 오퍼레이터 아이디를 입력</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>생성할 플레이어 아이디</td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td>String</td>
                            <td>선택</td>
                            <td>생성할 플레이어 패스워드</td>
                        </tr>
                        <tr>
                            <td>userName</td>
                            <td>String</td>
                            <td>선택</td>
                            <td>생성할 플레이어 이름</td>
                        </tr>
                        <tr>
                            <td>currency</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>게임에서 사용할 화폐단위</td>
                        </tr>
                        </tbody>
                    </table>

                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>returnCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패여부 상태코드름</td>
                            <td>0: 성공(정상)
                                99:실패</td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left; table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                { "statusCode": 800, "statusMessage": "DB Error", "description": "error massage", "returnCode": 99 }
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플<br>
                                { "statusCode": 200, "statusMessage": "Success", "description": "", "returnCode": 0 }
                            </td>

                        </tr>

                    </table>
                </div>
                <br>
                <hr>
                <div id="apiFlashSession">
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i><b> Game Flash Session 발급 </b></h2>
                    <p style="padding-left: 30px;"> - 럭키라더 게임 사용하기 위한 플레이어 임시 세션발급</p>
                    <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : GET 방식으로 호출</h4>
                    <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  " http://game.luckyladder168.com/api/v1/generate/{operatorId}/{apiKey}/{userId}" </p>


                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>FlashSession</td>
                            <td>1</td>
                            <td></td>
                            <td>Player 임시 세션 발급 값</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left; table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                {"statusCode":404,"statusMessage":"Error","description":"Found Not User Information or Invalid Parameter"}
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플<br>
                                {"statusCode":200,"statusMessage":"Success","FlashSession":"47d7d960e2cae157fca314321a32a2aa1b987901"}
                            </td>

                        </tr>

                    </table>
                </div>
                <br>
                <hr>
                <div id="apiGameLobbyUrl">
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i> <b>게임 로비 URL 호출</b> </h2>
                    <p style="padding-left: 30px;"> - 플레이어가 요청하여 임시로 발급받은 세션을 가지고 게임로비 URL을 호출한다.</p>
                    <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : GET 방식으로 호출</h4>
                    <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  " http://game.luckyladder168.com/api/v1/game/lobby/{operatorId}/{apiKey}/{userId}/{flashSession}" </p>


                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>lobbyUrl</td>
                            <td>1</td>
                            <td></td>
                            <td>임시 세션으로 발급 받은 게임 로비 URL</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left; table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                {"statusCode":401,"statusMessage":"Unauthorized","description":"Invalid ApiKey or Flash Session"}
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플<br>
                                {"statusCode":200,"statusMessage":"Success"
                                ,"lobbyUrl":"http://game.luckyladder168.com/luckyLadder/lobby/eyJpdiI6InJCSHVLNUQyMTVqcmNVdlBkc
                                1FRVlE9PSIsInZhbHVlIjoiQ2dIOEE1UDVPc253UlpqbEk1OGpFa3R2MDlpSGRWZFlHektSZTBEREpPVGhV
                                QlpkbE85N0tQSmx0OTJDTmh2Z1A1ZWgzM2QzZnQzaUhUbXprSlpYOVE9PSIsIm1hYyI6IjFlY2U4OWE5ZGQ
                                zZmU0OGVhYmYwMTdlOTg2Y2E5ZjVmYjYwMmNiYmQ2MDczZWRjN2RkZGYyMzAyYzRkNWYwMWQifQ=="}
                            </td>

                        </tr>

                    </table>
                </div>
                <br>
                <hr>
                <div id="apiBalance">
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i> <b>Player Balance 조회</b> </h2>
                    <p style="padding-left: 30px;"> - 럭키라더 게임 안에 있는 플레이어 게임 포인트 조회.</p>
                    <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : GET 방식으로 호출</h4>
                    <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  "http://game.luckyladder168.com/api/v1/service/account/getBalance/{operatorId}/{apiKey}/{userId}" </p>


                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>returnCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패여부 상태코드</td>
                            <td>0: 성공(정상)
                                99:실패</td>
                        </tr>
                        <tr>
                            <td>resultData</td>
                            <td>1</td>
                            <td>Y</td>
                            <td>플레이어 게임 포인트 값</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left; table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                {"statusCode":700,"statusMessage":"DB null","description":"Game transaction is null","returnCode":0,"resultData":null}
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플<br>
                                {"statusCode":200,"statusMessage":"Success","description":"","returnCode":0
                                ,"resultData":[{"op_id":"master","mb_id":"nck6026","mb_point":"20000.00","mb_currency":"KRW"}]}
                            </td>

                        </tr>

                    </table>
                </div>
                <br>
                <hr>
                <div id="apiPointCredit">
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i> <b>Player Point Credit</b> </h2>
                    <p style="padding-left: 30px;"> - 플레이어가 게임을 하기 위해 럭키라더 게임으로 게임 포인트를 이동한다.</p>
                    <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : POST 방식으로 호출</h4>
                    <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  "http://game.luckyladder168.com/api/v1/service/deposit" </p>

                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Parameter 형식(POST 방식)</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">파라미터명</th>
                            <th class="tableHead">타입</th>
                            <th class="tableHead">필수여부</th>
                            <th class="tableHead">설명</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>operatorId</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>럭키라더에서 발급 받은 오퍼레이터 아이디를 입력</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>플레이어 아이디</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>오퍼레이터에게 발급한 API Key</td>
                        </tr>
                        <tr>
                            <td>amount</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>럭키라더로 이동할 게임 포인트</td>
                        </tr>
                        <tr>
                            <td>currency</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>게임에서 사용할 화폐단위(KRW)</td>
                        </tr>
                        </tbody>
                    </table>

                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>returnCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패여부 상태코드</td>
                            <td>0: 성공(정상)
                                99:실패</td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left; table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                {"statusCode":800,"statusMessage":"DB Error","description":"SQLSTATE[42000]: Syntax error",returnCode":99}
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플<br>
                                {"statusCode":200,"statusMessage":"Success","description":"","returnCode":0}
                            </td>

                        </tr>

                    </table>
                </div>
                <br>
                <hr>
                <div id="apiPointDebit">
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i> <b>Player Point Dedit</b> </h2>
                    <p style="padding-left: 30px;"> - 플레이어가 럭키라더에 가지고있는 게임 포인트를  차감한다.</p>
                    <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : POST 방식으로 호출</h4>
                    <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  "http://game.luckyladder168.com/api/v1/service/payout" </p>

                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Parameter 형식(POST 방식)</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">파라미터명</th>
                            <th class="tableHead">타입</th>
                            <th class="tableHead">필수여부</th>
                            <th class="tableHead">설명</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>operatorId</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>럭키라더에서 발급 받은 오퍼레이터 아이디를 입력</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>플레이어 아이디</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>오퍼레이터에게 발급한 API Key</td>
                        </tr>
                        <tr>
                            <td>amount</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>럭키라더로 이동할 게임 포인트</td>
                        </tr>
                        <tr>
                            <td>currency</td>
                            <td>String</td>
                            <td>필수</td>
                            <td>게임에서 사용할 화폐단위(KRW)</td>
                        </tr>
                        </tbody>
                    </table>

                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>returnCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패여부 상태코드</td>
                            <td>0: 성공(정상)
                                99:실패</td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left;table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                {"statusCode":800,"statusMessage":"DB Error","description":"SQLSTATE[42000]: Syntax error",returnCode":99}
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플<br>
                                {"statusCode":200,"statusMessage":"Success","description":"","returnCode":0}
                            </td>

                        </tr>

                    </table>
                </div>
                <br>
                <hr>
                <div id="apiGameHistory">
                    <h2 style="padding-left: 10px;"><i class="fa fa-diamond"></i> <b>Game History</b> </h2>
                    <p style="padding-left: 30px;"> - 플레이어 게임 BET/WIN 히스토리 제공.</p>
                    <h4 style="padding-left: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Request : GET 방식으로 호출</h4>
                    <p style="padding-left: 10px;word-break:break-all;word-wrap:break-word;">- HTTP URL :  "http://game.luckyladder168.com/api/v1/service/account/getBalance/{operatorId}/{apiKey}/{userId}"<br>
                        - getType 정의  :  1. GAMEBETWIN( 게임 BET/WIN 내역을 가져온다) /  2. GAMEBETWINDETAIL ( 게임 BET/WIN 결과 내역을 가져온다.)</p>


                    <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Response Format :: JSON 형태로 전환</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="tableHead">엘리먼트 명</th>
                            <th class="tableHead">DEPT</th>
                            <th class="tableHead">배열 구분</th>
                            <th class="tableHead">설명</th>
                            <th class="tableHead">값 구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>statusCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패 상태상세 코드</td>
                            <td>200 : 성공(정상)</td>
                        </tr>
                        <tr>
                            <td>statusMessage</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/ 실패에 대한 간략한 상태값 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>1</td>
                            <td></td>
                            <td>Error 상세 메시지</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>returnCode</td>
                            <td>1</td>
                            <td></td>
                            <td>성공/실패여부 상태코드</td>
                            <td>0: 성공(정상)
                                99:실패</td>
                        </tr>
                        <tr>
                            <td>resultData</td>
                            <td>1</td>
                            <td>Y</td>
                            <td>플레이어 게임 히스토리</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 샘플 JSON 예제</h4>
                    <table style="width:100%; border: 1px solid #ddd; text-align: left; table-layout:fixed;">
                        <tr >
                            <td style="border-bottom: 0px; text-align: left;word-break:break-all;word-wrap:break-word;">- 실패할 경우 샘플<br>
                                {"statusCode":700,"statusMessage":"DB null","description":"Game transaction is null","returnCode":0,"resultData":null}
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: left;word-break:break-all;word-wrap:break-word;">- 성공할 경우 샘플(getType: GAMEBETWIN)<br>
                              {"statusCode":200,"statusMessage":"Success","description":"","returnCode":0,"resultData":[{"gh_idx":10546,"op_id":"master"
                                ,"mb_id":"hy001","g_id":"LL_V1_001","tb_idx":10,"trs_type":"BET","before_amount":"99628500.00","trs_amount":"1000000.00"
                                ,"after_amount":"98628500.00","trs_id":"201706221730221610230116127_hy001"
                                ,"round_id":"20170622173022_16102301_161_27","period_id":"20170622173139_16102301_161_27"
                                ,"trs_date":"2017-06-22 17:32:48","currency":"KRW","check_result":"Y","trs_check_date":"2017-06-22 17:34:18"}]}
                                <br> <br>
                                -성공할 경우 샘플(getType: GAMEBETWINDETAIL)
                                <br>
                                {"statusCode":200,"statusMessage":"Success","description":"","returnCode":0,"resultData":[{"ghd_idx":16327,"op_id":"master"
                                ,"mb_id":"hy001","trs_id":"201706221730221610230116127_hy001","round_id":"20170622173022_16102301_161_27"
                                ,"period_id":"20170622173139_16102301_161_27","trs_type":"WIN","bet_location":"NUM3","bet_profitrate":"1.95"
                                ,"trs_amount":"585000.00","trs_date":"2017-06-22 17:34:17","check_result":"Y","gh_idx":10546,"trs_check_date":"2017-06-22 17:34:17","g_result":"RE3E","g_date":"2017-06-22 17:34:17}]}

                            </td>

                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>



    <!-- UI Object -->
<!--    <div id="contentapi">-->
<!--        <p>- luckyLadder API Documenet</p>-->
<!---->
<!---->
<!--        <!-- container -->
<!--        <div id="containerApi">-->
<!--            <!-- snb -->
<!--            <div class="snb">-->
<!--                 <h4 style="padding-left:10px">API Guide Document</h4>-->
<!--                <ul>-->
<!--                    <li><a href="#into">API Guide Document</a></li>-->
<!--                    <li><a href="">guide</a></li>-->
<!--                    <li>ccc</li>-->
<!--                    <li>dddd</li>-->
<!--                </ul>-->

<!--            </div>-->
<!--            <!-- //snb -->
<!--            <!-- content -->
<!--            <div id="contentRight">-->
<!--                <div class="main" >-->

<!--                    <div id="into" >-->

<!--                        <h2> API Guide Document</h2>-->

<!--                        <blockquote> <p>LuckyLadder 에서 제공하는 Rest-ful API 관련 정보 구성과 사용법에 대해 설명하고 있다. </p></blockquote>-->
<!--                        <hr>-->
<!--                        <h4>1. Rest-ful API 연동 규격서 소개</h4>-->
<!--                        <p style="padding-left: 20px;"> 이 문서는 Client 개발자에게 럭키라더(http://game.luckyladder168.com/api/v1/)과의 연동을 위한 응용 프로그램 개발에 필요한 데이터 구조와 game.luckyladder168.com/api/v1/ 에서 제공하는 기능에 대한 정보를 제공한다.-->
<!--                            </p>-->
<!--                        <hr>-->

<!--                    </div>-->
<!--                    <div id="guide" >-->
<!--11232-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <!-- //content -->
<!--        </div>-->
<!--        <!-- //container -->

<!--    </div>-->
    <!-- //UI Object -->
    <!-- end vertical-box -->
</div>
<script>




</script>
@endsection

