<?php

return [

    /* 공통 */
    'copy'                              =>      '복사',

    /* 대시보드 */
    'systemLevelName'                   =>      '시스템',
    'adminLevelName'                    =>      '어드민',
    'SYSTEM'                             =>     '시스템',
    'ADMIN'                             =>      '어드민',
    'OPERATOR'                          =>      '오퍼레이터',
    'PLAYER'                            =>      '플레이어',
    'superMasterAgentLevelName'         =>      '슈퍼마스터에이전트',
    'masterAgentLevelName'              =>      '마스터에이전트',
    'agentLevelName'                    =>      '에이전트',

    'home'                              =>      '홈',

    'todayDeposit'                      =>      '금일 입금',
    'todayWithdrawal'                   =>      '금일 출금',

    'dashboard'                         =>      '대시보드',
    'fixedDate'                         =>      '기준일',
    'currentMonthBets'                  =>      '이번달 총 베팅 금액',
    'lastMonthBets'                     =>      '지난달 총 베팅 금액',
    'betterThanLastMonth'               =>      '지난달 대비',
    'currentMonthWins'                  =>      '이번달 총 윈 금액',
    'lastMonthWins'                     =>      '지난달 총 윈 금액',
    'currentMonthPayout'                =>      '이번달 페이아웃',
    'lastMonthPayout'                   =>      '지난달 페이아웃',
    'totalMembers'                      =>      '총 회원가입수',
    'lastMonth'                         =>      '지난달',
    'currentMonth'                      =>      '이번달',
    'today'                             =>      '오늘',
    'last6monthBetWin'                  =>      '지난 6개월 벳윈',
    'thirdpartyStatus'                  =>      '서드파티 상태',
    'AllBet'                            =>      '올벳',
    'AsiaGaming'                        =>      '아시아게이밍',
    'BBIN'                              =>      'BBIN',
    'MicroGaming'                       =>      '마이크로게이밍',
    'MoCaGame'                          =>      '모카게임',
    'NewAsia'                           =>      '뉴아시아',
    'Oriental'                          =>      '오리엔탈',
    'StarGame'                          =>      '스타게임',
    'Taishan'                           =>      '타이산',
    'Midas'                             =>      '마이다스',

    'ACTIVE'                            =>      '활성화',
    'PENDING'                           =>      '중지',

    'MAINTENANCE'                       =>      '점검',
    'READY'                             =>      '준비',
    'ACTIVATE'                          =>      '활성',
    'DEACTIVATE'                        =>      '비활성',

    'DISCONNECTED'                      =>      '연결해제',
    'CONNECTED'                         =>      '연결완료',
    'STOPPING'                          =>      '중지중',

    'currentMonthDeposit'               =>      '이번달 총 입금액',
    'lastMonthDeposit'                  =>      '지난달 총 입금액',
    'currentMonthWithdrawal'            =>      '이번달 총 출금액',
    'lastMonthWithdrawal'               =>      '지난달 총 출금액',
    'currentMonthManualDeposit'         =>      '이번달 총 매뉴얼 입금액',
    'lastMonthManualDeposit'            =>      '지난달 총 매뉴얼 입금액',
    'currentMonthManualWithdrawal'      =>      '이번달 총 매뉴얼 출금액',
    'lastMonthManualWithdrawal'         =>      '지난달 총 매뉴얼 출금액',
    'last6monthDepositWithdrawal'       =>      '지난 6개월 입출금',
    'thirdpartyBets'                    =>      '서드파티별 베팅금액',
    'rate'                              =>      '비율',


    /* 멤버 */
    'members'                           =>      '멤버',
    'management'                        =>      '회원관리',
    'list'                              =>      '목록',
    'operator'                          =>      '오퍼레이터',
    'admin'                             =>      '마스터관리자',
    'operatorlist'                      =>      '오퍼레이터 리스트',
    'gamelist'                          =>      '게임 리스트',
    'playerlist'                        =>      '플레이어 리스트',
    'adminlist'                         =>      '마스터관리자 리스트',
    'site'                              =>      '슈퍼마스터에이전트',
    'superMasterAgent'                  =>      '슈퍼마스터에이전트',
    'masterAgent'                       =>      '마스터 에이전트',
    'agent'                             =>      '에이전트',
    'createsite'                        =>      '슈퍼마스터에이전트 생성',
    'createsuperMasterAgent'            =>      '슈퍼마스터에이전트 생성',
    'createmasterAgent'                 =>      '마스터에이진트 생성',
    'createagent'                       =>      '에이전트 생성',
    'id'                                =>      '아이디',
    'name'                              =>      '이름',
    'email'                             =>      '이메일',
    'prefix'                            =>      '사용자 Prefix',
    'balance'                           =>      '보유금액',
    'mainUrl'                           =>      '메인 사이트주소',
    'dealSet'                           =>      '딜',
    'signUpDate'                        =>      '가입일',

    'player'                            =>      '플레이어',
    'searchBox'                         =>      '검색',
    'detail'                            =>      '상세',
    'date'                              =>      '기간',
    'gameDate'                          =>      '게임일자',
    'searchDuration'                    =>      '검색기간',
    'searchCondition'                   =>      '검색조건',
    'to'                                =>      '부터',
    'playerId'                          =>      '플레이어 아이디',
    'operatorName'                      =>      '오퍼레이터명',
    'confirmResult'                     =>      '게임결과 처리',
    'complete'                          =>      '완료',
    'incomplete'                        =>      '미완료',
    'playerName'                        =>      '플레이어명',
    'adminName'                         =>      '마스터관리자명',
    'all'                               =>      '전체',
    'assignedMember'                    =>      '소속 에이전트',
    'lastLoginDate'                     =>      '최근 접속일',
    'betWinHistory'                     =>      '게임기록',
    'view'                              =>      '보기',
    'searching'                         =>      '검색하기',

    'createDownLine'                    =>      '하부라인 생성',
    'level'                             =>      '레벨',
    'create'                            =>      '생성',

    'memberLevel'                       =>      '레벨',
    'agentName'                         =>      '에이전트명',
    'agentID'                           =>      '에이전트 아이디',

    'createNewDownLine'                 =>      '하부라인 생성',
    'memberBasicInformation'            =>      '멤버 기본정보',
    'playerBasicInformation'            =>      '플레이어 기본정보',
    'operatorBasicInformation'          =>      '오퍼레이터 기본정보',
    'adminBasicInformation'             =>      '마스터관리자 기본정보',
    'contactInformation'                =>      '연락정보',
    'loginInformation'                  =>      '로그인 계정',
    'complete'                          =>      '완료',
    'memberName'                        =>      '멤버 이름',
    'memberDeal'                        =>      '멤버 딜',
    'memberRoll'                        =>      '멤버 롤',
    'memberCashOperating'               =>      '멤버 입출금 운영방식',
    'memberMainUrl'                     =>      '멤버 메인 사이트',
    'accountingMethod'                  =>      '정산방식',
    'bet/win'                           =>      '베팅/윈정산',
    'rolling'                           =>      '롤링정산',
    'deposit/withdrawal'                =>      '입출금',
    'previous'                          =>      '이전',
    'next'                              =>      '다음',
    'choiceCashOperating'               =>      '입출금 운영방식 선택',
    'self'                              =>      '자체운영',
    'upper'                             =>      '상위레벨 운영',
    'memberCurrency'                    =>      '화폐',
    'memberCashType'                    =>      '정산타입',
    'phoneNumber'                       =>      '전화번호',
    'mobileNumber'                      =>      '휴대전화번호',
    'apiKey'                            =>      'API 키',
    'emailAddress'                      =>      '이메일',
    'adminLoginAccount'                 =>      '관리자 계정',
    'operatorLoginAccount'              =>      '오퍼레이터 계정',
    'playerLoginAccount'                =>      '플레이어 계정',
    'loginAccount'                      =>      '로그인 계정',
    'password'                          =>      '비밀번호',
    'confirmCreateMember'               =>      '멤버 생성확인',
    'confirmCreatePlayer'               =>      '플레이어 생성확인',
    'confirmCreateOperator'             =>      '오퍼레이터 생성확인',
    'confirmCreateAdmin'                =>      '마스터관리자 생성확인',
    'confirm'                           =>      '확인',
    'restore'                           =>      '취소',
    'login'                             =>      '로그인',


    'createSubAdmin'                    =>      '서브관리자 생성',
    'subAdmin'                          =>      '서브관리자',
    'subAdminAccountInfo'               =>      '서브관리자 계정 정보',
    'subAdminLoginId'                   =>      '서브관리자 로그인 아이디',

    'betWinDaily'                       =>      '일별 벳윈',
    'betWin'                            =>      '베팅 / 윈',
    'daily'                             =>      '일별',
    'thirdparty'                        =>      '서드파티',
    'bet'                               =>      '베팅',
    'win'                               =>      '윈',
    'bet-win'                           =>      '베팅 - 윈',
    'payout'                            =>      '페이아웃',
    'reports'                           =>      '리포트',
    'rollingDaily'                      =>      '일별 롤링',
    'rollingAgent'                      =>      '롤링 에이전트',
    'rollingAmount'                     =>      '롤링',
    'rollingDeal'                       =>      '롤링 딜',


    'betWinThirdparty'                  =>      '베팅 / 윈 서드파티',

    'depositWithdrawalDaily'            =>      '일별 입출금',
    'depositWithdrawal'                 =>      '입금 / 출금',
    'deposit'                           =>      '입금',
    'manualDeposit'                     =>      '수동입금',
    'withdrawal'                        =>      '출금',
    'manualWithdrawal'                  =>      '수동출금',
    'totalDeposit'                      =>      '전체입금',
    'totalWithdrawal'                   =>      '전체출금',
    'TOTAL'                             =>      '전체',
    'NETELLER'                          =>      '넷텔러',
    'UNIONPAY'                          =>      '유니온페이',
    'BANK'                              =>      '무통장',
    'MANUAL'                            =>      '수동',
    'depositMethod'                     =>      '입금수단',
    'depositWithdrawalMethod'           =>      '입출금 수단',
    'method'                            =>      '수단',


    'gameHistory'                       =>      '게임내역',
    'operatorLevel'                     =>      '등급',
    'createOperator'                    =>      '오퍼레이터 생성',
    'createAdmin'                       =>      '마스터관리자 생성',
    'amount'                            =>      '금액',
    'betWinAmount'                      =>      '베팅/윈 금액',


    'accountRequest'                    =>      '정산요청',
    'request'                           =>      '요청',
    'accountingAmount'                  =>      '정산금액',
    'startDate'                         =>      '시작일',
    'endDate'                           =>      '종료일',
    'status'                            =>      '상태',
    'viewRequest'                       =>      '보기 & 요청',
    'myRequestStatusList'               =>      '내 정산요청 상태 리스트',
    'betWinStatusList'                  =>      '벳윈 정산요청 상태 리스트',
    'rollingStatusList'                 =>      '롤링 정산요청 상태 리스트',
    'accountings'                       =>      '정산',
    'NoDataAvailableTable'              =>      '데이터가 없습니다',

    'accountingRequested'               =>      '정산완료',
    'command'                           =>      '비고',
    'requested'                         =>      '신청완료',

    'accountStatus'                     =>      '정산상태',


    'requestDate'                       =>      '요청일',
    'issuedDate'                        =>      '처리일',
    'CONFIRM'                           =>      '승인',
    'REQUEST'                           =>      '요청',
    'CANCEL'                            =>      '취소',
    'issuedAdmin'                       =>      '처리자',
    'transaction'                       =>      '입출금 기록',
    'depositList'                       =>      '입금목록',
    'nameOfDeposit'                     =>      '입금자명',
    'depositBankName'                   =>      '입금은행명',
    'depositBankNumber'                 =>      '입금은행 계좌',
    'mobile'                            =>      '휴대폰번호',

    'nameOfWithdrawal'                  =>      '출금자명',
    'withdrawalBankName'                =>      '출금은행명',
    'withdrawalBankNumber'              =>      '출금은행 계좌',
    'withdrawalList'                    =>      '출금목록',


    'modify'                            =>      '변경',
    'activate-deactivate'               =>      '활성/비활성',
    'active'                            =>      '활성화',
    'activate'                          =>      '활성화',
    'deactivate'                        =>      '비활성화',
    'maintenance'                       =>      '점검',
    'thirdpartySetting'                 =>      '서드파티 설정',


    'games'                             =>      '게임',
    'gameSetting'                       =>      '게임설정',
    'gameName'                          =>      '게임명',
    'gameType'                          =>      '게임타입',
    'gameKind'                          =>      '게임종류',
    'baccarat'                          =>      '바카라',
    'roulette'                          =>      '룰렛',
    'dragonTiger'                       =>      '드래곤타이거',
    'blackJack'                         =>      '블랙잭',
    'sicbo'                             =>      '식보',
    'videoPoker'                        =>      '비디오포커',
    'slot'                              =>      '슬롯',
    '3dSlot'                            =>      '3D 슬롯',
    'lobby'                             =>      '로비',
    'live'                              =>      '라이브게임',
    'rng'                               =>      'RNG게임',
    'thirdpartyName'                    =>      '서드파티명',
    'thirdparytStatus'                  =>      '서드파티 상태',

    'gameStatus'                        =>      '게임 상태',
    'gameTitle'                         =>      '게임 타이틀',
    'betTime'                           =>      '베팅시간',
    'restTime'                          =>      '대기시간',
    'delayTime'                         =>      '딜레이시간',
    'gameShoe'                          =>      '게임 슈',
    'gameCount'                         =>      '게임 회차',
    'gameVideo'                         =>      '영상주소',

    'machineStatus'                     =>      '기계 상태',
    'tableStatus'                       =>      '테이블 상태',
    'dealerName'                        =>      '딜러 명',
    'platform'                          =>      '구동환경',
    'flash'                             =>      '플래시',
    'html5'                             =>      'HTML5',
    'hot'                               =>      '핫게임',
    'hotGameSet'                        =>      '핫게임 설정',
    'LIVE'                              =>      '라이브게임',
    'RNG'                               =>      'RNG게임',
    'FLASH'                             =>      '플래시',
    'HTML5'                             =>      'HTML5',


    'bonusList'                         =>      '보너스 목록',
    'bonus'                             =>      '보너스',
    'bonusName'                         =>      '보너스명',
    'bonusType'                         =>      '보너스 타입',
    'bonusKind'                         =>      '보너스 종류',
    'bonusAmount'                       =>      '보너스 금액',
    'wageringAmount'                    =>      '웨이저링 금액',
    'AUTOBONUS'                         =>      '자동보너스',
    'MANUALBONUS'                       =>      '수동보너스',
    'DEPOSITBONUS'                      =>      '입금보너스',
    'SIGNUPBONUS'                       =>      '회원가입보너스',
    'ETCBONUS'                          =>      '기타보너스',
    'INSTANTREDEEMED'                   =>      '즉시출금가능',
    'SUSPEND'                           =>      '중지',
    'promotion'                         =>      '프로모션',
    'firstDepositBonus'                 =>      '첫입금 보너스',
    'manualDepositBonus'                =>      '수동입금 보너스',
    'of'                                =>      '의',
    'depositAmount'                     =>      '입금액',

    'createManualBonus'                 =>      '수동보너스 생성',
    'manualBonus'                       =>      '수동보너스',
    'bonusCashOutCondition'             =>      '보너스 출금방식',
    'instantRedeemed'                   =>      '즉시출금',
    'redeemedAfterWagering'             =>      '웨이저링 클리어 후 출금',
    'wageringAmountSet'                 =>      '웨이저링 금액 설장',
    'bonusAmountTimes'                  =>      '보너스금액의 배수',
    'fixAmount'                         =>      '고정금액',
    'CREATEMANUALBONUS'                 =>      '수동보너스 생성',

    'createAutoBonus'                   =>      '자동보너스 생성',
    'autoBonus'                         =>      '자동보너스',
    'signupBonus'                       =>      '회원가입보너스',
    'depositBonus'                      =>      '입금보너스',
    'dailyFirstDeposit'                 =>      '매일첫입금보너스',
    'depositAmountTimes'                =>      '입금액의 배수',
    'depositOption'                     =>      '입금옵션',
    'percentOfDepositAmount'            =>      '입금액의 퍼센티지',
    'inputDirect'                       =>      '직접입력',
    'CREATEAUTOBONUS'                   =>      '자동보너스생성',


    'noticeList'                        =>      '공지사항 목록',
    'board'                             =>      '게시판',
    'subject'                           =>      '제목',
    'createNotice'                      =>      '공지사항 생성',
    'notice'                            =>      '공지사항',
    'DELETE'                            =>      '삭제',
    'delete'                            =>      '삭제',
    'noticeSubject'                     =>      '공지사항 제목',
    'contents'                          =>      '내용',
    'CREATENOTICE'                      =>      '공지사항 생성',
    'UPDATENOTICE'                      =>      '공지사항 수정',
    'view&UpdateNotice'                 =>      '보기 & 수정',

    'event'                             =>      '이벤트',
    'eventList'                         =>      '이벤트 목록',
    'CREATEEVENT'                       =>      '이벤트 생성',
    'eventLabel'                        =>      '이벤트 라벨',
    'eventName'                         =>      '이벤트명',
    'eventImage'                        =>      '이벤트 이미지',
    'createEvent'                       =>      '이벤트 생성',
    'MODIFYEVENT'                       =>      '이벤트 수정',


    'FAQ'                               =>      '자주묻는질문',
    'Basic'                             =>      '기본',
    'Deposit & Withdrawal'              =>      '입출금',
    'modifyFAQ'                         =>      '자주묻는질문 수정',
    'CREATEBASICFAQ'                    =>      '자주묻는질문 기본 생성',
    'CREATEDEPOSIT&WITHDRAWALFAQ'       =>      '자주묻는질문 입출금 생성',
    'CREATEGAMEFAQ'                     =>      '자주묻는질문 게임 생성',
    'Game'                              =>      '게임',
    'question'                          =>      '질문',
    'answer'                            =>      '답변',
    'CREATEFAQ'                         =>      '자주묻는질문 생성',
    'MODIFYFAQ'                         =>      '자주묻는질문 수정',



    'bannerList'                        =>      '배너 목록',
    'banner'                            =>      '배너',
    'viewBanner'                        =>      '배너보기',
    'bannerBigText'                     =>      '배너 큰 문구',
    'bannerSmallText1'                  =>      '배너 작은 문구 1',
    'bannerSmallText2'                  =>      '배너 작은 문구 2',
    'bannerBigTextColor'                =>      '배너 큰 문구 색상',
    'bannerSmallTextColor1'             =>      '배너 작은 문구 색상 1',
    'bannerSmallTextColor2'             =>      '배너 작은 문구 색상 2',
    'bannerImage'                       =>      '배너 이미지',
    'MODIFYBANNER'                      =>      '배너수정',
    'CREATEBANNER'                      =>      '배너생성',
    'createBanner'                      =>      '배너생성',


    'playerSiteSetting'                 =>      '플레이어 사이트 설정',
    'playerSiteInformation'             =>      '플레이어 사이트 정보',
    'windowTitle'                       =>      '윈도우 타이틀',
    'supportPhoneNumber1'               =>      '고객지원 전화번호 1',
    'supportPhoneNumber2'               =>      '고객지원 전화번호 2',
    'supportMessenger1'                 =>      '고객지원 메신저 1',
    'supportMessenger2'                 =>      '고객지원 메신저 2',
    'confirmCreateSiteSetting'          =>      '플레이어 사이트 설정 확인',
    'createSiteSettings'                =>      '플레이어 사이트 설정 생성',


    'memberDetails'                     =>      '상세정보',
    'basicInfo'                         =>      '기본정보',
    'INFORMATION'                       =>      '정보',
    'playerSiteInfo'                    =>      '플레이어 사이트정보',
    'subAccountInfo'                    =>      '서브 관리자정보',
    'subDomainManage'                   =>      '홍보도메인 관리',
    'point'                             =>      '포인트',
    'contact'                           =>      '연락처',
    'registedDate'                      =>      '등록일',
    'cashType'                          =>      '정산방식',
    'currencyType'                      =>      '화폐',
    'modifyInformation'                 =>      '정보수정',
    'pointTransaction'                  =>      '포인트 내역',
    'adminId'                           =>      '관리자 아이디',
    'operatorId'                        =>      '오퍼레이터 아이디',
    'createDate'                        =>      '생성일',
    'lastLogin'                         =>      '최근 로그인',
    'subDomainUrl'                      =>      '홍보도메인주소',
    'createSubDomain'                   =>      '홍보도메인 등록',
    'addPoint'                          =>      '포인트 지급',
    'subtractPoint'                     =>      '포인트 회수',
    'deposit&withdrawal'                =>      '입출금',
    'POSTPAID'                          =>      '후불정산',
    'PREPAID'                           =>      '선불정산',
    'choiceCashType'                    =>      '정산방식 선택',
    'SUPER MASTER AGENT'                =>      '슈퍼마스터에이전트',
    'MASTER AGENT'                      =>      '마스터에이전트',
    'AGENT'                             =>      'AGENT',
    'subAdminId'                        =>      '서브관리자 아이디',
    'SUB'                               =>      '서브계정',
    'MAIN'                              =>      '주계정',
    'subDomain'                         =>      '홍보 도메인',
    'memo'                              =>      '메모',
    'setMemberThirdparty'               =>      '멤버 서드파티 세팅',
    'assignThirdparty'                  =>      '서드파티 할당',
    'assignGame'                        =>      '게임할당',
    'setMemberGames'                    =>      '멤버 게임세팅',


    'playerDetail'                      =>      '플레이어 상세정보',
    'summary'                           =>      '요약',
    'bwt/Win'                           =>      '베팅 / 윈',
    'MANUALTRANSACTIONS'                =>      '수동 입출금 및 보너스',
    'last3monthBetWin'                  =>      '지난 3개월 베팅/윈',
    'last3monthDeposit&withdrawal'      =>      '지난 3개월 입출금',
    'month'                             =>      '월',
    'deposit-withdrawal'                =>      '입금 - 출금',
    'information'                       =>      '정보',
    'tel'                               =>      '전화번호',
    'signupDate'                        =>      '가입일',
    'manualDepositToPlayer'             =>      '플레이어 수동입금',
    'depositToPlayer'                   =>      '수동입금',
    'manualWithdrawalToPlayer'          =>      '플레이어 수동출금',
    'withdrawalToPlayer'                =>      '수동출금',
    'depositType'                       =>      '입출금',
    'D'                                 =>      '입금',
    'P'                                 =>      '출금',


    'bonusReceivedDate'                 =>      '보너스 받은날',
    'wageringFinishedDate'              =>      '웨이저링 달성일',
    'bonusStatus'                       =>      '보너스 상태',
    'A'                                 =>      '활성화',
    'W'                                 =>      '웨이저링 클리어',
    'C'                                 =>      '취소',

    'memberRollingDeal'                 =>      '멤버 롤링 딜',
    'beforeAmount'                      =>      '이전금액',
    'totalAmount'                       =>      '전체금액',
    'createPlayer'                      =>      '플레이어 생성',
    'playerList'                        =>      '플레이어 리스트',

    'accountingIssuedDate'              =>      '정산날짜',
    'accountingRolling'                 =>      '롤링정산',
    'accountingBetWin'                  =>      '벳윈정산',
    'vendorDeal'                        =>      '벳윈 딜',
    'desc'                              =>      '비고',

    'searchResultAccBet'                =>      '검색결과 : 벳윈정산',
    'searchResultAccRoll'               =>      '검색결과 : 롤링정산',
    'exceptionAmount'                   =>      '제외 베팅',
    'drawBet'                           =>      '무승부',

    'gamemanagement'                    =>      '게임관리',
    'betLimitList'                      =>      '베팅팅제한 리스트',
    'registerBetLimit'                  =>      '베팅팅제한 등록',
    'register'                          =>      '등록',

    'maxBetting'                        =>      '최대 베팅',
    'minBetting'                        =>      '최소 베팅',
    'maxTieBetting'                     =>      '최대 타이 베팅',
    'minTieBetting'                     =>      '최소 타이 베팅',

    'registerDate'                      =>      '등록일',
    'statusChange'                      =>      '상태변경',
    'gameResult'                        =>      '게임결과',
    'resultDate'                        =>      '결과일자',
    'transactionId'                     =>      '식별코드',
    'periodId'                          =>      '회차코드',
    'betLocation'                       =>      '베팅위치',
    'betAmount'                         =>      '베팅금액',
    'winAmount'                         =>      '배당금액',
    'noTransId'                         =>      '트렌젝션 코드가 없습니다. 관리자에게 문의 하세요.',
    'noGameDate'                        =>      '해당 게임의 결과일자가 없습니다 관리자에게 문의하세요.',
    'noGameResult'                      =>      '해당 게임의 결과가 없습니다 관리자에게 문의하세요.',
    'noProcessing'                      =>      '해당 게임의 결과를 처리 할 수 없습니다. 시스템 관리자에게 문의하세요.',

    'machineId'                         =>      '기계 아이디',
    'resttime'                          =>      '대기시간(초)',
    'bettime'                           =>      '베팅시간(초)',

    'proceedConfirm'                    =>      '이 작업을 계속 진행 하시겠습니까?',
    'lineUp'                            =>      '정렬',
    'lineUpAsc'                         =>      '내림차순',
    'lineUpDesc'                        =>      '오름차순',

    'gameTableList'                     =>      '게임테이블 리스트',

    'edit'                              =>      '수정',

    'tableName'                         =>      '테이블 명',
    'tableCurrency'                     =>      '화폐',
    'gameId'                            =>      '게임 아이디',

    'registerTable'                     =>      '테이블 등록',
    'tableBasicInfo'                    =>      '테이블 기본정보',
    'gameInfo'                          =>      '게임정보',

    'gameList'                          =>      '게임 리스트',
    'dealerList'                        =>      '딜러 리스트',

    'tableDetails'                      =>      '테이블 상세정보',
    'tieMaxBetting'                     =>      '타이 최대 베팅',

    'agentList'                         =>      '에이전트 리스트',
    'PREPAIDReport'                           =>      '선불계정',
    'winAmountReport'                   =>      '승리금액',
    'bet-winReport'                     =>      '베팅 - 승리',
    'commissionReport'                  =>      '커미션',
];