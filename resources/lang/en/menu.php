<?php

return [
    'DASHBOARD'                 =>  'DASHBOARD',
    'MANAGEMENT'                =>  'MANAGEMENT',
    'OPERATORS'                 =>  'OPERATORS',

    'Home'                      =>  'Home',
    'Member'                    =>  'Member',
    'OperatorList'              =>  'Operator List',
    'AdminList'                 =>  'Admin List',
    'PlayerList'                =>  'Player List',

    'OperatorName'              => 'Operator Name',
    'OperatorId'                => 'Operator ID',
    'OperatorEmail'             => 'Operator Email',
    'OperatorBalance'           => 'Operator Balance',
    'OperatorDealSet'           => 'Operator Deal Set',
    'OperatorSignUp'            => 'Operator Sign Up Date',



    'MEMBERS'                   =>  'MEMBERS',
    'MEMBERTREE'                =>  'Member Tree',
    'operatorList'              =>  'Operator List',
    'createPlayer'              =>  'Create Player',
    'createOperator'            =>  'Create Operator',
    'createAdmin'               =>  'Create Admin',
    'SITELIST'                  =>  'Super Master Agent List',
    'MASTERAGENTLIST'           =>  'Master Agent List',
    'AGENTLIST'                 =>  'Agent List',
    'PLAYERLIST'                =>  'Player List',
    'CREATEMEMBER'              =>  'Create Member',
    'CREATESUBADMIN'            =>  'CREATE SUB-ADMIN',
    'REPORTS'                   =>  'REPORTS',
    'POINT'                     =>  'Point',
    'BETWIN'                    =>  'Bet / Win',
    'DAILY'                     =>  'Daily',
    'THIRDPARTY'                =>  'Thirdparty',
    'DEPOSITWITHDRAWAL'         =>  'Deposit / Withdrawal',
    'BONUS'                     =>  'Bonus',
    'GAMEHISTORY'               =>  'GAME HISTORY',
    'ACCOUNTINGS'               =>  'ACCOUNTINGS',
    'REQUESTLIST'               =>  'Request & List',
    'CONFIRMEDLIST'             =>  'Confirmed List',
    'TRANSACTIONS'              =>  'TRANSACTIONS',
    'DEPOSITS'                  =>  'Deposits',
    'WITHDRAWALS'               =>  'Withdrawals',
    'FAKETRANSACTION'           =>  'Fake Transaction',

    'GAMESETTINGS'              =>  'GAME SETTINGS',
    'GAMES'                     =>  'Games',

    'GAMEMANAGEMENT'            =>  'GAME MANAGEMENT',
    'gameStatus'                =>  'Game Status',
    'gameSetting'               =>  'Game Setting',
    'betLimits'                 =>  'Betting Limits',

    'HOTGAMES'                  =>  'Hot Games',
    'POINTREQUEST'              =>  'POINT REQUEST',
    'PROMOTIONS'                =>  'PROMOTIONS',
    'CREATEMANUALBONUS'         =>  'Create Manual Bonus',
    'CREATEAUTOBONUS'           =>  'Create Auto Bonus',
    'COUPONS'                   =>  'Coupons',
    'CREATECOUPONS'             =>  'Create Coupons',
    'BOARDS'                    =>  'BOARDS',
    'NOTICE'                    =>  'Notice',
    'EVENT'                     =>  'Event',
    'FAQ'                       =>  'FAQ',
    '247BOARD'                  =>  '24/7 Board',
    'CREATEPOPUP'               =>  'Create Popup',
    'CREATEBANNER'              =>  'Create Banner',
    'PLAYERSITESETTING'         =>  'PLAYER SITE SETTING',
    'LOGOUT'                    =>  'LOGOUT',
    'THIRDPARTYSETTING'         =>  'Thirdparys Setting',
    'REQUEST'                   =>  'Request',
    'REQUESTED'                 =>  'Requested',
    'STATUS'                    =>  'Status',
    'depositMethod'             =>  'Deposit Method',
    'ROLLING'                   =>  'Rolling',
    'AGENTREPORT'               =>  'Agent',

    'gameTables'                =>   'Game Tables',

];